﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MonitorPlayerWeapons : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_MonitorPlayerWeapons = false;
    private string isDebugScriptName_MonitorPlayerWeapons = "MonitorPlayerWeapons";
    #endregion

    #region Public
    [Header("UI")]
    [SerializeField] private Slider chargeSlider = null;
    [SerializeField] private Text ammoCounter = null;

    [Header("Weapons")]
    [SerializeField] private Gun weaponOne = null;
    [SerializeField] private Gun weaponTwo = null;
    [SerializeField] private Gun weaponThree = null;
    [SerializeField] private Gun weaponFour = null;
    #endregion

    #region Private
    private int activeWeapon = 0;

    private bool aquiredWeaponOne = false;
    private bool aquiredWeaponTwo = false;
    private bool aquiredWeaponThree = false;
    private bool aquiredWeaponFour = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Switches to the desired weapon if that weapon is available.
    /// </summary>
    /// <param name="selection">The weapon to be selected (1-4).</param>
    public void SelectWeapon(int selection)
    {
        if (selection < 0) selection = 0;
        else if (selection > 3) selection = 3;
        PrintDebugMsg_MonitorPlayerWeapons("Changing to weapon " + selection + "...");

        switch(selection)
        {
            case 0:
                if (aquiredWeaponOne)
                {
                    activeWeapon = selection;

                    weaponOne.gameObject.SetActive(true);
                    weaponOne.OnWeaponSelected();
                    if (weaponTwo != null)
                    {
                        weaponTwo.gameObject.SetActive(false);
                        weaponTwo.OnWeaponDeSelected();
                    }
                    if (weaponThree != null)
                    {
                        weaponThree.gameObject.SetActive(false);
                        weaponThree.OnWeaponDeSelected();
                    }
                    if (weaponFour != null)
                    {
                        weaponFour.gameObject.SetActive(false);
                        weaponFour.OnWeaponDeSelected();
                    }

                    UpdateUI();
                }
                else PrintDebugMsg_MonitorPlayerWeapons("  Have not aquired weapon one yet or is unavailable!");
                break;
            case 1:
                if (aquiredWeaponTwo)
                {
                    activeWeapon = selection;

                    if (weaponOne != null)
                    {
                        weaponOne.gameObject.SetActive(false);
                        weaponOne.OnWeaponDeSelected();
                    }
                    weaponTwo.gameObject.SetActive(true);
                    weaponTwo.OnWeaponSelected();
                    if (weaponThree != null)
                    {
                        weaponThree.gameObject.SetActive(false);
                        weaponThree.OnWeaponDeSelected();
                    }
                    if (weaponFour != null)
                    {
                        weaponFour.gameObject.SetActive(false);
                        weaponFour.OnWeaponDeSelected();
                    }

                    UpdateUI();
                }
                else PrintDebugMsg_MonitorPlayerWeapons("  Have not aquired weapon two yet or is unavailable!");
                break;
            case 2:
                if (aquiredWeaponThree)
                {
                    activeWeapon = selection;

                    if (weaponOne != null)
                    {
                        weaponOne.gameObject.SetActive(false);
                        weaponOne.OnWeaponDeSelected();
                    }
                    if (weaponTwo != null)
                    {
                        weaponTwo.gameObject.SetActive(false);
                        weaponTwo.OnWeaponDeSelected();
                    }
                    weaponThree.gameObject.SetActive(true);
                    weaponThree.OnWeaponSelected();
                    if (weaponFour != null)
                    {
                        weaponFour.gameObject.SetActive(false);
                        weaponFour.OnWeaponDeSelected();
                    }

                    UpdateUI();
                }
                else PrintDebugMsg_MonitorPlayerWeapons("  Have not aquired weapon three yet or is unavailable!");
                break;
            case 3:
                if (aquiredWeaponFour)
                {
                    activeWeapon = selection;

                    if (weaponOne != null)
                    {
                        weaponOne.gameObject.SetActive(false);
                        weaponOne.OnWeaponDeSelected();
                    }
                    if (weaponTwo != null)
                    {
                        weaponTwo.gameObject.SetActive(false);
                        weaponTwo.OnWeaponDeSelected();
                    }
                    if (weaponThree != null)
                    {
                        weaponThree.gameObject.SetActive(false);
                        weaponThree.OnWeaponDeSelected();
                    }
                    weaponFour.gameObject.SetActive(true);
                    weaponFour.OnWeaponSelected();

                    UpdateUI();
                }
                else PrintDebugMsg_MonitorPlayerWeapons("  Have not aquired weapon four yet or is unavailable!");
                break;
        }
    }

    /// <summary>
    /// Tells the currently active gun to fire with the current fire action.
    /// </summary>
    /// <param name="action">The current action about this shot in a series of shots (started, ended, etc.).</param>
    public void FireWeapon(WeaponAction action)
    {
        switch(activeWeapon)
        {
            case 0:
                PrintDebugMsg_MonitorPlayerWeapons("Firing weapon one (" + activeWeapon + ")...");
                weaponOne.Fire(action);
                break;
            case 1:
                PrintDebugMsg_MonitorPlayerWeapons("Firing weapon two (" + activeWeapon + ")...");
                weaponTwo.Fire(action);
                break;
            case 2:
                PrintDebugMsg_MonitorPlayerWeapons("Firing weapon three (" + activeWeapon + ")...");
                weaponThree.Fire(action);
                break;
            case 3:
                PrintDebugMsg_MonitorPlayerWeapons("Firing weapon four (" + activeWeapon + ")...");
                weaponFour.Fire(action);
                break;
        }
    }

    /// <summary>
    /// Attempts to re-supply the current wapon selected.
    /// </summary>
    /// <param name="isCharge">Whether or not the amount is for a charge weapon.</param>
    /// <param name="amount">The amount to be re-supplied.</param>
    /// <returns>True if successful.</returns>
    public bool ReSupplyCurrentWeapon(bool isCharge, int amount)
    {
        PrintDebugMsg_MonitorPlayerWeapons("Attempting to re-supply weapon #" + activeWeapon + " with " + amount + ((isCharge) ? " charge" : " ammo") + ".");

        ChargeGun chargeWeapon = null;
        AmmoGun ammoWeapon = null;
        switch(activeWeapon)
        {
            case 0:
                if (isCharge) chargeWeapon = weaponOne.GetComponent<ChargeGun>();
                else ammoWeapon = weaponOne.GetComponent<AmmoGun>();
                break;
            case 1:
                if (isCharge) chargeWeapon = weaponTwo.GetComponent<ChargeGun>();
                else ammoWeapon = weaponTwo.GetComponent<AmmoGun>();
                break;
            case 2:
                if (isCharge) chargeWeapon = weaponThree.GetComponent<ChargeGun>();
                else ammoWeapon = weaponThree.GetComponent<AmmoGun>();
                break;
            case 3:
                if (isCharge) chargeWeapon = weaponFour.GetComponent<ChargeGun>();
                else ammoWeapon = weaponFour.GetComponent<AmmoGun>();
                break;
        }
        if(chargeWeapon != null)
        {
            PrintDebugMsg_MonitorPlayerWeapons("  Attempting to re-supply a charge weapon...");
            if(chargeWeapon.ReSupply(amount))
            {
                PrintDebugMsg_MonitorPlayerWeapons("    Successfully re-supplied charge weapon.");
                return true;
            }
        }
        if(ammoWeapon != null)
        {
            PrintDebugMsg_MonitorPlayerWeapons("  Attempting to re-supply an ammo weapon...");
            if (ammoWeapon.ReSupply(amount))
            {
                PrintDebugMsg_MonitorPlayerWeapons("    Successfully re-supplied ammo weapon.");
                return true;
            }
        }

        PrintDebugMsg_MonitorPlayerWeapons("  Unable to re-supply.");
        return false;
    }
    /// <summary>
    /// Picks up the weapon for the given weapon num.
    /// </summary>
    /// <param name="weaponNum">The number of this weapon the player is holding.</param>
    /// <returns>True if successful.</returns>
    public bool PickupWeapon(int weaponNum)
    {
        if(weaponNum < 0 || weaponNum > 3)
        {
            PrintErrorDebugMsg_MonitorPlayerWeapons("Weapon number must be 0-3!");
            return false;
        }

        switch(weaponNum)
        {
            case 0:
                if (!aquiredWeaponOne)
                {
                    aquiredWeaponOne = true;
                    SelectWeapon(0);
                    return true;
                }
                else
                {
                    SelectWeapon(0);
                    if (weaponOne.GetComponent<Gun>() != null) return weaponOne.GetComponent<Gun>().ReSupplyFull();
                }
                break;
            case 1:
                if (!aquiredWeaponTwo)
                {
                    aquiredWeaponTwo = true;
                    SelectWeapon(1);
                    return true;
                }
                else
                {
                    SelectWeapon(1);
                    if (weaponTwo.GetComponent<Gun>() != null) return weaponTwo.GetComponent<Gun>().ReSupplyFull();
                }
                break;
            case 2:
                if (!aquiredWeaponThree)
                {
                    aquiredWeaponThree = true;
                    SelectWeapon(2);
                    return true;
                }
                else
                {
                    SelectWeapon(2);
                    if (weaponThree.GetComponent<Gun>() != null) return weaponThree.GetComponent<Gun>().ReSupplyFull();
                }
                break;
            case 3:
                if (!aquiredWeaponFour)
                {
                    aquiredWeaponFour = true;
                    SelectWeapon(3);
                    return true;
                }
                else
                {
                    SelectWeapon(3);
                    if (weaponFour.GetComponent<Gun>() != null) return weaponFour.GetComponent<Gun>().ReSupplyFull();
                }
                break;
        }

        return false;
    }
    #endregion

    #region Private
    /// <summary>
    /// Sets variables, gives the monitor player the weapons' mesh renderers, selects the first weapon available.
    /// </summary>
    private void Setup()
    {
        // Check weapons
        List<MeshRenderer> meshRenderers = new List<MeshRenderer>();
        MonitorPlayer mPlayerScript = GetComponent<MonitorPlayer>();
        MeshRenderer[] mRenderers;
        if (weaponOne != null)
        {
            mRenderers = weaponOne.transform.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mr in mRenderers) meshRenderers.Add(mr);
        }
        else PrintDebugMsg_MonitorPlayerWeapons("No weaopn one.");
        if (weaponTwo != null)
        {
            mRenderers = weaponTwo.transform.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mr in mRenderers) meshRenderers.Add(mr);
        }
        else PrintDebugMsg_MonitorPlayerWeapons("No weapon two.");
        if (weaponThree != null)
        {
            mRenderers = weaponThree.transform.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mr in mRenderers) meshRenderers.Add(mr);
        }
        else PrintDebugMsg_MonitorPlayerWeapons("No weapon three.");
        if (weaponFour != null)
        {
            mRenderers = weaponFour.transform.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mr in mRenderers) meshRenderers.Add(mr);
        }
        else PrintDebugMsg_MonitorPlayerWeapons("No weapon four.");
        mPlayerScript.SetRenderers(meshRenderers);

        // Select first weapon
        if (weaponOne != null)
        {
            aquiredWeaponOne = true;
            SelectWeapon(0);
        }
        else if (weaponTwo != null)
        {
            aquiredWeaponTwo = true;
            SelectWeapon(1);
        }
        else if (weaponThree != null)
        {
            aquiredWeaponThree = true;
            SelectWeapon(2);
        }
        else if (weaponFour != null)
        {
            aquiredWeaponFour = true;
            SelectWeapon(3);
        }
        else PrintDebugMsg_MonitorPlayerWeapons("No weapons available!");
    }

    /// <summary>
    /// Updates the ammunition UIs.
    /// </summary>
    private void UpdateUI()
    {
        if (weaponOne.GetComponent<ChargeGun>() != null)
        {
            if (chargeSlider != null) chargeSlider.gameObject.SetActive(true);
            if (ammoCounter != null) ammoCounter.gameObject.SetActive(false);
        }
        else
        {
            if (chargeSlider != null) chargeSlider.gameObject.SetActive(true);
            if (ammoCounter != null) ammoCounter.gameObject.SetActive(false);
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_MonitorPlayerWeapons(string msg)
    {
        if (isDebug_MonitorPlayerWeapons) Debug.Log(isDebugScriptName_MonitorPlayerWeapons + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_MonitorPlayerWeapons(string msg)
    {
        Debug.LogWarning(isDebugScriptName_MonitorPlayerWeapons + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_MonitorPlayerWeapons(string msg)
    {
        Debug.LogError(isDebugScriptName_MonitorPlayerWeapons + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_MonitorPlayerWeapons("Debugging enabled.");

        Setup();
    }
    #endregion
}