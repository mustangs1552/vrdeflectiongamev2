﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Misc.MyMath;

/// <summary>
/// 
/// </summary>
public class MonitorPlayer : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_MonitorPlayer = false;
    private string isDebugScriptName_MonitorPlayer = "MonitorPlayer";
    #endregion

    #region Public
    [Header("Movement and Looking")]
    [SerializeField] private bool usingMouseKeyboard = true;
    [SerializeField] private float speed = 5;
    [SerializeField] private float lookSpeed = 10;
    [SerializeField] private Vector2 lookXLimits = new Vector2(-90, 90);
    [Header("Actions")]
    [SerializeField] private float jumpForce = 1;
    [SerializeField] private float jumpCooldown = .5f;
    [SerializeField] private float fallAcceleration = .01f;
    [SerializeField] private float maxFallVelocity = -10;
    [SerializeField] private float interactRange = 2;
    [SerializeField] private float interactCooldown = .5f;
    [SerializeField] private LayerMask interactableLayers;

    [Header("Mechanics")]
    [SerializeField] private float visibleLength = 2;
    [Tooltip("Minimum height for an edge to spawn a dust cloud when landing back on ground again.")]
    [SerializeField] private float minEdgeHeight = 2;
    [SerializeField] private GameObject jumpingDustCloud = null;
    [SerializeField] private GameObject walkingDustCloud = null;
    [SerializeField] private float walkingDustCloudFrequency = 1;
    #endregion

    #region Private
    private Camera cam = null;
    private Rigidbody rigidbodyComp = null;
    private int playerNum = 0;
    private bool movementFrozen = false;
    private bool lookingFrozen = false;
    private bool actionsFrozen = false;
    private bool grounded = true;
    private bool generateCloud = false;
    private MonitorPlayerWeapons weapons = null;
    private List<MeshRenderer> meshRenderers = new List<MeshRenderer>();
    private bool isVisible = true;
    private float lastStep = 0;
    private bool triggerDown = false;
    private float currRotX = 0;
    private float lastJump = 0;
    private float lastInteraction = 0;
    private float currVelocityInc = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Make the player visible again.
    /// </summary>
    /// <param name="autoHide">Make the player invisible again automatically after a delay.</param>
    public void BecomeVisible(bool autoHide = true)
    {
        foreach (MeshRenderer renderer in meshRenderers) renderer.enabled = true;

        if (autoHide)
        {
            if (IsInvoking("BecomeInvisible")) CancelInvoke("BecomeInvisible");
            Invoke("BecomeInvisible", visibleLength);
        }
    }
    /// <summary>
    /// Make the player invisible.
    /// </summary>
    public void BecomeInvisible()
    {
        foreach (MeshRenderer renderer in meshRenderers) renderer.enabled = false;
    }

    /// <summary>
    /// Add the meshrenderers to this player's list and hide the player.
    /// </summary>
    /// <param name="renderers">The renderers to add.</param>
    public void SetRenderers(List<MeshRenderer> renderers)
    {
        foreach (MeshRenderer renderer in renderers) meshRenderers.Add(renderer);
        if (isVisible) BecomeVisible(false);
        else BecomeInvisible();
    }

    public void Death()
    {
        MonitorPlayerManager.SINGLETON.PlayerDied(playerNum);
        transform.Translate(Vector3.down * 100);
        movementFrozen = true;
        lookingFrozen = true;
        actionsFrozen = true;
    }
    #endregion

    #region Private
    /// <summary>
    /// Finds and sets variables.
    /// </summary>
    private void SetUp()
    {
        weapons = GetComponent<MonitorPlayerWeapons>();
        if (weapons == null) PrintErrorDebugMsg_MonitorPlayer("No MonitorPlayerWeapons found!");

        rigidbodyComp = GetComponent<Rigidbody>();
        if (rigidbodyComp == null) PrintErrorDebugMsg_MonitorPlayer("no Rigidbody found!");

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform currChild = transform.GetChild(i);

            if (currChild.name == "Camera") cam = currChild.GetComponent<Camera>();

            if (currChild.GetComponent<MeshRenderer>() != null) meshRenderers.Add(currChild.GetComponent<MeshRenderer>());
        }
        if (cam == null) PrintErrorDebugMsg_MonitorPlayer("No camera found!");
        if (meshRenderers.Count == 0) PrintErrorDebugMsg_MonitorPlayer("No meshe renderers found!");

        lastStep = Time.time;

        CheckGrounded();
    }

    /// <summary>
    /// Gets the walking/running input for the current player and performs it. Spawns the walking dust cloud also.
    /// </summary>
    private void Movement()
    {
        float x = 0;
        float z = 0;
        if (usingMouseKeyboard)
        {
            x = Input.GetAxis("Horizontal");
            z = Input.GetAxis("Vertical");
        }
        else
        {
            x = Input.GetAxis("Horizontal_P" + playerNum.ToString());
            z = -Input.GetAxis("Vertical_P" + playerNum.ToString());
        }
        rigidbodyComp.AddForce(transform.right * x * speed);
        rigidbodyComp.AddForce(transform.forward * z * speed);

        if (Time.time - lastStep >= walkingDustCloudFrequency && grounded)
        {
            if (walkingDustCloud != null)
            {
                GameObject spawnedCloud = ObjectManager.SINGLETON.CheckAddObject(walkingDustCloud);
                if (spawnedCloud == null) spawnedCloud = Instantiate(walkingDustCloud, new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2), transform.position.z), Quaternion.identity);
                else
                {
                    spawnedCloud.transform.position = new Vector3(transform.position.x, transform.position.y - (transform.localScale.y / 2), transform.position.z);
                    spawnedCloud.transform.rotation = Quaternion.identity;
                }
                spawnedCloud.SetActive(true);
            }
            lastStep = Time.time;
        }
    }
    /// <summary>
    /// Gets the mouse/joystick input for looking around.
    /// </summary>
    private void Look()
    {
        float x = 0;
        float y = 0;
        if (usingMouseKeyboard)
        {
            x = Input.GetAxis("MouseX");
            y = -Input.GetAxis("MouseY");
        }
        else
        {
            x = Input.GetAxis("LookX_P" + playerNum.ToString());
            y = Input.GetAxis("LookY_P" + playerNum.ToString());
        }
        cam.transform.Rotate(new Vector3(MyMath.Exponent(y, 3) * lookSpeed, 0, 0));
        transform.Rotate(new Vector3(0, MyMath.Exponent(x, 3) * lookSpeed, 0));

        currRotX += MyMath.Exponent(y, 3) * lookSpeed;
        if (currRotX > lookXLimits.y)
        {
            cam.transform.Rotate(new Vector3(-(MyMath.Exponent(y, 3) * lookSpeed), 0, 0));
            currRotX += -(MyMath.Exponent(y, 3) * lookSpeed);
        }
        else if (currRotX < lookXLimits.x)
        {
            cam.transform.Rotate(new Vector3(-(MyMath.Exponent(y, 3) * lookSpeed), 0, 0));
            currRotX += -(MyMath.Exponent(y, 3) * lookSpeed);
        }
    }
    /// <summary>
    /// Makes the player jump.
    /// </summary>
    private void Jump()
    {
        PrintDebugMsg_MonitorPlayer("Player " + playerNum + " is jumping...");
        rigidbodyComp.AddForce(Vector3.up * jumpForce);
        generateCloud = true;
        lastJump = Time.time;
    }

    /// <summary>
    /// The player started firing.
    /// </summary>
    private void FireDown()
    {
        PrintDebugMsg_MonitorPlayer("  Started firing.");
        weapons.FireWeapon(WeaponAction.Start);
        triggerDown = true;
        BecomeVisible();
    }
    /// <summary>
    /// The player is still firing.
    /// </summary>
    private void Firing()
    {
        PrintDebugMsg_MonitorPlayer("  Firing...");
        weapons.FireWeapon(WeaponAction.Continue);
        BecomeVisible();
    }
    /// <summary>
    /// The player stopped firing.
    /// </summary>
    private void FireUp()
    {
        PrintDebugMsg_MonitorPlayer("  Stopped firing.");
        weapons.FireWeapon(WeaponAction.Stop);
        triggerDown = false;
    }
    
    /// <summary>
    /// Use the object the player is currently looking at.
    /// </summary>
    private void Interact()
    {
        PrintDebugMsg_MonitorPlayer("Player " + playerNum + " is trying to interact with...");
        
        RaycastHit hit;
        if(Physics.Raycast(new Ray(cam.transform.position, cam.transform.forward), out hit, interactRange, interactableLayers))
        {
            PrintDebugMsg_MonitorPlayer("  ..." + hit.transform.name);
            if (Vector3.Distance(transform.position, hit.transform.position) <= interactRange)
            {
                if (hit.transform.GetComponent<PickUp>() != null)
                {
                    PrintDebugMsg_MonitorPlayer("Picking up an item...");
                    hit.transform.GetComponent<PickUp>().OnPickUp(gameObject);
                }
            }
        }

        lastInteraction = Time.time;
    }

    /// <summary>
    /// Checks to see if the player provided input from the mouse and keyboard.
    /// </summary>
    private void CheckGameplayInputMK()
    {
        if (AssassinGameModeManager.SINGLETON != null && AssassinGameModeManager.SINGLETON.State == AssassinGameModeState.PlayerInput)
        {
            if (Input.GetAxis("Jump") != 0)
            {
                AssassinGameModeManager.SINGLETON.PlayerReady(playerNum);
                lastJump = Time.time;
            }
        }
        else
        {
            if (!movementFrozen)
            {
                if (Mathf.Abs(Input.GetAxis("Horizontal")) >= .2f || Mathf.Abs(Input.GetAxis("Vertical")) >= .2f) Movement();
                if (Input.GetAxis("Jump") != 0 && grounded && Time.time - lastJump >= jumpCooldown) Jump();
            }
            if (!lookingFrozen && (Mathf.Abs(Input.GetAxis("MouseX")) >= 0 || Mathf.Abs(Input.GetAxis("MouseY")) >= 0)) Look();

            if (!actionsFrozen)
            {
                if (Input.GetAxis("Fire1") >= .2f)
                {
                    PrintDebugMsg_MonitorPlayer("Player " + playerNum + " fired.");
                    if (!triggerDown) FireDown();
                    else Firing();
                }
                else if (triggerDown) FireUp();

                if (Input.GetAxis("Weapon1") != 0) weapons.SelectWeapon(0);
                if (Input.GetAxis("Weapon2") != 0) weapons.SelectWeapon(1);
                if (Input.GetAxis("Weapon3") != 0) weapons.SelectWeapon(2);
                if (Input.GetAxis("Weapon4") != 0) weapons.SelectWeapon(3);

                if (Input.GetAxis("Interact") != 0 && Time.time - lastInteraction >= interactCooldown) Interact();
            }
        }
    }
    /// <summary>
    /// Checks to see if the player provided input from a controller.
    /// </summary>
    private void CheckGameplayInputController()
    {
        if (AssassinGameModeManager.SINGLETON != null && AssassinGameModeManager.SINGLETON.State == AssassinGameModeState.PlayerInput)
        {
            if (Input.GetAxis("Jump_P" + playerNum.ToString()) != 0)
            {
                AssassinGameModeManager.SINGLETON.PlayerReady(playerNum);
                lastJump = Time.time;
            }
        }
        else
        {
            if (!movementFrozen)
            {
                if (Mathf.Abs(Input.GetAxis("Horizontal_P" + playerNum.ToString())) >= .2f || Mathf.Abs(Input.GetAxis("Vertical_P" + playerNum.ToString())) >= .2f) Movement();
                if (Input.GetAxis("Jump_P" + playerNum.ToString()) != 0 && grounded && Time.time - lastJump >= jumpCooldown) Jump();
            }
            if (!lookingFrozen && (Mathf.Abs(Input.GetAxis("LookX_P" + playerNum.ToString())) >= 0 || Mathf.Abs(Input.GetAxis("LookY_P" + playerNum.ToString())) >= 0)) Look();

            if (!actionsFrozen)
            {
                if (Input.GetAxis("Fire1_P" + playerNum.ToString()) >= .2f)
                {
                    PrintDebugMsg_MonitorPlayer("Player " + playerNum + " fired.");
                    if (!triggerDown) FireDown();
                    else Firing();
                }
                else if (triggerDown) FireUp();

                if (Input.GetAxis("DPadX_P" + playerNum.ToString()) < 0) weapons.SelectWeapon(3);
                if (Input.GetAxis("DPadX_P" + playerNum.ToString()) > 0) weapons.SelectWeapon(1);
                if (Input.GetAxis("DPadY_P" + playerNum.ToString()) > 0) weapons.SelectWeapon(0);
                if (Input.GetAxis("DPadY_P" + playerNum.ToString()) < 0) weapons.SelectWeapon(2);

                if (Input.GetAxis("Interact_P" + playerNum.ToString()) != 0 && Time.time - lastInteraction >= interactCooldown) Interact();
            }
        }
    }

    /// <summary>
    /// Checks to see if the player is on the ground.
    /// </summary>
    private void CheckGrounded()
    {
        if (!grounded)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                if (hit.transform.tag == "Ground" && Vector3.Distance(transform.position, hit.point) <= transform.localScale.y) grounded = true;
            }
        }
        else
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                if (hit.transform.tag == "Ground" && Vector3.Distance(transform.position, hit.point) <= transform.localScale.y) grounded = true;
                else grounded = false;
            }
            else grounded = false;
        }
    }

    private void ApplyGravity()
    {
        if (rigidbodyComp.velocity.y >= 0) PrintDebugMsg_MonitorPlayer("Not falling yet.");
        else if (rigidbodyComp.velocity.y >= -maxFallVelocity)
        {
            PrintDebugMsg_MonitorPlayer("Accelerating falling...");
            rigidbodyComp.AddForce(Vector3.down * currVelocityInc, ForceMode.VelocityChange);
            currVelocityInc += fallAcceleration;
        }
        else PrintDebugMsg_MonitorPlayer("At max velocity.");
    }
    #endregion

    #region Unity Functions
    /// <summary>
    /// Check to see if we landed on the ground.
    /// </summary>
    private void OnCollisionEnter(Collision collision)
    {
        if (!grounded && collision.gameObject.tag == "Ground" && collision.contacts[0].point.y < transform.position.y)
        {
            PrintDebugMsg_MonitorPlayer("Collided with " + collision.gameObject.name + ".");
            grounded = true;
            if (jumpingDustCloud != null && generateCloud)
            {
                GameObject spawnedCloud = ObjectManager.SINGLETON.CheckAddObject(jumpingDustCloud);
                if (spawnedCloud == null) spawnedCloud = Instantiate(jumpingDustCloud, collision.contacts[0].point, Quaternion.identity);
                else
                {
                    spawnedCloud.transform.position = collision.contacts[0].point;
                    spawnedCloud.transform.rotation = Quaternion.identity;
                }
                spawnedCloud.SetActive(true);
            }
            generateCloud = false;

            currVelocityInc = 0;
        }
    }
    /// <summary>
    /// Check to see if we left the ground.
    /// </summary>
    private void OnCollisionExit(Collision collision)
    {
        if (grounded && collision.gameObject.tag == "Ground")
        {
            PrintDebugMsg_MonitorPlayer("Stopped colliding with " + collision.gameObject.name + ".");
            grounded = false;

            RaycastHit hit;
            if(!generateCloud && Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                if (hit.transform.tag == "Ground" && Vector3.Distance(transform.position, hit.point) > transform.localScale.y * (1 + minEdgeHeight)) generateCloud = true;
            }
        }
    }
    #endregion

    #region Debug
    /// <summary>
    /// Draw rays and lines showing ranges.
    /// </summary>
    private void ShowRanges()
    {
        if (isDebug_MonitorPlayer)
        {
            Debug.DrawRay(cam.transform.position, cam.transform.forward * interactRange, Color.green);

            Debug.DrawRay(transform.position, Vector3.down * transform.localScale.y * (1 + minEdgeHeight));
        }
    }

    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_MonitorPlayer(string msg)
    {
        if (isDebug_MonitorPlayer) Debug.Log(isDebugScriptName_MonitorPlayer + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_MonitorPlayer(string msg)
    {
        Debug.LogWarning(isDebugScriptName_MonitorPlayer + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_MonitorPlayer(string msg)
    {
        Debug.LogError(isDebugScriptName_MonitorPlayer + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// The player's camera.
    /// </summary>
    public Camera Cam
    {
        get
        {
            return cam;
        }
    }
    /// <summary>
    /// The player's number.
    /// </summary>
    public int PlayerNum
    {
        get
        {
            return playerNum;
        }
        set
        {
            playerNum = value;
        }
    }

    /// <summary>
    /// Is the player using mouse and keyboard?
    /// </summary>
    public bool UsingMouseKeyboard
    {
        get
        {
            return usingMouseKeyboard;
        }
        set
        {
            usingMouseKeyboard = value;
        }
    }

    /// <summary>
    /// Freeze player's movement and jumping.
    /// </summary>
    public bool MovementFrozen
    {
        get
        {
            return movementFrozen;
        }
        set
        {
            movementFrozen = value;
        }
    }
    /// <summary>
    /// Freeze the player's ability to look around.
    /// </summary>
    public bool LookingFrozen
    {
        get
        {
            return lookingFrozen;
        }
        set
        {
            lookingFrozen = value;
        }
    }
    /// <summary>
    /// Freezes all actions except movement and looking.
    /// </summary>
    public bool ActionsFrozen
    {
        get
        {
            return actionsFrozen;
        }
        set
        {
            actionsFrozen = value;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_MonitorPlayer("Debugging enabled.");

        SetUp();
    }
    private void FixedUpdate()
    {
        if (usingMouseKeyboard) CheckGameplayInputMK();
        else CheckGameplayInputController();
        
        ShowRanges();

        if (!grounded) ApplyGravity();
    }
    #endregion
}