﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class VRPlayer : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_VRPlayer = false;
    private string isDebugScriptName_VRPlayer = "VRPlayer";
    #endregion

    #region Public
    [SerializeField] private PlasmaSword leftPSword = null;
    [SerializeField] private PlasmaSword rightPSword = null;
    [SerializeField] private AudioClip hapticClip = null;
    [SerializeField] private Image damageOverlay = null;
    [SerializeField] private float damageOverlayFadeSpeed = 1;
    #endregion

    #region Private
    private OVRHapticsClip ovrHapticClip = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Kill the VR player.
    /// </summary>
    public void Death()
    {
        PrintDebugMsg_VRPlayer("Died!");
        if(TrainingGameModeManager.SINGLETON != null) TrainingGameModeManager.SINGLETON.EndRound(TrainingModeEndRoundReason.VRPlayerDied);
        if (AssassinGameModeManager.SINGLETON != null) AssassinGameModeManager.SINGLETON.VRPlayerDied();
    }
    /// <summary>
    /// Main health was adjusted.
    /// </summary>
    /// <param name="amount">Amount adjusted.</param>
    public void HealthAdjusted(float amount)
    {
        if (amount < 0) ShowDamageOverlay();
    }

    /// <summary>
    /// Reset the VR player.
    /// </summary>
    public void ResetVRPlayer()
    {
        GetComponent<MainHealth>().ResetHealth();
        HideDamageOverlay();
    }

    /// <summary>
    /// Generates a haptic feedback for the given hand.
    /// </summary>
    /// <param name="isRight">Is right hand, else left hand.</param>
    public void GenerateHapticFeedback(bool isRight)
    {
        if (ovrHapticClip != null)
        {
            if (isRight) OVRHaptics.RightChannel.Preempt(ovrHapticClip);
            else OVRHaptics.LeftChannel.Preempt(ovrHapticClip);
        }
    }

    /// <summary>
    /// Hide given sber for single saber mode.
    /// </summary>
    /// <param name="useRight">Keep right hand on.</param>
    public void EnableSingleSaberMode(bool useRight)
    {
        if (useRight) leftPSword.DisableEnergy();
        else rightPSword.DisableEnergy();
    }
    /// <summary>
    /// Show both sabers.
    /// </summary>
    public void DisableSingleSaberMode()
    {
        leftPSword.EnableEnergy();
        rightPSword.EnableEnergy();
    }
    #endregion

    #region Private
    /// <summary>
    /// Check the VR player's input and perform desired action accordingly.
    /// </summary>
    private void CheckInput()
    {
        if (OVRInput.GetUp(OVRInput.Button.Start)) TrainingGameModeManager.SINGLETON.PauseResume();

        if (OVRInput.GetUp(OVRInput.Button.One))
        {
            if (AssassinGameModeManager.SINGLETON != null)
            {
                if (AssassinGameModeManager.SINGLETON.State == AssassinGameModeState.PlayerInput) AssassinGameModeManager.SINGLETON.PlayerReady();
            }

            if (isDebug_VRPlayer)
            {
                if (TrainingGameModeManager.SINGLETON != null)
                {
                    switch (TrainingGameModeManager.SINGLETON.State)
                    {
                        case TraingingGameModeState.PreGame:
                            TrainingGameModeManager.SINGLETON.StartRound();
                            break;
                        case TraingingGameModeState.Playing:
                            TrainingGameModeManager.SINGLETON.EndRound();
                            break;
                        case TraingingGameModeState.PostGame:
                            TrainingGameModeManager.SINGLETON.RestartRound();
                            break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Shows the damage overlay.
    /// </summary>
    private void ShowDamageOverlay()
    {
        if (damageOverlay != null)
        {
            damageOverlay.color = new Color(damageOverlay.color.r, damageOverlay.color.g, damageOverlay.color.b, 1);
            damageOverlay.gameObject.SetActive(true);
        }
    }
    /// <summary>
    /// Fades the damage overlay out overtime.
    /// </summary>
    private void DamageOverlayFadeOut()
    {
        if (damageOverlay != null)
        {
            damageOverlay.color = new Color(damageOverlay.color.r, damageOverlay.color.g, damageOverlay.color.b, damageOverlay.color.a - damageOverlayFadeSpeed * Time.deltaTime);
            if (damageOverlay.color.a <= 0) HideDamageOverlay();
        }
    }
    /// <summary>
    /// Hide the damage overlay.
    /// </summary>
    private void HideDamageOverlay()
    {
        if(damageOverlay != null) damageOverlay.gameObject.SetActive(false);
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_VRPlayer(string msg)
    {
        if (isDebug_VRPlayer) Debug.Log(isDebugScriptName_VRPlayer + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_VRPlayer(string msg)
    {
        Debug.LogWarning(isDebugScriptName_VRPlayer + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_VRPlayer(string msg)
    {
        Debug.LogError(isDebugScriptName_VRPlayer + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_VRPlayer("Debugging enabled.");

        if(hapticClip != null) ovrHapticClip = new OVRHapticsClip(hapticClip);

        HideDamageOverlay();
    }
    private void Update()
    {
        CheckInput();

        if (damageOverlay.gameObject.activeInHierarchy) DamageOverlayFadeOut();
    }
    #endregion
}