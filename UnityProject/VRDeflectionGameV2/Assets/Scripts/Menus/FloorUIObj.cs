﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class FloorUIObj : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_FloorUIObj = false;
    private string isDebugScriptName_FloorUIObj = "FloorUIObj";
    #endregion

    #region Public
    [SerializeField] private float speed = 1;
    [SerializeField] private GameObject ui = null;
    #endregion

    #region Private
    private float visibleXRot = 0;
    private bool hiding = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Show the object.
    /// </summary>
    /// <param name="force">Force the object straight to its visible position.</param>
    public void Show(bool force = false)
    {
        if (!force)
        {
            PrintDebugMsg_FloorUIObj("Starting to show...");
            hiding = false;
            InvokeRepeating("Move", 0, .01f);
        }
        else
        {
            transform.localEulerAngles = new Vector3(visibleXRot, transform.localEulerAngles.y, transform.localEulerAngles.z);
            NowVisible();
            PrintDebugMsg_FloorUIObj("Now visible.");
        }
    }
    /// <summary>
    /// Hide the object.
    /// </summary>
    /// <param name="force">Force the object straight to its hidden position.</param>
    public void Hide(bool force = false)
    {
        ui.SetActive(false);
        if (!force)
        {
            PrintDebugMsg_FloorUIObj("Starting to hide...");
            hiding = true;
            InvokeRepeating("Move", 0, .01f);
        }
        else
        {
            transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);
            PrintDebugMsg_FloorUIObj("Now hidden.");
        }
    }
    #endregion

    #region Private
    /// <summary>
    /// Set variables.
    /// </summary>
    private void Setup()
    {
        visibleXRot = transform.localEulerAngles.x;
    }

    /// <summary>
    /// Object is done becoming visible.
    /// </summary>
    private void NowVisible()
    {
        ui.SetActive(true);
    }

    /// <summary>
    /// Object is in the process of hiding/showing.
    /// </summary>
    private void Move()
    {
        if (hiding)
        {
            transform.Rotate(new Vector3(-speed * Time.deltaTime, 0, 0));
            if (transform.localEulerAngles.x <= 1)
            {
                PrintDebugMsg_FloorUIObj("Finished hiding.");
                CancelInvoke("Move");
                transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z);
            }
        }
        else
        {
            transform.Rotate(new Vector3(speed * Time.deltaTime, 0, 0));
            if(transform.localEulerAngles.x >= visibleXRot)
            {
                PrintDebugMsg_FloorUIObj("Finished showing.");
                CancelInvoke("Move");
                transform.localEulerAngles = new Vector3(visibleXRot, transform.localEulerAngles.y, transform.localEulerAngles.z);
                NowVisible();
            }
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_FloorUIObj(string msg)
    {
        if (isDebug_FloorUIObj) Debug.Log(isDebugScriptName_FloorUIObj + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_FloorUIObj(string msg)
    {
        Debug.LogWarning(isDebugScriptName_FloorUIObj + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_FloorUIObj(string msg)
    {
        Debug.LogError(isDebugScriptName_FloorUIObj + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_FloorUIObj("Debugging enabled.");

        Setup();

        Hide(true);
    }
    #endregion
}