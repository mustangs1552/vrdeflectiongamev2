﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class MainMenu : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_MainMenu = false;
    private string isDebugScriptName_MainMenu = "MainMenu";
    #endregion

    #region Public
    public static MainMenu SINGLETON = null;

    [SerializeField] private GameObject titleScreen = null;
    [SerializeField] private GameObject creditsScreen = null;
    [SerializeField] private GameObject trainingModeLobbyScreen = null;
    //[SerializeField] private GameObject assassinModeLobbyScreen = null;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Shows the main menu screen.
    /// </summary>
    public void OpenTitleScreen()
    {
        titleScreen.SetActive(true);
        creditsScreen.SetActive(false);
        trainingModeLobbyScreen.SetActive(false);
        //assassinModeLobbyScreen.SetActive(false);
    }
    /// <summary>
    /// Shows the credits screen.
    /// </summary>
    public void OpenCreditsScreen()
    {
        titleScreen.SetActive(false);
        creditsScreen.SetActive(true);
        trainingModeLobbyScreen.SetActive(false);
        //assassinModeLobbyScreen.SetActive(false);
    }
    /// <summary>
    /// Shows the training mode screen.
    /// </summary>
    public void OpenTrainingModeLobby()
    {
        titleScreen.SetActive(false);
        creditsScreen.SetActive(false);
        trainingModeLobbyScreen.SetActive(true);
        //assassinModeLobbyScreen.SetActive(false);
    }
    /// <summary>
    /// Shows the assassin mode screen.
    /// </summary>
    public void OpenAssassinModeLobby()
    {
        titleScreen.SetActive(false);
        creditsScreen.SetActive(false);
        trainingModeLobbyScreen.SetActive(false);
        //assassinModeLobbyScreen.SetActive(true);
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_MainMenu(string msg)
    {
        if (isDebug_MainMenu) Debug.Log(isDebugScriptName_MainMenu + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_MainMenu(string msg)
    {
        Debug.LogWarning(isDebugScriptName_MainMenu + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_MainMenu(string msg)
    {
        Debug.LogError(isDebugScriptName_MainMenu + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_MainMenu("Debugging enabled.");

        if (MainMenu.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_MainMenu("More than one MainMenu singletons found!");

        OpenTitleScreen();
    }
    #endregion
}