﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

/// <summary>
/// 
/// </summary>
public class AssassinModeLobbyMenu : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_AssassinModeLobbyMenu = false;
    private string isDebugScriptName_AssassinModeLobbyMenu = "AssassinModeLobbyMenu";
    #endregion

    #region Public
    [SerializeField] private Dropdown playerCountDropdown = null;
    [SerializeField] private Toggle timeEndlessToggle = null;
    [SerializeField] private Dropdown timeLimitDropdown = null;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public
    public void OnTimeEndlessChanged()
    {
        if (timeEndlessToggle.isOn) timeLimitDropdown.interactable = false;
        else timeLimitDropdown.interactable = true;
    }

    public void LoadMatch()
    {
        AssassinModeSettings settings = new AssassinModeSettings();
        settings.playerCount = playerCountDropdown.value + 1;
        if(timeEndlessToggle != null) settings.timeEndless = timeEndlessToggle.isOn;
        settings.timeLimitIndex = timeLimitDropdown.value;

        MatchSettings matchSettings = new GameObject("MatchSettings").AddComponent<MatchSettings>();
        matchSettings.SettingsAssassinmode = settings;
        SceneManager.LoadScene(1);
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_AssassinModeLobbyMenu(string msg)
    {
        if (isDebug_AssassinModeLobbyMenu) Debug.Log(isDebugScriptName_AssassinModeLobbyMenu + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_AssassinModeLobbyMenu(string msg)
    {
        Debug.LogWarning(isDebugScriptName_AssassinModeLobbyMenu + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_AssassinModeLobbyMenu(string msg)
    {
        Debug.LogError(isDebugScriptName_AssassinModeLobbyMenu + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_AssassinModeLobbyMenu("Debugging enabled.");
    }
    #endregion
}

[Serializable]
public class AssassinModeSettings
{
    public int playerCount = 0;
    public bool timeEndless = false;
    public int timeLimitIndex = 0;
}