﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class MatchSettings : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_MatchSettings = false;
    private string isDebugScriptName_MatchSettings = "MatchSettings";
    #endregion

    #region Public
    public static MatchSettings SINGLETON = null;
    #endregion

    #region Private
    private TrainingModeSettings settingsTrainingMode = null;
    private AssassinModeSettings settingsAssassinMode = null;
    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_MatchSettings(string msg)
    {
        if (isDebug_MatchSettings) Debug.Log(isDebugScriptName_MatchSettings + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_MatchSettings(string msg)
    {
        Debug.LogWarning(isDebugScriptName_MatchSettings + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_MatchSettings(string msg)
    {
        Debug.LogError(isDebugScriptName_MatchSettings + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    /// <summary>
    /// The settings for the training mode.
    /// </summary>
    public TrainingModeSettings SettingsTrainingMode
    {
        set
        {
            settingsTrainingMode = value;
        }
        get
        {
            return settingsTrainingMode;
        }
    }
    /// <summary>
    /// The settings for the assassin mode.
    /// </summary>
    public AssassinModeSettings SettingsAssassinmode
    {
        set
        {
            settingsAssassinMode = value;
        }
        get
        {
            return settingsAssassinMode;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_MatchSettings("Debugging enabled.");

        if (MatchSettings.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_MatchSettings("More than one MatchSettings singletons found!");

        DontDestroyOnLoad(gameObject);
    }
    #endregion
}