﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

/// <summary>
/// 
/// </summary>
public class TrainingModeLobbyMenu : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_TrainingModeLobbyMenu = false;
    private string isDebugScriptName_TrainingModeLobbyMenu = "TrainingModeLobbyMenu";
    #endregion

    #region Public
    [Header("Presets")]
    [SerializeField] private TrainingModeSettings easyPreset = new TrainingModeSettings();
    [SerializeField] private TrainingModeSettings mediumPreset = new TrainingModeSettings();
    [SerializeField] private TrainingModeSettings hardPreset = new TrainingModeSettings();

    [Header("UI")]
    [SerializeField] private Toggle use360Toggle = null;
    [SerializeField] private Toggle darknessModeToggle = null;
    [SerializeField] private Toggle leftSingleSaberToggle = null;
    [SerializeField] private Toggle rightSingleSaberToggle = null;
    [SerializeField] private Button turretSpawnMinButton = null;
    [SerializeField] private Button turretSpawnMaxButton = null;
    [SerializeField] private Button newTurretRateButton = null;
    [SerializeField] private Toggle maxTurretsUnlimitedToggle = null;
    [SerializeField] private Button maxTurretsButton = null;
    [SerializeField] private Button easyButton = null;
    [SerializeField] private Button mediumButton = null;
    [SerializeField] private Button hardButton = null;
    [SerializeField] private Toggle oneMinToggle = null;
    [SerializeField] private Toggle twoMinToggle = null;
    [SerializeField] private Toggle threeMinToggle = null;
    [SerializeField] private Toggle timeEndlessToggle = null;
    [SerializeField] private Button startButton = null;
    [SerializeField] private int startButtonSmallerFont = 28;
    #endregion

    #region Private
    private Text turretSpawnMinText = null;
    private Text turretSpawnMaxText = null;
    private Text newTurretRateText = null;
    private Text maxTurretsText = null;

    private Text startButtonText = null;
    private int startButtonStartFont = 0;

    private enum KeypadUses
    {
        MinTurretSpawn,
        MaxTurretSpawn,
        NewTurretRate,
        MaxTurrets,
        None
    }
    private KeypadUses currUse = KeypadUses.None;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Sets the values of the settings according to the currently selected preset.
    /// </summary>
    public void OnPresetChanged(int presetI)
    {
        if(presetI == 0)
        {
            switch (easyPreset.timeLimitValue)
            {
                case 0:
                    oneMinToggle.isOn = true;
                    break;
                case 1:
                    twoMinToggle.isOn = true;
                    break;
                case 2:
                    threeMinToggle.isOn = true;
                    break;
            }
            timeEndlessToggle.isOn = easyPreset.timeEndless;
            turretSpawnMinText.text = easyPreset.turretSpawnRate.x.ToString();
            turretSpawnMaxText.text = easyPreset.turretSpawnRate.y.ToString();
            newTurretRateText.text = easyPreset.newTurretRate.ToString();
            maxTurretsUnlimitedToggle.isOn = easyPreset.unlimitedTurrets;
            if (!maxTurretsUnlimitedToggle.isOn) maxTurretsText.text = easyPreset.maxTurrets.ToString();
        }
        else if(presetI == 1)
        {
            switch (mediumPreset.timeLimitValue)
            {
                case 0:
                    oneMinToggle.isOn = true;
                    break;
                case 1:
                    twoMinToggle.isOn = true;
                    break;
                case 2:
                    threeMinToggle.isOn = true;
                    break;
            }
            timeEndlessToggle.isOn = mediumPreset.timeEndless;
            turretSpawnMinText.text = easyPreset.turretSpawnRate.x.ToString();
            turretSpawnMaxText.text = mediumPreset.turretSpawnRate.y.ToString();
            newTurretRateText.text = mediumPreset.newTurretRate.ToString();
            maxTurretsUnlimitedToggle.isOn = mediumPreset.unlimitedTurrets;
            if (!maxTurretsUnlimitedToggle.isOn) maxTurretsText.text = mediumPreset.maxTurrets.ToString();
        }
        else if(presetI == 2)
        {
            switch (hardPreset.timeLimitValue)
            {
                case 0:
                    oneMinToggle.isOn = true;
                    break;
                case 1:
                    twoMinToggle.isOn = true;
                    break;
                case 2:
                    threeMinToggle.isOn = true;
                    break;
            }
            timeEndlessToggle.isOn = hardPreset.timeEndless;
            turretSpawnMinText.text = easyPreset.turretSpawnRate.x.ToString();
            turretSpawnMaxText.text = hardPreset.turretSpawnRate.y.ToString();
            newTurretRateText.text = hardPreset.newTurretRate.ToString();
            maxTurretsUnlimitedToggle.isOn = hardPreset.unlimitedTurrets;
            if (!maxTurretsUnlimitedToggle.isOn) maxTurretsText.text = hardPreset.maxTurrets.ToString();
        }
    }

    /// <summary>
    /// Marks the preset as custom when a setting is changed and disables time limit and max turrets inputs when thier toggles are enabled.
    /// </summary>
    public void OnInputChanged()
    {
        if (maxTurretsUnlimitedToggle.isOn) maxTurretsButton.interactable = false;
        else maxTurretsButton.interactable = true;
    }
    
    /// <summary>
    /// Minimum turret spawn input button was pressed.
    /// </summary>
    public void MinTurretSpawnInput()
    {
        Keypad.SINGLETON.Activate(gameObject, false, false);
        currUse = KeypadUses.MinTurretSpawn;
        DisableUIInput();
    }
    /// <summary>
    /// Maximum turret spawn input button was pressed.
    /// </summary>
    public void MaxTurretSpawnInput()
    {
        Keypad.SINGLETON.Activate(gameObject, false, false);
        currUse = KeypadUses.MaxTurretSpawn;
        DisableUIInput();
    }
    /// <summary>
    /// New turret spawn rate input button was pressed.
    /// </summary>
    public void NewTurretInput()
    {
        Keypad.SINGLETON.Activate(gameObject, false, false);
        currUse = KeypadUses.NewTurretRate;
        DisableUIInput();
    }
    /// <summary>
    /// Max turrets input button was pressed.
    /// </summary>
    public void MaxTurretsInput()
    {
        Keypad.SINGLETON.Activate(gameObject, true, false);
        currUse = KeypadUses.MaxTurrets;
        DisableUIInput();
    }

    /// <summary>
    /// Loads the match after setting the match's settings.
    /// </summary>
    public void LoadMatch()
    {
        MatchSettings matchSettings = new GameObject("MatchSettings").AddComponent<MatchSettings>();
        matchSettings.SettingsTrainingMode = CurrSettings;
        
        TrainingGameModeManager.SINGLETON.SetUpGame();
        MainMenu.SINGLETON.gameObject.SetActive(false);
        TurretManager.SINGLETON.Invoke("MarkRerunSetup", 1);
    }

    /// <summary>
    /// Makes UI elements not interactable.
    /// </summary>
    public void DisableUIInput()
    {
        use360Toggle.interactable = false;
        darknessModeToggle.interactable = false;
        leftSingleSaberToggle.interactable = false;
        rightSingleSaberToggle.interactable = false;
        easyButton.interactable = false;
        mediumButton.interactable = false;
        hardButton.interactable = false;
        oneMinToggle.interactable = false;
        twoMinToggle.interactable = false;
        threeMinToggle.interactable = false;
        timeEndlessToggle.interactable = false;
        turretSpawnMinButton.interactable = false;
        turretSpawnMaxButton.interactable = false;
        newTurretRateButton.interactable = false;
        maxTurretsUnlimitedToggle.interactable = false;
        maxTurretsButton.interactable = false;
        startButton.interactable = false;
    }
    /// <summary>
    /// Makes UI elements interactable.
    /// </summary>
    public void EnableUIInput()
    {
        use360Toggle.interactable = true;
        darknessModeToggle.interactable = true;
        leftSingleSaberToggle.interactable = true;
        rightSingleSaberToggle.interactable = true;
        easyButton.interactable = true;
        mediumButton.interactable = true;
        hardButton.interactable = true;
        oneMinToggle.interactable = true;
        twoMinToggle.interactable = true;
        threeMinToggle.interactable = true;
        timeEndlessToggle.interactable = true;
        turretSpawnMinButton.interactable = true;
        turretSpawnMaxButton.interactable = true;
        newTurretRateButton.interactable = true;
        maxTurretsUnlimitedToggle.interactable = true;
        if(!maxTurretsUnlimitedToggle.isOn) maxTurretsButton.interactable = true;
        startButton.interactable = true;
    }
    #endregion

    #region Private
    /// <summary>
    /// Sets up some variables.
    /// </summary>
    private void SetUp()
    {
        turretSpawnMinText = turretSpawnMinButton.GetComponentInChildren<Text>();
        turretSpawnMaxText = turretSpawnMaxButton.GetComponentInChildren<Text>();
        newTurretRateText = newTurretRateButton.GetComponentInChildren<Text>();
        maxTurretsText = maxTurretsButton.GetComponentInChildren<Text>();

        OnPresetChanged(1);

        startButtonText = startButton.GetComponentInChildren<Text>();
        if (startButtonText == null) PrintErrorDebugMsg_TrainingModeLobbyMenu("No start button text found!");
        else startButtonStartFont = startButtonText.fontSize;
    }
    
    /// <summary>
    /// Keypad input received. Assign the input after making sure that it is valid.
    /// </summary>
    /// <param name="input">The input received.</param>
    private void KeypadInputDone(string input)
    {
        if (input.Length > 0)
        {
            string confirmedGood = "";
            bool decimalExists = false;
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == '.')
                {
                    if (!decimalExists)
                    {
                        confirmedGood += input[i];
                        decimalExists = true;
                    }
                }
                else confirmedGood += input[i];
            }

            switch (currUse)
            {
                case KeypadUses.MinTurretSpawn:
                    turretSpawnMinText.text = confirmedGood.ToString();
                    break;
                case KeypadUses.MaxTurretSpawn:
                    turretSpawnMaxText.text = confirmedGood.ToString();
                    break;
                case KeypadUses.NewTurretRate:
                    newTurretRateText.text = confirmedGood.ToString();
                    break;
                case KeypadUses.MaxTurrets:
                    maxTurretsText.text = confirmedGood.ToString();
                    break;
            }
        }

        EnableUIInput();
        currUse = KeypadUses.None;
    }
    #endregion

    #region Unity Functions
    
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_TrainingModeLobbyMenu(string msg)
    {
        if (isDebug_TrainingModeLobbyMenu) Debug.Log(isDebugScriptName_TrainingModeLobbyMenu + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_TrainingModeLobbyMenu(string msg)
    {
        Debug.LogWarning(isDebugScriptName_TrainingModeLobbyMenu + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_TrainingModeLobbyMenu(string msg)
    {
        Debug.LogError(isDebugScriptName_TrainingModeLobbyMenu + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    /// <summary>
    /// The current settings of the UI.
    /// </summary>
    public TrainingModeSettings CurrSettings
    {
        get
        {
            TrainingModeSettings settings = new TrainingModeSettings();
            settings.Use360SetUp = use360Toggle.isOn;
            settings.DarknessMode = darknessModeToggle.isOn;

            if (!leftSingleSaberToggle.isOn && !rightSingleSaberToggle.isOn) settings.singleSaberMode = 0;
            else if (rightSingleSaberToggle.isOn) settings.singleSaberMode = 1;
            else if (leftSingleSaberToggle.isOn) settings.singleSaberMode = 2;

            settings.timeEndless = timeEndlessToggle.isOn;
            if (oneMinToggle.isOn) settings.timeLimitValue = 0;
            else if (twoMinToggle.isOn) settings.timeLimitValue = 1;
            else if (threeMinToggle.isOn) settings.timeLimitValue = 2;

            settings.turretSpawnRate = new Vector2(float.Parse(turretSpawnMinText.text), float.Parse(turretSpawnMaxText.text));
            settings.newTurretRate = float.Parse(newTurretRateText.text);
            settings.unlimitedTurrets = maxTurretsUnlimitedToggle.isOn;
            settings.maxTurrets = int.Parse(maxTurretsText.text);

            return settings;
        }
    }

    /// <summary>
    /// The easy preset's settings.
    /// </summary>
    public TrainingModeSettings EasyPreset
    {
        get
        {
            return easyPreset;
        }
    }
    /// <summary>
    /// The medium preset's settings.
    /// </summary>
    public TrainingModeSettings MediumPreset
    {
        get
        {
            return mediumPreset;
        }
    }
    /// <summary>
    /// The hard preset's settigns.
    /// </summary>
    public TrainingModeSettings HardPreset
    {
        get
        {
            return hardPreset;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_TrainingModeLobbyMenu("Debugging enabled.");

        SetUp();
    }
    private void Update()
    {
        if (!TurretPillarManager.SINGLETON.PillarsMoving && !Keypad.SINGLETON.gameObject.activeInHierarchy)
        {
            startButton.interactable = true;
            if (startButtonText != null)
            {
                startButtonText.text = "Start";
                startButtonText.fontSize = startButtonStartFont;
            }
        }
        else
        {
            startButton.interactable = false;
            if (startButtonText != null && TurretPillarManager.SINGLETON.PillarsMoving)
            {
                startButtonText.text = "Lowering Pillers...";
                startButtonText.fontSize = startButtonSmallerFont;
            }
        }
    }
    #endregion
}

/// <summary>
/// Settings for the training mode presets.
/// </summary>
[Serializable]
public class TrainingModeSettings
{
    public bool timeEndless = false;
    public int timeLimitValue = 0;
    public Vector2 turretSpawnRate = new Vector2(0, 5);
    public float newTurretRate = 5;
    public bool unlimitedTurrets = false;
    public int maxTurrets = 10;

    private bool use360SetUp = false;
    private bool darknessMode = false;
    public bool Use360SetUp
    {
        get
        {
            return use360SetUp;
        }
        set
        {
            use360SetUp = value;
        }
    }
    public bool DarknessMode
    {
        get
        {
            return darknessMode;
        }
        set
        {
            darknessMode = value;
        }
    }
    /// <summary>
    /// Single saber mode. 0 = Disabled; 1 = Enabled, use right; 2 = Enabled, use left
    /// </summary>
    public int singleSaberMode = 0;
}