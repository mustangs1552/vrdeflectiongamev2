﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class TurretManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_TurretManager = false;
    private string isDebugScriptName_TurretManager = "TurretManager";
    #endregion

    #region Public
    public static TurretManager SINGLETON = null;

    [SerializeField] private GameObject turret = null;
    [SerializeField] private float newTurretInterval = 5;
    [SerializeField] private int maxTurrets = 5;
    #endregion

    #region Private
    private List<Transform> turretSpawnpoints = new List<Transform>();
    private List<bool> spawnpointUsed = new List<bool>();
    private List<Turret> turrets = new List<Turret>();
    private Transform turretGroup = null;

    private int currTotalAvailable = 0;
    private float lastNewTurretTime = 0;

    private bool ranSetUp = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Start adding new turrets to the max for new round.
    /// </summary>
    public void StartRound()
    {
        InvokeRepeating("AddToMaxTurret", 0, newTurretInterval);
    }
    /// <summary>
    /// Reset turret counts and objects to prepare for new round.
    /// </summary>
    public void ResetRound()
    {
        CancelInvoke("AddToMaxTurret");
        ResetObjects();
        currTotalAvailable = 0;
    }
    /// <summary>
    /// Prepare to re-run the setup.
    /// </summary>
    public void MarkRerunSetup()
    {
        ranSetUp = false;
        foreach (Turret turret in turrets) ObjectManager.SINGLETON.DestroyObject(turret.gameObject);
        turrets = new List<Turret>();
        turretSpawnpoints = new List<Transform>();
        spawnpointUsed = new List<bool>();
    }

    /// <summary>
    /// After checking various conditions, spawns a turret at the given spawnpoint.
    /// </summary>
    /// <param name="spawnpoint">The spawnpoint to spawn at (TurretManager must know about spawnpoint).</param>
    /// <returns>True if successfull.</returns>
    public bool SpawnTurret(Transform spawnpoint)
    {
        PrintDebugMsg_TurretManager("Attempting to spawn a turret at " + spawnpoint.name + "...");

        if(CheckTurretMax()) PrintDebugMsg_TurretManager("  Max turrets already active!");
        else if (spawnpoint.tag != "TurretSpawnpoint") PrintDebugMsg_TurretManager("  Can only spawn turrets at turret spawnpoints!");
        else
        {
            for(int i = 0; i < turretSpawnpoints.Count; i++)
            {
                if(turretSpawnpoints[i] == spawnpoint)
                {
                    if (spawnpointUsed[i])
                    {
                        PrintDebugMsg_TurretManager("  Spawnpoint already in use!");
                        break;
                    }
                    else
                    {
                        Turret spawnedTurret = CheckForAvailableTurret();
                        if (turret != null)
                        {
                            spawnedTurret.transform.position = spawnpoint.position;
                            spawnedTurret.transform.rotation = spawnpoint.rotation;
                            spawnedTurret.gameObject.SetActive(true);
                            spawnedTurret.RevealAndFire(spawnpoint);
                            spawnpointUsed[i] = true;
                            
                            PrintDebugMsg_TurretManager("Spawned a turret!");
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /// <summary>
    /// INTENDED FOR TURRETS! Called from the turrets to tell the TurretManager to make thier spawnpoint available again.
    /// </summary>
    /// <param name="spawnpoint">The spawnpoint used by the turret.</param>
    public void MakeSpawnpointAvailable(Transform spawnpoint)
    {
        PrintDebugMsg_TurretManager("Making a spawnpoint available...");
        for(int i = 0; i < turretSpawnpoints.Count; i++)
        {
            if(spawnpoint == turretSpawnpoints[i])
            {
                PrintDebugMsg_TurretManager("  Matching spawnpoint found!");
                spawnpointUsed[i] = false;
                return;
            }
        }

        PrintErrorDebugMsg_TurretManager("Spawnpoint not found in list!");
    }

    /// <summary>
    /// Hide/show the particles of the levitation device of each turret.
    /// </summary>
    public void UpdateTurretLevitationParticles(bool darknessMode)
    {
        foreach (Turret turret in turrets) turret.UpdateLevitaionDeviceParticles(darknessMode);
    }

    /// <summary>
    /// Set the setting for the match from the main menu.
    /// </summary>
    public void SetSettings()
    {
        TrainingModeSettings settings = MatchSettings.SINGLETON.SettingsTrainingMode;
        newTurretInterval = settings.newTurretRate;
        if (newTurretInterval <= 0) newTurretInterval = .1f;
        if (settings.unlimitedTurrets) maxTurrets = 0;
        else maxTurrets = settings.maxTurrets;

        PrintDebugMsg_TurretManager("SETTINGS | New Turret Interval: " + newTurretInterval + " | Max Turrets: " + maxTurrets);
    }
    #endregion

    #region Private
    /// <summary>
    /// Finds all the turret spawnpoints and spawns a turret for each spawnpoint found.
    /// </summary>
    private void SetUp()
    {
        PrintDebugMsg_TurretManager("Setting up...");
        
        turretGroup = new GameObject("TurretGroup").transform;

        GameObject[] foundSpawns = GameObject.FindGameObjectsWithTag("TurretSpawnpoint");
        foreach (GameObject spawn in foundSpawns)
        {
            turretSpawnpoints.Add(spawn.transform);
            spawnpointUsed.Add(new bool());
        }
        PrintDebugMsg_TurretManager("  Found " + turretSpawnpoints.Count + " turret spawnpoint(s).");
        if (maxTurrets > turretSpawnpoints.Count) maxTurrets = turretSpawnpoints.Count;
        
        foreach(Transform spawn in turretSpawnpoints)
        {
            GameObject spawnedTurret = ObjectManager.SINGLETON.CheckAddObject(turret);
            if(spawnedTurret == null) spawnedTurret = Instantiate(turret, turretGroup);
            spawnedTurret.GetComponent<Turret>().SetupLevitationDevice();
            spawnedTurret.gameObject.SetActive(false);
            turrets.Add(spawnedTurret.GetComponent<Turret>());
        }
        PrintDebugMsg_TurretManager("  Spawned " + turrets.Count + " turret(s).");

        ranSetUp = true;
    }

    /// <summary>
    /// Increases the max turrets that can be spawned at once.
    /// </summary>
    private void AddToMaxTurret()
    {
        PrintDebugMsg_TurretManager("Adding to max active turrets...");

        if ((maxTurrets > 0 && currTotalAvailable == maxTurrets) || (maxTurrets <= 0 && currTotalAvailable == turrets.Count))
        {
            PrintDebugMsg_TurretManager("  Max turrets reached!");
            CancelInvoke("AddToMaxTurret");
        }
        else
        {
            currTotalAvailable++;
            PrintDebugMsg_TurretManager("  New max active turret(s): " + currTotalAvailable);
        }

        lastNewTurretTime = Time.time;

        if(isDebug_TurretManager) SpawnTurret(turretSpawnpoints[0]);
    }
    
    /// <summary>
    /// Checks for a turret that is not currently active.
    /// </summary>
    /// <returns>The turret found that isn't currently active.</returns>
    private Turret CheckForAvailableTurret()
    {
        foreach(Turret currTurret in turrets)
        {
            if(!currTurret.gameObject.activeInHierarchy)
            {
                PrintDebugMsg_TurretManager("  Found available turret!");
                return currTurret;
            }
        }

        PrintDebugMsg_TurretManager("  No turrets available.");
        return null;
    }

    /// <summary>
    /// Disable all turrets and make all the spawnpoints available again.
    /// </summary>
    private void ResetObjects()
    {
        foreach (Turret turret in turrets) turret.ResetObject();
        for (int i = 0; i < spawnpointUsed.Count; i++)
        {
            spawnpointUsed[i] = false;
            turretSpawnpoints[i].GetComponent<SlidingDoor>().ToClosedPosition();
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_TurretManager(string msg)
    {
        if (isDebug_TurretManager) Debug.Log(isDebugScriptName_TurretManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_TurretManager(string msg)
    {
        Debug.LogWarning(isDebugScriptName_TurretManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_TurretManager(string msg)
    {
        Debug.LogError(isDebugScriptName_TurretManager + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// Check to see if active turrets reached max allowed.
    /// </summary>
    /// <returns>True if max was reached.</returns>
    public bool CheckTurretMax()
    {
        int turretsActive = 0;
        for (int i = 0; i < turrets.Count; i++)
        {
            if (turrets[i].gameObject.activeInHierarchy) turretsActive++;
            if (turretsActive >= currTotalAvailable) return true;
        }

        return false;
    }
    /// <summary>
    /// Finds out how many turrets that can be allowed to spawn.
    /// </summary>
    /// <returns>Spawnable count.</returns>
    public int SpawnableTurretCount()
    {
        int turretsActive = 0;
        for (int i = 0; i < turrets.Count; i++)
        {
            if (turrets[i].gameObject.activeInHierarchy) turretsActive++;
            if (turretsActive >= currTotalAvailable) return 0;
        }

        return currTotalAvailable - turretsActive;
    }
    
    /// <summary>
    /// Checks to see if the given spawnpoint is available.
    /// </summary>
    /// <param name="spawnpoint">Spawnpoint to check.</param>
    /// <returns>True if available.</returns>
    public bool CheckSpawnpointAvailable(Transform spawnpoint)
    {
        if(spawnpoint.tag != "TurretSpawnpoint")
        {
            PrintDebugMsg_TurretManager("Not a spawnpoint!");
            return false;
        }

        for(int i = 0; i < turretSpawnpoints.Count; i++)
        {
            if (spawnpoint == turretSpawnpoints[i]) return !spawnpointUsed[i];
        }

        PrintDebugMsg_TurretManager("Spawnpoint not found in list!");
        return false;
    }
    /// <summary>
    /// Gets all the spawnpoints that are not in use by a turret.
    /// </summary>
    /// <returns>Spawnpoints not in use by a turret.</returns>
    public List<Transform> GetAvailableSpawnpoints()
    {
        List<Transform> availableSpawnpoints = new List<Transform>();
        for(int i = 0; i < spawnpointUsed.Count; i++)
        {
            if (!spawnpointUsed[i]) availableSpawnpoints.Add(turretSpawnpoints[i]);
        }

        PrintDebugMsg_TurretManager("Found " + availableSpawnpoints.Count + " available spawnpoints.");
        return availableSpawnpoints;
    }

    /// <summary>
    /// The time until the next turret is added, between 0 and 1.
    /// </summary>
    public float TimeTilNewTurretPerc
    {
        get
        {
            if ((maxTurrets > 0 && currTotalAvailable == maxTurrets) || (maxTurrets <= 0 && currTotalAvailable == turrets.Count)) return 1;
            else return (Time.time - lastNewTurretTime) / newTurretInterval;
        }
    }

    /// <summary>
    /// The max turrets allowed to spawn.
    /// </summary>
    public int MaxTurrets
    {
        get
        {
            return (maxTurrets > 0) ? maxTurrets : turrets.Count;
        }
        set
        {
            maxTurrets = value;
            if (maxTurrets < currTotalAvailable) currTotalAvailable = maxTurrets;
            if (maxTurrets > turrets.Count) maxTurrets = turrets.Count;
            InvokeRepeating("AddToMaxTurret", 0, newTurretInterval);
        }
    }
    /// <summary>
    /// The current total turrets available.
    /// </summary>
    public int CurrTotalAvailable
    {
        get
        {
            return currTotalAvailable;
        }
    }
    /// <summary>
    /// The current total turrets out and active.
    /// </summary>
    public int TotalTurretsActive
    {
        get
        {
            int turretsOut = 0;
            foreach(Turret turret in turrets)
            {
                if (turret.gameObject.activeInHierarchy) turretsOut++;
            }
            return turretsOut;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_TurretManager("Debugging enabled.");

        if (TurretManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_TurretManager("More than one TurretManager singletons found!");
    }
    private void Update()
    {
        if (!ranSetUp && TurretPillarManager.SINGLETON.IsPillarsDone) SetUp();
    }
    #endregion
}