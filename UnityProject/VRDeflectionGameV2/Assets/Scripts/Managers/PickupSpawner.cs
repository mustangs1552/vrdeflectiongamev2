﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections;

/// <summary>
/// 
/// </summary>
public class PickupSpawner : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_PickupSpawner = false;
    private string isDebugScriptName_PickupSpawner = "PickupSpawner";
    #endregion

    #region Public
    public static PickupSpawner SINGLETON = null;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Respawns the given pickup after its respawn rate.
    /// </summary>
    /// <param name="pickup">The pickup to be respawned.</param>
    public IEnumerator RespawnPickup(PickUp pickup)
    {
        PrintDebugMsg_PickupSpawner("Starting respawn for " + pickup.gameObject.name + "...");
        yield return new WaitForSeconds(pickup.RespawnRate);
        pickup.gameObject.SetActive(true);
    }
    #endregion

    #region Private
    
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_PickupSpawner(string msg)
    {
        if (isDebug_PickupSpawner) Debug.Log(isDebugScriptName_PickupSpawner + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_PickupSpawner(string msg)
    {
        Debug.LogWarning(isDebugScriptName_PickupSpawner + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_PickupSpawner(string msg)
    {
        Debug.LogError(isDebugScriptName_PickupSpawner + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_PickupSpawner("Debugging enabled.");

        if (PickupSpawner.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_PickupSpawner("More than one PickupSpawner singltons found!");
    }
    #endregion
}