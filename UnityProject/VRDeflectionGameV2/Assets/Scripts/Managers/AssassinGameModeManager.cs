﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public enum AssassinGameModeState
{
    PlayerInput, // Pre-game. Waits til all players ready up.
    Starting,    // Give players time before match starts.
    Playing,     // Match is playing.
    EndGame      // Match has ended.
}

/// <summary>
/// 
/// </summary>
public class AssassinGameModeManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_AssassinGameModeManager = false;
    private string isDebugScriptName_AssassinGameModeManager = "AssassinGameModeManager";
    #endregion

    #region Public
    public static AssassinGameModeManager SINGLETON = null;
    [SerializeField] private bool debugVRPlayerReadyOnStart = false;

    [Header("Times")]
    [SerializeField] private float[] timeLimitOptions = new float[] { 1, 1.5f, 2, 2.5f, 3 };
    [SerializeField] private float gameStartTimerSec = 10;
    [SerializeField] private float gameplayTimerMin = 2;

    [Header("UI")]
    [SerializeField] private GameObject vrPausePanel = null;
    [SerializeField] private Text vrReadyText = null;
    [SerializeField] private Text vrTimerText = null;
    [SerializeField] private Text vrVictoryText = null;
    [SerializeField] private Text infoText = null;
    //[SerializeField] private float infoTextFadeDelay = 2;
    [SerializeField] private GameObject globalPauseCanvas = null;
    //[SerializeField] private Selectable selectOnPauseLoad = null;
    #endregion

    #region Private
    private AssassinGameModeState state = AssassinGameModeState.Starting;
    private VRPlayer vrPlayer = null;
    private MonitorPlayerManager mpManager = null;

    private bool vrPlayerReady = false;
    private List<int> monitorPlayersReady = new List<int>();
    #endregion
    #endregion

    #region Functions
    #region Public
    public void TimeUp()
    {
        switch(state)
        {
            case AssassinGameModeState.Starting:
                StartPlayingRound();
                break;
            case AssassinGameModeState.Playing:
                EndRound();
                break;
        }
    }

    public void PlayerReady(int playerNum = -1)
    {
        if(playerNum < 0 && !vrPlayerReady)
        {
            vrPlayerReady = true;
            PrintDebugMsg_AssassinGameModeManager("VR player ready!");
        }
        else if(playerNum >= 0)
        {
            PrintDebugMsg_AssassinGameModeManager("Monitor player " + playerNum + " attempting to ready up.");
            bool newPlayer = true;
            foreach(int player in monitorPlayersReady)
            {
                if(player == playerNum)
                {
                    newPlayer = false;
                    PrintDebugMsg_AssassinGameModeManager("  Already ready.");
                    break;
                }
            }

            if(newPlayer)
            {
                monitorPlayersReady.Add(playerNum);
                PrintDebugMsg_AssassinGameModeManager("Monitor players ready: " + monitorPlayersReady.Count + "/" + MonitorPlayerManager.SINGLETON.NumOfPlayers);
            }
        }

        if (vrPlayerReady && monitorPlayersReady.Count >= MonitorPlayerManager.SINGLETON.NumOfPlayers)
        {
            StartRound();

            vrPlayerReady = debugVRPlayerReadyOnStart;
            monitorPlayersReady = new List<int>();
        }
    }

    public void VRPlayerDied()
    {
        EndRound();
    }
    #endregion

    #region Private
    private void CheckState()
    {
        MinSec timeRemainging;
        switch (state)
        {
            case AssassinGameModeState.PlayerInput:
                if (vrReadyText != null) vrReadyText.text = (vrPlayerReady) ? "Ready!" : "Press \'A\' to Ready up!";
                break;
            case AssassinGameModeState.Starting:
                timeRemainging = Timer.SINGLETON.CurrTimeMinSec;
                if (vrTimerText != null) vrTimerText.text = timeRemainging.mins.ToString("D2") + ":" + timeRemainging.secs.ToString("D2");
                break;
            case AssassinGameModeState.Playing:
                timeRemainging = Timer.SINGLETON.CurrTimeMinSec;
                if (vrTimerText != null) vrTimerText.text = timeRemainging.mins.ToString("D2") + ":" + timeRemainging.secs.ToString("D2");
                break;
            case AssassinGameModeState.EndGame:
                if (vrReadyText != null) vrReadyText.text = (vrPlayerReady) ? "Ready!" : "Press \'A\' to Ready up!";
                break;
        }
    }

    private void SetupRound()
    {
        PrintDebugMsg_AssassinGameModeManager("Setting up round...");

        vrPlayerReady = debugVRPlayerReadyOnStart;
        if (vrPlayerReady) PrintDebugMsg_AssassinGameModeManager("  VR player ready!");
        vrPlayer.GetComponent<MainHealth>().ResetHealth();
        mpManager.SpawnAllPlayers();
        mpManager.ShowPlayers();
        mpManager.FreezeMovement(true);
        mpManager.FreezeLooking(true);
        mpManager.FreezeActions(true);

        Timer.SINGLETON.CancelTimer();

        if(vrReadyText != null) vrReadyText.gameObject.SetActive(true);
        if(vrTimerText != null) vrTimerText.gameObject.SetActive(false);
        if(vrPausePanel != null) vrPausePanel.SetActive(false);
        if(vrVictoryText != null) vrVictoryText.gameObject.SetActive(false);
        if(infoText != null) infoText.gameObject.SetActive(false);
        if(globalPauseCanvas != null) globalPauseCanvas.SetActive(false);

        state = AssassinGameModeState.PlayerInput;
        PrintDebugMsg_AssassinGameModeManager("  State: " + state.ToString());
    }
    private void StartRound()
    {
        PrintDebugMsg_AssassinGameModeManager("Starting round...");

        Timer.SINGLETON.StartCountdown(gameStartTimerSec);

        mpManager.HidePlayers();
        mpManager.FreezeMovement(false);
        mpManager.FreezeLooking(false);
        mpManager.FreezeActions(true);

        if (vrReadyText != null) vrReadyText.gameObject.SetActive(false);
        if (vrTimerText != null) vrTimerText.gameObject.SetActive(true);
        if (vrPausePanel != null) vrPausePanel.SetActive(false);
        if (vrVictoryText != null) vrVictoryText.gameObject.SetActive(false);
        if (infoText != null) infoText.gameObject.SetActive(false);
        if (globalPauseCanvas != null) globalPauseCanvas.SetActive(false);

        state = AssassinGameModeState.Starting;
        PrintDebugMsg_AssassinGameModeManager("  State: " + state.ToString());
    }
    private void StartPlayingRound()
    {
        PrintDebugMsg_AssassinGameModeManager("Now playing round...");

        Timer.SINGLETON.StartCountdown(gameplayTimerMin * 60);

        mpManager.FreezeMovement(false);
        mpManager.FreezeLooking(false);
        mpManager.FreezeActions(false);

        if (vrReadyText != null) vrReadyText.gameObject.SetActive(false);
        if (vrTimerText != null) vrTimerText.gameObject.SetActive(true);
        if (vrPausePanel != null) vrPausePanel.SetActive(false);
        if (vrVictoryText != null) vrVictoryText.gameObject.SetActive(false);
        if (infoText != null) infoText.gameObject.SetActive(false);
        if (globalPauseCanvas != null) globalPauseCanvas.SetActive(false);

        state = AssassinGameModeState.Playing;
        PrintDebugMsg_AssassinGameModeManager("  State: " + state.ToString());
    }
    private void EndRound()
    {
        PrintDebugMsg_AssassinGameModeManager("Ending round...");
        
        mpManager.FreezeMovement(true);
        mpManager.FreezeLooking(true);
        mpManager.FreezeActions(true);

        Timer.SINGLETON.CancelTimer();

        state = AssassinGameModeState.EndGame;
        PrintDebugMsg_AssassinGameModeManager("  State: " + state.ToString());

        SetupRound();
    }
    
    private void SetSettings()
    {
        if (MatchSettings.SINGLETON != null)
        {
            AssassinModeSettings settings = MatchSettings.SINGLETON.SettingsAssassinmode;

            if (settings != null)
            {
                gameplayTimerMin = timeLimitOptions[settings.timeLimitIndex];
                mpManager.SetSettings();
            }

            Destroy(MatchSettings.SINGLETON.gameObject);
        }
    }
    private void Setup()
    {
        GameObject[] objectsFound = GameObject.FindGameObjectsWithTag("VRPlayer");
        foreach (GameObject obj in objectsFound)
        {
            vrPlayer = obj.GetComponent<VRPlayer>();
            if (vrPlayer != null) break;
        }
        if (vrPlayer == null) PrintErrorDebugMsg_AssassinGameModeManager("No VR player found!");

        mpManager = GetComponent<MonitorPlayerManager>();
        if (mpManager == null) PrintErrorDebugMsg_AssassinGameModeManager("No MonitorPlayerManager found!");
        
        SetSettings();
        mpManager.Setup();
        SetupRound();
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_AssassinGameModeManager(string msg)
    {
        if (isDebug_AssassinGameModeManager) Debug.Log(isDebugScriptName_AssassinGameModeManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_AssassinGameModeManager(string msg)
    {
        Debug.LogWarning(isDebugScriptName_AssassinGameModeManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_AssassinGameModeManager(string msg)
    {
        Debug.LogError(isDebugScriptName_AssassinGameModeManager + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    public AssassinGameModeState State
    {
        get
        {
            return state;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_AssassinGameModeManager("Debugging enabled.");

        if (AssassinGameModeManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_AssassinGameModeManager("More than one AssassinGameModeManagers found!");
    }
    private void Start()
    {
        Setup();
    }
    private void Update()
    {
        CheckState();
    }
    #endregion
}