﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum TraingingGameModeState
{
    PreGame,
    Playing,
    PostGame,
    Paused
}
public enum TrainingModeEndRoundReason
{
    VRPlayerDied,
    TimeUp,
    Other
}

/// <summary>
/// 
/// </summary>
public class TrainingGameModeManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_TrainingGameModeManager = false;
    private string isDebugScriptName_TrainingGameModeManager = "TrainingGameModeManager";
    #endregion

    #region Public
    public static TrainingGameModeManager SINGLETON = null;

    [Tooltip("For a time limit use > 0. For a timer use <= 0.")]
    [SerializeField] private float timeLimitMin = 5;
    [Tooltip("This must match the dropdown on the main menu.")]
    [SerializeField] private float[] timeLimitOptions = new float[] {1, 1.5f, 2, 2.5f, 3};
    [SerializeField] private float darknessModeIntensity = 0;
    [SerializeField] private float lightingTransitionSpeed = .1f;
    [SerializeField] private Light[] environmentLights = null;
    [SerializeField] private ReflectionProbe windowReflectionProbe = null;
    [SerializeField] private Material levitaionDevicePartMat = null;
    [Header("UI")]
    [SerializeField] private GameObject mainUI = null;
    [SerializeField] private GameObject vrLaserPointer = null;
    [SerializeField] private GameObject preGameUI = null;
    [SerializeField] private Text pillerMovementText = null;
    [SerializeField] private int pillarMovementTextSmallerFont = 18;
    [SerializeField] private GameObject postGameUI = null;
    [SerializeField] private Text resultText = null;
    [SerializeField] private FloorUIObj[] vrPlayerFloorHUDs = null;
    [SerializeField] private Text vrPlayerHealthText = null;
    [SerializeField] private Text timerText = null;
    [SerializeField] private Slider nextTurretSlider = null;
    [SerializeField] private Text activeTurretsText = null;
    [SerializeField] private Text availableTurretsText = null;
    [SerializeField] private Text maxTurretsText = null;
    [SerializeField] private GameObject pausedUI = null;
    #endregion

    #region Private
    private TraingingGameModeState state = TraingingGameModeState.PreGame;
    private VRPlayer vrPlayer = null;

    private float startLightIntensity = 0;
    private float startReflectionIntensity = 0;
    private bool transitioningLights = false;
    private bool dimming = false;

    private int pillarMovementTextStartFont = 0;

    private bool darknessMode = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Sets the round's settings and prepares the round.
    /// </summary>
    public void SetUpGame()
    {
        SetSettings();

        Resume();
        pausedUI.gameObject.SetActive(false);
        vrLaserPointer.SetActive(false);
        ShowPreGameUI();
    }

    /// <summary>
    /// Start the playing the round.
    /// </summary>
    public void StartRound()
    {
        if (state == TraingingGameModeState.PreGame)
        {
            PrintDebugMsg_TrainingGameModeManager("Starting round...");

            if (timeLimitMin > 0) Timer.SINGLETON.StartCountdown(timeLimitMin * 60 - .01f);
            else Timer.SINGLETON.StartTimer();

            state = TraingingGameModeState.Playing;
            TurretManager.SINGLETON.StartRound();
            TurretSpawnManager.SINGLETON.StartRound();
            HideUI();
            Resume();
        }
    }
    /// <summary>
    /// End the round and enter post-game.
    /// </summary>
    public void EndRound(TrainingModeEndRoundReason reason = TrainingModeEndRoundReason.Other)
    {
        if (state == TraingingGameModeState.Playing)
        {
            PrintDebugMsg_TrainingGameModeManager("Ending round...");
            state = TraingingGameModeState.PostGame;
            Time.timeScale = 0;
            ShowPostUI(reason);
        }
    }
    /// <summary>
    /// Restart the round and enter pre-game
    /// </summary>
    public void ResetRound()
    {
        if (state == TraingingGameModeState.PostGame)
        {
            PrintDebugMsg_TrainingGameModeManager("Reseting round...");
            state = TraingingGameModeState.PreGame;
            Timer.SINGLETON.CancelTimer();
            TurretManager.SINGLETON.ResetRound();
            ObjectManager.SINGLETON.ResetObjects();
            vrPlayer.ResetVRPlayer();
            Time.timeScale = 1;
            ShowPreGameUI();
        }
    }
    /// <summary>
    /// Resets amd starts a new round only if a round has already ended.
    /// </summary>
    public void RestartRound()
    {
        ResetRound();
        StartRound();
    }

    /// <summary>
    /// Timer hit 0.
    /// </summary>
    public void TimeUp()
    {
        EndRound(TrainingModeEndRoundReason.TimeUp);
    }

    /// <summary>
    /// Pauses/resumes the game.
    /// </summary>
    public void PauseResume()
    {
        if (state == TraingingGameModeState.Playing)
        {
            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                pausedUI.gameObject.SetActive(false);
                mainUI.SetActive(false);
                vrLaserPointer.SetActive(false);
            }
            else
            {
                Time.timeScale = 0;
                if (!mainUI.activeInHierarchy) mainUI.SetActive(true);
                pausedUI.gameObject.SetActive(true);
                vrLaserPointer.SetActive(true);
            }
        }
    }
    /// <summary>
    /// Pauses the game.
    /// </summary>
    public void Pause()
    {
        if (state == TraingingGameModeState.Playing)
        {
            Time.timeScale = 0;
            if (!mainUI.activeInHierarchy) mainUI.SetActive(true);
            pausedUI.gameObject.SetActive(true);
            vrLaserPointer.SetActive(true);
        }
    }
    /// <summary>
    /// Resumes the game.
    /// </summary>
    public void Resume()
    {
        if (state == TraingingGameModeState.Playing)
        {
            Time.timeScale = 1;
            pausedUI.gameObject.SetActive(false);
            mainUI.SetActive(false);
            vrLaserPointer.SetActive(false);
        }
    }

    /// <summary>
    /// Ends the round and shows the main menu.
    /// </summary>
    public void LoadMainMenu()
    {
        MainMenu.SINGLETON.gameObject.SetActive(true);
        mainUI.SetActive(false);
        TurretPillarManager.SINGLETON.LowerPillars();
        foreach (FloorUIObj obj in vrPlayerFloorHUDs) obj.Hide();

        dimming = false;
        DimLighting();
        darknessMode = false;
        vrPlayer.DisableSingleSaberMode();

        EndRound();
        ResetRound();
    }
    #endregion

    #region Private
    /// <summary>
    /// Assign variables.
    /// </summary>
    private void SetUp()
    {
        GameObject[] objectsFound = GameObject.FindGameObjectsWithTag("VRPlayer");
        foreach(GameObject obj in objectsFound)
        {
            vrPlayer = obj.GetComponent<VRPlayer>();
            if (vrPlayer != null) break;
        }
        if(vrPlayer == null) PrintErrorDebugMsg_TrainingGameModeManager("No VR player found!");

        startLightIntensity = environmentLights[0].intensity;
        if (windowReflectionProbe != null) startReflectionIntensity = windowReflectionProbe.intensity;

        visEffects.staticInit();

        pillarMovementTextStartFont = pillerMovementText.fontSize;
    }

    /// <summary>
    /// Set the setting for the match from the main menu.
    /// </summary>
    private void SetSettings()
    {
        if (MatchSettings.SINGLETON != null)
        {
            TrainingModeSettings settings = MatchSettings.SINGLETON.SettingsTrainingMode;

            if (settings != null)
            {
                if (settings.timeEndless) timeLimitMin = 0;
                else timeLimitMin = timeLimitOptions[settings.timeLimitValue];
                PrintDebugMsg_TrainingGameModeManager("SETTINGS | Time Limit: " + timeLimitMin);

                TurretSpawnManager.SINGLETON.SetSettings();
                TurretManager.SINGLETON.SetSettings();
                TurretPillarManager.SINGLETON.RaisePillars(settings.Use360SetUp);
                foreach (FloorUIObj obj in vrPlayerFloorHUDs) obj.Show();
                if (settings.DarknessMode)
                {
                    dimming = true;
                    DimLighting();
                }
                darknessMode = settings.DarknessMode;
                
                if (settings.singleSaberMode == 1) vrPlayer.EnableSingleSaberMode(true);
                else if (settings.singleSaberMode == 2) vrPlayer.EnableSingleSaberMode(false);
                else vrPlayer.DisableSingleSaberMode();
            }
            else TurretPillarManager.SINGLETON.RaisePillars();

            Destroy(MatchSettings.SINGLETON.gameObject);
        }
        else TurretPillarManager.SINGLETON.RaisePillars();
    }

    /// <summary>
    /// Darkens or brightens the environment's lighting depending on what dimming is set to.
    /// </summary>
    /// <param name="updating">Used for updating the lights' value while transitioning.</param>
    private void DimLighting(bool updating = false)
    {
        if (!updating) transitioningLights = true;

        if (transitioningLights)
        {
            foreach (Light light in environmentLights)
            {
                if (dimming)
                {
                    light.intensity -= lightingTransitionSpeed * Time.deltaTime;
                    windowReflectionProbe.intensity -= lightingTransitionSpeed * Time.deltaTime;
                    if (light.intensity <= darknessModeIntensity)
                    {
                        light.intensity = darknessModeIntensity;
                        transitioningLights = false;
                    }
                    if (windowReflectionProbe.intensity < 0) windowReflectionProbe.intensity = 0;
                }
                else
                {
                    light.intensity += lightingTransitionSpeed * Time.deltaTime;
                    windowReflectionProbe.intensity += lightingTransitionSpeed * Time.deltaTime;
                    if (light.intensity >= startLightIntensity)
                    {
                        light.intensity = startLightIntensity;
                        transitioningLights = false;
                    }
                    if (windowReflectionProbe.intensity > startReflectionIntensity) windowReflectionProbe.intensity = startReflectionIntensity;
                }
            }
        }
    }

    /// <summary>
    /// Update the UI.
    /// </summary>
    private void UpdateUI()
    {
        //if (TurretPillarManager.SINGLETON.IsPillarsDone && !mainUI.activeInHierarchy && !MainMenu.SINGLETON.gameObject.activeInHierarchy) ShowPreGameUI();
        if (MainMenu.SINGLETON.gameObject.activeInHierarchy)
        {
            mainUI.SetActive(false);
            vrLaserPointer.SetActive(true);
        }
        else
        {
            Button startButton = ObjectUtility.ChildrenUtility.FindChildByName(preGameUI.transform, "StartButton").GetComponent<Button>();
            if (TurretPillarManager.SINGLETON.PillarsMoving) startButton.interactable = false;
            else startButton.interactable = true;
        }

        vrPlayerHealthText.text = vrPlayer.GetComponent<MainHealth>().CurrHealth.ToString();

        if (state == TraingingGameModeState.PreGame)
        {
            if (timeLimitMin > 0)
            {
                MinSec startingTime = new MinSec();
                startingTime.mins = (int)timeLimitMin;
                startingTime.secs = (int)((timeLimitMin - startingTime.mins) * 60);
                timerText.text = startingTime.mins.ToString("D2") + ":" + startingTime.secs.ToString("D2");
            }
            else timerText.text = "00:00";
        }
        else if (state == TraingingGameModeState.Playing)
        {
            MinSec timeRemainging = Timer.SINGLETON.CurrTimeMinSec;
            timerText.text = timeRemainging.mins.ToString("D2") + ":" + timeRemainging.secs.ToString("D2");
        }

        if (nextTurretSlider != null)
        {
            if (state != TraingingGameModeState.PreGame) nextTurretSlider.value = TurretManager.SINGLETON.TimeTilNewTurretPerc;
            else nextTurretSlider.value = 1;
        }
        activeTurretsText.text = TurretManager.SINGLETON.TotalTurretsActive.ToString((TurretManager.SINGLETON.TotalTurretsActive >= 10) ? "D2" : "D1");
        availableTurretsText.text = TurretManager.SINGLETON.CurrTotalAvailable.ToString((TurretManager.SINGLETON.CurrTotalAvailable >= 10) ? "D2" : "D1");
        maxTurretsText.text = TurretManager.SINGLETON.MaxTurrets.ToString((TurretManager.SINGLETON.MaxTurrets >= 10) ? "D2" : "D1");

        if (preGameUI.activeInHierarchy)
        {
            if (TurretPillarManager.SINGLETON.PillarsMoving && pillerMovementText.fontSize != pillarMovementTextSmallerFont)
            {
                pillerMovementText.text = "Raising Pillers...";
                pillerMovementText.fontSize = pillarMovementTextSmallerFont;
            }
            else if(!TurretPillarManager.SINGLETON.PillarsMoving && pillerMovementText.fontSize != pillarMovementTextStartFont)
            {
                pillerMovementText.text = "Start";
                pillerMovementText.fontSize = pillarMovementTextStartFont;
            }
        }
    }
    
    /// <summary>
    /// Shows the pre-game menu controls.
    /// </summary>
    private void ShowPreGameUI()
    {
        if (!mainUI.activeInHierarchy) mainUI.SetActive(true);

        vrLaserPointer.SetActive(true);
        preGameUI.SetActive(true);
        postGameUI.SetActive(false);
    }
    /// <summary>
    /// Shows the post-game menu controls.
    /// </summary>
    private void ShowPostUI(TrainingModeEndRoundReason reason)
    {
        if (!mainUI.activeInHierarchy) mainUI.SetActive(true);

        vrLaserPointer.SetActive(true);
        preGameUI.SetActive(false);
        postGameUI.SetActive(true);

        if (reason == TrainingModeEndRoundReason.VRPlayerDied) resultText.text = "You died!";
        else if (reason == TrainingModeEndRoundReason.TimeUp) resultText.text = "You survived!";
        else resultText.text = "";
    }
    /// <summary>
    /// Hides the pre- and post-game menu controls.
    /// </summary>
    private void HideUI()
    {
        mainUI.SetActive(false);
        vrLaserPointer.SetActive(false);
        //preGameUI.SetActive(false);
        //postGameUI.SetActive(false);
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_TrainingGameModeManager(string msg)
    {
        if (isDebug_TrainingGameModeManager) Debug.Log(isDebugScriptName_TrainingGameModeManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_TrainingGameModeManager(string msg)
    {
        Debug.LogWarning(isDebugScriptName_TrainingGameModeManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_TrainingGameModeManager(string msg)
    {
        Debug.LogError(isDebugScriptName_TrainingGameModeManager + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// The current state of the game.
    /// </summary>
    public TraingingGameModeState State
    {
        get
        {
            return state;
        }
    }

    /// <summary>
    /// The VR player in the scene.
    /// </summary>
    public VRPlayer VRPlayer
    {
        get
        {
            return vrPlayer;
        }
    }

    /// <summary>
    /// Darkness mode.
    /// </summary>
    public bool DarknessMode
    {
        get
        {
            return darknessMode;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_TrainingGameModeManager("Debugging enabled.");

        if (TrainingGameModeManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_TrainingGameModeManager("More than one TrainingGameModeManager singleton found!");

        SetUp();
        mainUI.SetActive(false);
        vrLaserPointer.SetActive(false);
    }
    private void Update()
    {
        UpdateUI();

        DimLighting(true);

        if(isDebug_TrainingGameModeManager && Input.GetKeyUp(KeyCode.O))
        {
            switch(state)
            {
                case TraingingGameModeState.PreGame:
                    StartRound();
                    break;
                case TraingingGameModeState.Playing:
                    EndRound();
                    break;
                case TraingingGameModeState.PostGame:
                    ResetRound();
                    break;
            }
        }
    }
    #endregion
}