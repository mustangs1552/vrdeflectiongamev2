﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MonitorPlayerManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_MonitorPlayerManager = false;
    private string isDebugScriptName_MonitorPlayerManager = "MonitorPlayerManager";
    #endregion

    #region Public
    public static MonitorPlayerManager SINGLETON = null;

    [SerializeField] private GameObject playerPrefab = null;
    [Range(1, 4)] [SerializeField] private int numOfPlayers = 1;
    [SerializeField] private float monitorPlayerRespawnDelay = 3;
    #endregion

    #region Private
    private List<GameObject> players = new List<GameObject>();
    private List<Transform> playerSpawnpoints = new List<Transform>();

    private bool playerUsingMouseKeyboard = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Spawns the selected player at thier spawnpoint.
    /// </summary>
    /// <param name="player">The player to be spawned.</param>
    /// <param name="usingMouseKeyboard">The player is using mouse and keyboard. Default is false.</param>
    public void SpawnPlayer(GameObject player, bool usingMouseKeyboard = false)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i] == player)
            {
                player.transform.position = playerSpawnpoints[i].position;
                player.transform.rotation = playerSpawnpoints[i].rotation;
                player.GetComponent<MonitorPlayer>().UsingMouseKeyboard = false;
                player.GetComponent<MonitorPlayer>().CancelInvoke("BecomeInvisible");
                if (usingMouseKeyboard && !playerUsingMouseKeyboard)
                {
                    player.GetComponent<MonitorPlayer>().UsingMouseKeyboard = true;
                    playerUsingMouseKeyboard = true;
                }
                else if (usingMouseKeyboard) PrintWarningDebugMsg_MonitorPlayerManager("A player is already using mouse and keyboard! This one will be set to controller instead.");
                player.SetActive(true);
                return;
            }
        }
    }
    /// <summary>
    /// Spawns all players.
    /// </summary>
    public void SpawnAllPlayers()
    {
        foreach (GameObject player in players) SpawnPlayer(player);
        UpdatePlayerNumbers();
    }

    /// <summary>
    /// Freeze the movement for all players.
    /// </summary>
    /// <param name="freeze">True to freeze.</param>
    public void FreezeMovement(bool freeze)
    {
        foreach (GameObject player in players)
        {
            player.GetComponent<MonitorPlayer>().MovementFrozen = freeze;
        }
    }
    /// <summary>
    /// Freeze the ability to look around for all players.
    /// </summary>
    /// <param name="freeze">True to freeze.</param>
    public void FreezeLooking(bool freeze)
    {
        foreach (GameObject player in players)
        {
            player.GetComponent<MonitorPlayer>().LookingFrozen = freeze;
        }
    }
    /// <summary>
    /// Freeze the actions for all players.
    /// </summary>
    /// <param name="freeze">True to freeze.</param>
    public void FreezeActions(bool freeze)
    {
        foreach (GameObject player in players)
        {
            player.GetComponent<MonitorPlayer>().ActionsFrozen = freeze;
        }
    }

    /// <summary>
    /// Makes all players visible.
    /// </summary>
    public void ShowPlayers()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().BecomeVisible(false);
    }
    /// <summary>
    /// Makes all players invisible.
    /// </summary>
    public void HidePlayers()
    {
        foreach (GameObject player in players) player.GetComponent<MonitorPlayer>().BecomeInvisible();
    }

    /// <summary>
    /// A monitor player died.
    /// </summary>
    /// <param name="playerNum">The number of the died player.</param>
    public void PlayerDied(int playerNum)
    {
        PrintDebugMsg_MonitorPlayerManager("Monitor player died!");
        foreach(GameObject player in players)
        {
            if (player.GetComponent<MonitorPlayer>().PlayerNum == playerNum)
            {
                StartCoroutine(RespawnPlayer(player));
                break;
            }
        }
    }
    
    public void SetSettings()
    {
        numOfPlayers = MatchSettings.SINGLETON.SettingsAssassinmode.playerCount;
    }
    /// <summary>
    /// Sets up the players and thier spawnpoints.
    /// </summary>
    public void Setup()
    {
        SetupPlayers();
        FindPlayerspawnpoints();
    }
    #endregion

    #region Private
    /// <summary>
    /// Instantiate all the players and set thier views on the screen.
    /// </summary>
    private void SetupPlayers()
    {
        for (int i = 0; i < numOfPlayers; i++)
        {
            players.Add(Instantiate(playerPrefab));
            players[i].SetActive(false);
        }
        PrintDebugMsg_MonitorPlayerManager("Number of players created: " + players.Count + " (" + numOfPlayers + ")");

        Rect[] viewportRects = new Rect[4];
        if (players.Count == 1)
        {
            viewportRects[0] = new Rect(0, 0, 1, 1);
            viewportRects[1] = new Rect(0, 0, 0, 0);
            viewportRects[2] = new Rect(0, 0, 0, 0);
            viewportRects[3] = new Rect(0, 0, 0, 0);
        }
        else if (players.Count == 2)
        {
            viewportRects[0] = new Rect(0, .5f, 1, 1);
            viewportRects[1] = new Rect(0, 0, 1, .5f);
            viewportRects[2] = new Rect(0, 0, 0, 0);
            viewportRects[3] = new Rect(0, 0, 0, 0);
        }
        else if (players.Count == 3)
        {
            viewportRects[0] = new Rect(0, .5f, 1, 1);
            viewportRects[1] = new Rect(0, 0, .5f, .5f);
            viewportRects[2] = new Rect(.5f, 0, 1, .5f);
            viewportRects[3] = new Rect(0, 0, 0, 0);
        }
        else if (players.Count == 4)
        {
            viewportRects[0] = new Rect(0, .5f, .5f, 1);
            viewportRects[1] = new Rect(.5f, .5f, 1, 1);
            viewportRects[2] = new Rect(0, 0, .5f, .5f);
            viewportRects[3] = new Rect(.5f, 0, 1, .5f);
        }
        for (int i = 0; i < players.Count; i++)
        {
            MonitorPlayer mpScript = players[i].GetComponent<MonitorPlayer>();
            mpScript.Cam.rect = viewportRects[i];
            mpScript.PlayerNum = i;
        }
    }
    /// <summary>
    /// Find all the spawnpoints for the players. There must be at least four total.
    /// </summary>
    private void FindPlayerspawnpoints()
    {
        GameObject[] foundSpawns = GameObject.FindGameObjectsWithTag("MonitorPlayerSpawnpoint");
        foreach (GameObject obj in foundSpawns) playerSpawnpoints.Add(obj.transform);

        PrintDebugMsg_MonitorPlayerManager("Monitor player spawnpoints found: " + playerSpawnpoints.Count);
        if(playerSpawnpoints.Count < 4) PrintErrorDebugMsg_MonitorPlayerManager("There are not enough spawnpoints! There must be four.");
    }

    /// <summary>
    /// Updates the players' numbers.
    /// </summary>
    private void UpdatePlayerNumbers()
    {
        string debugStr = "Player numbers:\n";
        if (playerUsingMouseKeyboard)
        {
            int currPlayerNum = 0;
            foreach (GameObject player in players)
            {
                MonitorPlayer mpScript = player.GetComponent<MonitorPlayer>();
                if (mpScript.UsingMouseKeyboard)
                {
                    mpScript.PlayerNum = players.Count - 1;
                    debugStr += "  Mouse & keyboard player: " + mpScript.PlayerNum + "\n";
                }
                else
                {
                    mpScript.PlayerNum = currPlayerNum;
                    currPlayerNum++;
                    debugStr += "  Controller player: " + mpScript.PlayerNum + "\n";
                }

                player.name = playerPrefab.name + "(Clone) (P" + mpScript.PlayerNum + ")";
            }
        }
        else
        {
            for (int i = 0; i < players.Count; i++)
            {
                MonitorPlayer mpScript = players[i].GetComponent<MonitorPlayer>();
                mpScript.PlayerNum = i;
                players[i].name = playerPrefab.name + "(Clone) (P" + mpScript.PlayerNum + ")";
                debugStr += "  Controller player: " + mpScript.PlayerNum + "\n";
            }
        }

        PrintDebugMsg_MonitorPlayerManager(debugStr);
    }

    /// <summary>
    /// Spawn the dead player back in.
    /// </summary>
    /// <param name="player">The player to be spawned.</param>
    private IEnumerator RespawnPlayer(GameObject player)
    {
        yield return new WaitForSeconds(monitorPlayerRespawnDelay);

        PrintDebugMsg_MonitorPlayerManager("Respawning a monitor player...");
        MonitorPlayer mPlayer = player.GetComponent<MonitorPlayer>();
        SpawnPlayer(player, mPlayer.UsingMouseKeyboard);
        mPlayer.MovementFrozen = false;
        mPlayer.LookingFrozen = false;
        mPlayer.ActionsFrozen = false;
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_MonitorPlayerManager(string msg)
    {
        if (isDebug_MonitorPlayerManager) Debug.Log(isDebugScriptName_MonitorPlayerManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_MonitorPlayerManager(string msg)
    {
        Debug.LogWarning(isDebugScriptName_MonitorPlayerManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_MonitorPlayerManager(string msg)
    {
        Debug.LogError(isDebugScriptName_MonitorPlayerManager + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// The number of players in the game.
    /// </summary>
    public int NumOfPlayers
    {
        get
        {
            return numOfPlayers;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_MonitorPlayerManager("Debugging enabled.");

        if (MonitorPlayerManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_MonitorPlayerManager("More than one MonitorPlayerManager singleton found!");
    }
    #endregion
}