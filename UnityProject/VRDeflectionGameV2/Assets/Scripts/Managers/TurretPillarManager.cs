﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class TurretPillarManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_TurretPillarManager = false;
    private string isDebugScriptName_TurretPillarManager = "TurretPillarManager";
    #endregion

    #region Public
    public static TurretPillarManager SINGLETON = null;

    [SerializeField] private TurretPillar[] frontFacingPillars = null;
    [SerializeField] private TurretPillar[] allFacingPillars = null;
    [SerializeField] private bool enable360 = false;
    [SerializeField] private float startDelay = 3;
    [SerializeField] private float eachPillarDelay = 1;
    #endregion

    #region Private
    private bool on360Pillars = false;
    private int currPillar = 0;
    private bool isStartPillarsDone = false;
    private bool isRaising = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Start lowering the pillars.
    /// </summary>
    public void LowerPillars()
    {
        isRaising = false;
        currPillar = 0;
        on360Pillars = false;
        isStartPillarsDone = false;
        Invoke("StartNextPillars", startDelay);
    }
    /// <summary>
    /// Start raising the pillars using whatever enable360 is currently set to.
    /// </summary>
    public void RaisePillars()
    {
        isRaising = true;
        currPillar = 0;
        on360Pillars = false;
        isStartPillarsDone = false;
        Invoke("StartNextPillars", startDelay);
    }
    /// <summary>
    /// Start raising pillars.
    /// </summary>
    /// <param name="use360SetUp">Use 360 setup pillars.</param>
    public void RaisePillars(bool use360SetUp)
    {
        isRaising = true;
        currPillar = 0;
        on360Pillars = false;
        isStartPillarsDone = false;
        enable360 = use360SetUp;
        Invoke("StartNextPillars", startDelay);
    }
    #endregion

    #region Private
    /// <summary>
    /// Start the next pillar.
    /// </summary>
    private void StartNextPillars()
    {
        if (!on360Pillars)
        {
            PrintDebugMsg_TurretPillarManager("Moving a 180 pillar...");

            frontFacingPillars[currPillar].StartMoving(isRaising);
            currPillar++;

            if (currPillar > 1)
            {
                frontFacingPillars[currPillar].StartMoving(isRaising);
                currPillar++;
            }

            if(currPillar == frontFacingPillars.Length)
            {
                currPillar = 0;
                on360Pillars = true;

                if(!enable360)
                {
                    isStartPillarsDone = true;
                    PrintDebugMsg_TurretPillarManager("Done starting pillars.");
                }
            }

            Invoke("StartNextPillars", eachPillarDelay);
        }
        else if(enable360)
        {
            PrintDebugMsg_TurretPillarManager("Moving a 360 pillar...");

            allFacingPillars[currPillar].EnableSpawnpoints();
            allFacingPillars[currPillar].StartMoving(isRaising);
            currPillar++;

            if (currPillar != allFacingPillars.Length)
            {
                allFacingPillars[currPillar].EnableSpawnpoints();
                allFacingPillars[currPillar].StartMoving(isRaising);
                currPillar++;

                Invoke("StartNextPillars", eachPillarDelay);
            }
            else
            {
                isStartPillarsDone = true;
                PrintDebugMsg_TurretPillarManager("Done starting pillars.");
            }
        }
        else if(currPillar != allFacingPillars.Length)
        {
            allFacingPillars[currPillar].DisableSpawnpoints();
            currPillar++;

            if (currPillar != allFacingPillars.Length) StartNextPillars();
            else PrintDebugMsg_TurretPillarManager("Done disabling 360 setup spawnpoints.");
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_TurretPillarManager(string msg)
    {
        if (isDebug_TurretPillarManager) Debug.Log(isDebugScriptName_TurretPillarManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_TurretPillarManager(string msg)
    {
        Debug.LogWarning(isDebugScriptName_TurretPillarManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_TurretPillarManager(string msg)
    {
        Debug.LogError(isDebugScriptName_TurretPillarManager + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    /// <summary>
    /// Are all the pillars done moving?
    /// </summary>
    public bool IsPillarsDone
    {
        get
        {
            if (IsStartPillarsDone)
            {
                if (!enable360) return !frontFacingPillars[frontFacingPillars.Length - 1].IsMoving;
                else return !allFacingPillars[allFacingPillars.Length - 1].IsMoving;
            }
            else return false;
        }
    }
    /// <summary>
    /// Did we start moving all the pillars?
    /// </summary>
    public bool IsStartPillarsDone
    {
        get
        {
            return isStartPillarsDone;
        }
    }

    /// <summary>
    /// Are any pillars moving?
    /// </summary>
    public bool PillarsMoving
    {
        get
        {
            bool isMoving = false;
            foreach(TurretPillar pillar in frontFacingPillars)
            {
                isMoving = pillar.IsMoving;
                if (isMoving) break;
            }
            if(!isMoving)
            {
                foreach (TurretPillar pillar in allFacingPillars)
                {
                    isMoving = pillar.IsMoving;
                    if (isMoving) break;
                }
            }

            return isMoving;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_TurretPillarManager("Debugging enabled.");

        if (TurretPillarManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_TurretPillarManager("More than one TurretPillarManager found!");
    }
    #endregion
}