﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class ObjectManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_ObjectManager = false;
    private string isDebugScriptName_ObjectManager = "ObjectManager";
    #endregion

    #region Public
    public static ObjectManager SINGLETON = null;
    #endregion

    #region Private
    List<GameObject> objects = new List<GameObject>();
    private Transform objectGroup = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Checks to see if an object is available to use and returns it.
    /// </summary>
    /// <param name="type">The type of object to check for.</param>
    /// <returns>An available object. If none returns null.</returns>
    public GameObject CheckAddObject(GameObject desiredObj)
    {
        foreach (GameObject obj in objects)
        {
            if (!obj.gameObject.activeInHierarchy && obj.name == desiredObj.name + "(Clone)")
            {
                PickUp pickup = obj.GetComponent<PickUp>();
                if (pickup != null && pickup.RespawnRate >= 0) continue;

                PrintDebugMsg_ObjectManager("Found an object!");
                return obj;
            }
        }

        PrintDebugMsg_ObjectManager("No available object found!");
        return null;
    }
    // Transform CheckAddObject(GameObject)
    /// <summary>
    /// Adds the given object to the list if it doesn't already exist.
    /// </summary>
    /// <param name="obj">The object to add.</param>
    /// <returns>True if added successfully.</returns>
    public bool AddObject(GameObject obj)
    {
        foreach (GameObject o in objects)
        {
            if (o == obj)
            {
                PrintWarningDebugMsg_ObjectManager("Object already in list!");
                return false;
            }
        }

        PrintDebugMsg_ObjectManager("Adding object...");
        objects.Add(obj);
        obj.transform.parent = objectGroup;
        return true;
    }
    /// <summary>
    /// Disables the given object in the list.
    /// </summary>
    /// <param name="obj">The object to disable.</param>
    /// <returns>True if disabled successfully.</returns>
    public bool DestroyObject(GameObject obj)
    {
        foreach (GameObject o in objects)
        {
            if (o == obj)
            {
                PrintDebugMsg_ObjectManager("Disabling object...");
                o.gameObject.SetActive(false);
                return true;
            }
        }

        PrintWarningDebugMsg_ObjectManager("Object not in list!");
        return false;
    }
    // RemoveObject(GameObject)

    /// <summary>
    /// Disable all objects.
    /// </summary>
    public void ResetObjects()
    {
        foreach (GameObject obj in objects) obj.SetActive(false);
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_ObjectManager(string msg)
    {
        if (isDebug_ObjectManager) Debug.Log(isDebugScriptName_ObjectManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_ObjectManager(string msg)
    {
        Debug.LogWarning(isDebugScriptName_ObjectManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_ObjectManager(string msg)
    {
        Debug.LogError(isDebugScriptName_ObjectManager + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// List of all objects.
    /// </summary>
    public List<GameObject> Objects
    {
        get
        {
            return objects;
        }
    }
    // List<GameObject>/GameObject[] AllActiveObjects()
    // List<Transform>/Transform[] AllActiveObjects()
    // List<GameObject>/GameObject[] AllDeactiveObjects()
    // List<Transform>/Transform[] AllDeactiveObjects()
    // List<GameObject>/GameObject[] AllSpecificObjects(GameObject)
    // List<Transform>/Transform[] AllSpecificObjects(GameObject)
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_ObjectManager("Debugging enabled.");

        if (ObjectManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_ObjectManager("More than one ObjectManager singletons found!");

        objectGroup = new GameObject("ObjectGroup").transform;
    }
    #endregion
}