﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class TurretPillar : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_TurretPillar = false;
    private string isDebugScriptName_TurretPillar = "TurretPillar";
    #endregion

    #region Public
    [SerializeField] private float speed = 1;
    [SerializeField] private float height = 15;
    [SerializeField] private AudioClip movingSound = null;
    #endregion

    #region Private
    private AudioSource audioSource = null;
    private float startHeight = 0;
    private float endHeight = 0;
    private bool isMoving = false;
    private bool isRaising = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Start raising/lowering.
    /// </summary>
    /// <param name="raising">True if raising, false if lowering.</param>
    public void StartMoving(bool raising)
    {
        isRaising = raising;
        isMoving = true;

        if (audioSource != null && movingSound != null)
        {
            audioSource.clip = movingSound;
            audioSource.Play();
        }
    }

    /// <summary>
    /// Make all spawnpoints not active.
    /// </summary>
    public void DisableSpawnpoints()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if(child.GetChild(0).tag == "TurretSpawnpoint")
            {
                for(int ii = 0; ii < child.childCount; ii++)
                {
                    Transform spawnpoint = child.GetChild(ii);
                    spawnpoint.gameObject.SetActive(false);
                }
            }
        }
    }
    /// <summary>
    /// Make all spawnpoints active.
    /// </summary>
    public void EnableSpawnpoints()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (child.GetChild(0).tag == "TurretSpawnpoint")
            {
                for (int ii = 0; ii < child.childCount; ii++)
                {
                    Transform spawnpoint = child.GetChild(ii);
                    spawnpoint.gameObject.SetActive(true);
                }
            }
        }
    }
    #endregion

    #region Private
    /// <summary>
    /// Move the pillar and check end points.
    /// </summary>
    private void Move()
    {
        if (transform.position.y <= endHeight && transform.position.y >= startHeight)
        {
            if(isRaising) transform.Translate(Vector3.up * speed * Time.deltaTime);
            else transform.Translate(Vector3.down * speed * Time.deltaTime);
        }
        else
        {
            if(isRaising) transform.position = new Vector3(transform.position.x, endHeight, transform.position.z);
            else transform.position = new Vector3(transform.position.x, startHeight, transform.position.z);

            isMoving = false;
            if (audioSource != null && movingSound != null) audioSource.Stop();
        }
    }

    /// <summary>
    /// Set variables.
    /// </summary>
    private void SetUp()
    {
        audioSource = GetComponent<AudioSource>();

        startHeight = transform.position.y;
        endHeight = startHeight + height;
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_TurretPillar(string msg)
    {
        if (isDebug_TurretPillar) Debug.Log(isDebugScriptName_TurretPillar + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_TurretPillar(string msg)
    {
        Debug.LogWarning(isDebugScriptName_TurretPillar + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_TurretPillar(string msg)
    {
        Debug.LogError(isDebugScriptName_TurretPillar + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    /// <summary>
    /// Is the pillar moving?
    /// </summary>
    public bool IsMoving
    {
        get
        {
            return isMoving;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_TurretPillar("Debugging enabled.");

        SetUp();
    }
    private void Update()
    {
        if (isMoving) Move();
    }
    #endregion
}