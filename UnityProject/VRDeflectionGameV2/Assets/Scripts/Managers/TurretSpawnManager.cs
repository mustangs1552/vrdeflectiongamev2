﻿/* Created by: Matthew George
 */

using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class TurretSpawnManager : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_TurretSpawnManager = false;
    private string isDebugScriptName_TurretSpawnManager = "TurretSpawnManager";
    #endregion

    #region Public
    public static TurretSpawnManager SINGLETON = null;

    [SerializeField] private float debugSpawnpointBoxWidth = 1;

    [Tooltip("Min and max spawn rate. x = min, y = max.")]
    [SerializeField] private Vector2 spawnRate = new Vector2(1, 5);
    #endregion

    #region Private
    private float lastSpawn = 0;
    private float currSpawnRate = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Reset last turret spawn.
    /// </summary>
    public void StartRound()
    {
        lastSpawn = Time.time;
    }

    /// <summary>
    /// Set the setting for the match from the main menu.
    /// </summary>
    public void SetSettings()
    {
        TrainingModeSettings settings = MatchSettings.SINGLETON.SettingsTrainingMode;
        spawnRate = settings.turretSpawnRate;

        PrintDebugMsg_TurretSpawnManager("SETTINGS | Turrets Spawn Rate: " + spawnRate);
    }
    #endregion

    #region Private
    /// <summary>
    /// Chooses a new random spawn rate.
    /// </summary>
    private void PickNewSpawnRate()
    {
        float randNum = UnityEngine.Random.Range(spawnRate.x, spawnRate.y);
        currSpawnRate = randNum;
        PrintDebugMsg_TurretSpawnManager("New spawn rate: " + currSpawnRate);
    }
    /// <summary>
    /// Chooses a random spawnpoint that is currently available from the TurretManager.
    /// </summary>
    /// <returns>A chosen spawnpoint. Null if none available or errored.</returns>
    private Transform ChooseSpawnpoint()
    {
        List<Transform> availableSpawnpoints = TurretManager.SINGLETON.GetAvailableSpawnpoints();
        if (availableSpawnpoints.Count > 0)
        {
            int randNum = UnityEngine.Random.Range(0, availableSpawnpoints.Count);
            try
            {
                PrintDebugMsg_TurretSpawnManager("Chose " + availableSpawnpoints[randNum].name + " (randNum: " + randNum + ") out of " + availableSpawnpoints.Count + ".");
                return availableSpawnpoints[randNum];
            }
            catch (Exception e)
            {
                PrintErrorDebugMsg_TurretSpawnManager("Exception when choosing a spawnpoint: " + e.ToString() + "\n  randNum = " + randNum + "\n  Total spawnpoints: " + availableSpawnpoints.Count);
                return null;
            }
        }

        PrintDebugMsg_TurretSpawnManager("No spawnpoints available!");
        return null;
    }

    /// <summary>
    /// Attempt to spawn a new turret at a random available spawnpoint.
    /// </summary>
    private void AttemptTurretSpawn()
    {
        PrintDebugMsg_TurretSpawnManager("Attempting to spawn a turret...");
        Transform spawnpoint = ChooseSpawnpoint();
        if (spawnpoint != null)
        {
            bool success = TurretManager.SINGLETON.SpawnTurret(ChooseSpawnpoint());
            if (success)
            {
                PrintDebugMsg_TurretSpawnManager("Successfull!");
                PickNewSpawnRate();
                lastSpawn = Time.time;
            }
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Highlights all spawnpoints available.
    /// </summary>
    private void HighlightAvailableSpawnpoints()
    {
        if(isDebug_TurretSpawnManager)
        {
            List<Transform> availableSpawns = TurretManager.SINGLETON.GetAvailableSpawnpoints();
            foreach(Transform spawnpoint in availableSpawns)
            {
                Vector3 left = spawnpoint.position + Vector3.left * debugSpawnpointBoxWidth;
                Vector3 top = spawnpoint.position + Vector3.up * debugSpawnpointBoxWidth;
                Vector3 right = spawnpoint.position + Vector3.right * debugSpawnpointBoxWidth;
                Vector3 bot = spawnpoint.position + Vector3.down * debugSpawnpointBoxWidth;

                Debug.DrawLine(left, top, Color.green);
                Debug.DrawLine(top, right, Color.green);
                Debug.DrawLine(right, bot, Color.green);
                Debug.DrawLine(bot, left, Color.green);
            }
        }
    }

    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_TurretSpawnManager(string msg)
    {
        if (isDebug_TurretSpawnManager) Debug.Log(isDebugScriptName_TurretSpawnManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_TurretSpawnManager(string msg)
    {
        Debug.LogWarning(isDebugScriptName_TurretSpawnManager + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_TurretSpawnManager(string msg)
    {
        Debug.LogError(isDebugScriptName_TurretSpawnManager + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_TurretSpawnManager("Debugging enabled.");

        if (TurretSpawnManager.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_TurretSpawnManager("More than one TurretSpawnManager singletons found!");

        lastSpawn = Time.time;
        PickNewSpawnRate();
    }
    private void Update()
    {
        if (TrainingGameModeManager.SINGLETON.State == TraingingGameModeState.Playing && Time.time - lastSpawn >= currSpawnRate) AttemptTurretSpawn();

        HighlightAvailableSpawnpoints();
    }
    #endregion
}