﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class MissileWarhead : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_MissileWarhead = false;
    private string isDebugScriptName_MissileWarhead = "MissileWarhead";
    #endregion

    #region Public
    [SerializeField] private GameObject explosionPrefab = null;
    #endregion

    #region Private
    private Missile mainPart = null;
    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private

    #endregion

    #region Unity Functions
    private void OnTriggerEnter(Collider other)
    {
        PrintDebugMsg_MissileWarhead("Triggering explosion...");

        GameObject explosion = ObjectManager.SINGLETON.CheckAddObject(explosionPrefab);
        if (explosion == null) explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        else
        {
            explosion.transform.position = transform.position;
            explosion.transform.rotation = Quaternion.identity;
        }
        explosion.gameObject.SetActive(true);

        mainPart.RemoveSelf();
    }
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_MissileWarhead(string msg)
    {
        if (isDebug_MissileWarhead) Debug.Log(isDebugScriptName_MissileWarhead + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_MissileWarhead(string msg)
    {
        Debug.LogWarning(isDebugScriptName_MissileWarhead + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_MissileWarhead(string msg)
    {
        Debug.LogError(isDebugScriptName_MissileWarhead + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_MissileWarhead("Debugging enabled.");

        mainPart = transform.GetComponentInParent<Missile>();
        if (mainPart == null) PrintErrorDebugMsg_MissileWarhead("Main part not found!");
        PrintDebugMsg_MissileWarhead(ObjectUtility.ParentUtility.GetHighestParent(transform).name);
    }
    #endregion
}