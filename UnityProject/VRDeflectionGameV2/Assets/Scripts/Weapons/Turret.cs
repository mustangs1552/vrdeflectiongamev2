﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class Turret : MonoBehaviour
{
    #region Global Variables
    [Header("Turret")]

    #region Default Variables
    [SerializeField] private bool isDebug_Turret = false;
    private string isDebugScriptName_Turret = "Turret";
    #endregion

    #region Public
    [Header("Shooting")]
    [SerializeField] private GameObject laser = null;
    [SerializeField] private float damage = 5;
    [SerializeField] protected float aimSpeed = 1;
    [SerializeField] private AudioClip[] shotClips;
    [SerializeField] private visualEffectNames fireingEffect = 0;
    [Header("Movement")]
    [SerializeField] private float revealSpeed = 1;
    [SerializeField] private float fullyRevealedAmount = 1;
    [SerializeField] private float postRevealDelay = 1;
    [SerializeField] protected float postFireDelay = 1;
    [SerializeField] protected bool autoConceal = true;
    [SerializeField] private float stunDuration = 5;
    [Header("Effects")]
    [SerializeField] private GameObject levitaionDevice = null;
    [SerializeField] private float levitationDevicePos = 1;
    [SerializeField] private GameObject stunEffectLight = null;
    [SerializeField] private visualEffectNames[] stunEffects;
    [SerializeField] private Vector3 minSpawnPos = -Vector3.one;
    [SerializeField] private Vector3 maxSpawnPos = Vector3.one;
    [SerializeField] private Vector2 minMaxSpawnRate = new Vector2(0, 1);
    [SerializeField] private AudioClip[] stunBoomClips = null;
    #endregion

    #region Private
    private AudioSource audioSource = null;

    private List<Transform> vrPlayerPieces = new List<Transform>();
    private Transform laserSpawnpoint = null;

    protected Transform currTarget = null;

    private bool stunned = false;
    private bool revealing = false;
    protected bool concealing = false;
    protected bool straightening = false;
    protected Vector3 startPos = Vector3.zero;
    private Vector3 startRotTarget = Vector3.zero;
    private Vector3 revealedPos = Vector3.zero;
    protected float lastDist = 0;

    private Transform usedSpawnpoint = null;
    private SlidingDoor spawnpointDoor = null;
    private bool closingDoor = false;

    private float randFloat = 0;
    private int randInt = 0;

    private Transform spawnedLDevice = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Aim and fire at the VR player.
    /// </summary>
    public void Fire()
    {
        AimAndFire();
    }
    /// <summary>
    /// Aim and fire at the given target.
    /// </summary>
    /// <param name="target">Target to aim and fire at.</param>
    public void Fire(Transform target)
    {
        AimAndFire(target);
    }

    /// <summary>
    /// Start revealing turret, aim, then fire when ready. Saves starting position and rotation before beggining.
    /// </summary>
    public void RevealAndFire(Transform spawnpoint)
    {
        PrintDebugMsg_Turret("Begginning to reveal...");

        usedSpawnpoint = spawnpoint;
        spawnpointDoor = spawnpoint.GetComponent<SlidingDoor>();
        transform.position = usedSpawnpoint.position;
        startPos = transform.position;
        revealedPos = transform.position + transform.forward * fullyRevealedAmount;
        startRotTarget = revealedPos + transform.forward;
        PrintDebugMsg_Turret("  Start Position: " + startPos + " | Revealed Position: " + revealedPos + " | Start Rotation Target: " + startRotTarget);

        revealing = true;
        if(spawnpointDoor != null) spawnpointDoor.OpenDoor();
        lastDist = Vector3.Distance(transform.position, revealedPos);
        spawnedLDevice.gameObject.SetActive(true);
    }

    /// <summary>
    /// Stun the turret and pause all actions.
    /// </summary>
    public void Stun()
    {
        PrintDebugMsg_Turret("Stunned!");
        stunned = true;
        Invoke("StunEffect", 0);
        if (stunEffectLight != null) stunEffectLight.SetActive(true);
        Invoke("UnStun", stunDuration);
    }

    /// <summary>
    /// Hideshow the particles of the levitation device.
    /// </summary>
    public void UpdateLevitaionDeviceParticles(bool darknessMode)
    {
        levitaionDevice.GetComponent<LevitationDevice>().DarknessMode = darknessMode;
    }

    /// <summary>
    /// Resets this turret to prepare for next round.
    /// </summary>
    public void ResetObject()
    {
        CancelInvoke("UnStun");
        CancelInvoke("StartConceal");
        CancelInvoke("FireLaser");

        stunned = false;
        revealing = false;
        concealing = false;
        straightening = false;

        currTarget = null;
        usedSpawnpoint = null;
        spawnedLDevice.gameObject.SetActive(false);
        gameObject.SetActive(false);
        CancelInvoke("StunEffect");
        if (stunEffectLight != null) stunEffectLight.SetActive(false);
    }

    /// <summary>
    /// Sets up the levitation device if one is not yet set up.
    /// </summary>
    public void SetupLevitationDevice()
    {
        if (levitaionDevice != null)
        {
            GameObject spawnedLDeviceGO = ObjectManager.SINGLETON.CheckAddObject(levitaionDevice);
            if (spawnedLDeviceGO == null) spawnedLDevice = Instantiate(levitaionDevice, transform.position + (Vector3.down * levitationDevicePos), Quaternion.identity).transform;
            else
            {
                spawnedLDevice = spawnedLDeviceGO.transform;
                spawnedLDevice.position = transform.position + (Vector3.down * levitationDevicePos);
                spawnedLDevice.rotation = Quaternion.identity;
            }
            spawnedLDevice.gameObject.SetActive(false);
        }
    }
    #endregion

    #region Private
    /// <summary>
    /// Sets up any objects that need to be assigned.
    /// </summary>
    private void SetUp()
    {
        PrintDebugMsg_Turret("Setting up...");

        audioSource = GetComponent<AudioSource>();

        // Find VR player
        GameObject[] vrPlayerPiecesFound = GameObject.FindGameObjectsWithTag("VRPlayer");
        foreach(GameObject piece in vrPlayerPiecesFound)
        {
            if (piece.GetComponent<HealthPiece>() != null) vrPlayerPieces.Add(piece.transform);
        }
        if (vrPlayerPieces.Count == 0) PrintDebugMsg_Turret("  Found the VRPlayer.");

        // Find needed children
        for(int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == "LaserSpawnpoint")
            {
                laserSpawnpoint = transform.GetChild(i);
                break;
            }
            else if(transform.GetChild(i).childCount > 0)
            {
                for(int ii = 0; ii < transform.GetChild(i).childCount; ii++)
                {
                    if (transform.GetChild(i).GetChild(ii).name == "LaserSpawnpoint")
                    {
                        laserSpawnpoint = transform.GetChild(i).GetChild(ii);
                        break;
                    }
                }
            }
        }
        if(laserSpawnpoint == null) PrintErrorDebugMsg_Turret("  No LaserSpawnpoint found.");

        // Set variables
        startPos = transform.position;
        revealedPos = transform.position + transform.forward * fullyRevealedAmount;
        startRotTarget = revealedPos + transform.forward;
        PrintDebugMsg_Turret("  Start Position: " + startPos + " | Revealed Position: " + revealedPos + " | Start Rotation Target: " + startRotTarget);

        if (stunEffectLight != null) stunEffectLight.SetActive(false);

        SetupLevitationDevice();
    }

    /// <summary>
    /// Disables self and informs TurretManager so that it can make this spawnpoint available for new turrets.
    /// </summary>
    private void DisableSelf()
    {
        PrintDebugMsg_Turret("Disabling self and informing TurretManager...");
        TurretManager.SINGLETON.MakeSpawnpointAvailable(usedSpawnpoint);
        usedSpawnpoint = null;
        spawnpointDoor = null;
        gameObject.SetActive(false);
        if (stunEffectLight != null) stunEffectLight.gameObject.SetActive(false);
        spawnedLDevice.gameObject.SetActive(false);
    }

    #region Movement
    /// <summary>
    /// Move turret forward to reveal itself.
    /// </summary>
    private void Revealing()
    {
        PrintDebugMsg_Turret("Revealing...");

        if (spawnpointDoor == null || spawnpointDoor.IsOpen)
        {
            transform.Translate(Vector3.forward * revealSpeed * Time.deltaTime);
            if (Vector3.Distance(transform.position, revealedPos) <= .1)
            {
                PrintDebugMsg_Turret("Finished revealing!");
                revealing = false;
                //transform.position = revealedPos;
                AimAndFire();
            }
            else if (lastDist < Vector3.Distance(transform.position, revealedPos))
            {
                PrintWarningDebugMsg_Turret("Finished revealing! However, missed stopping point.");
                revealing = false;
                //transform.position = revealedPos;
                AimAndFire();
            }

            lastDist = Vector3.Distance(transform.position, revealedPos);
        }
    }
    /// <summary>
    /// Move turret backwards to conceal it.
    /// </summary>
    private void Concealing()
    {
        PrintDebugMsg_Turret("Concealing...");
        
        transform.Translate(-(Vector3.forward * revealSpeed * Time.deltaTime));
        if (Vector3.Distance(transform.position, startPos) <= .1)
        {
            PrintDebugMsg_Turret("Finished concealing!");
            concealing = false;
            //transform.position = startPos;
            if (spawnpointDoor != null)
            {
                spawnpointDoor.CloseDoor();
                closingDoor = true;
            }
        }
        else if(lastDist < Vector3.Distance(transform.position, startPos))
        {
            PrintWarningDebugMsg_Turret("Finished concealing! However, missed stopping point.");
            concealing = false;
            //transform.position = startPos;
            if (spawnpointDoor != null)
            {
                spawnpointDoor.CloseDoor();
                closingDoor = true;
            }
        }

        lastDist = Vector3.Distance(transform.position, startPos);
    }
    /// <summary>
    /// Start straightening out the turret before concealing.
    /// </summary>
    private void StartConceal()
    {
        if(!stunned) straightening = true;
        else if(IsInvoking("FireLaser")) Invoke("StartConceal", postRevealDelay + postFireDelay);
        else Invoke("StartConceal", postFireDelay);
    }
    /// <summary>
    /// Straighten out the turret to its starting rotation.
    /// </summary>
    protected virtual void Straighten()
    {
        PrintDebugMsg_Turret("Straightening...");

        Vector3 targetDir = startRotTarget - transform.position;
        float step = aimSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0);
        transform.rotation = Quaternion.LookRotation(newDir);

        Vector3 dirFromTarget = (startRotTarget - transform.position).normalized;
        float dotProd = Vector3.Dot(dirFromTarget, transform.forward);
        if (dotProd >= .999)
        {
            PrintDebugMsg_Turret("Finished straightening...");
            concealing = true;
            straightening = false;
            lastDist = Vector3.Distance(transform.position, startPos);
        }
    }
    #endregion

    #region Aiming
    /// <summary>
    /// Prepares to aim a random VR player piece.
    /// </summary>
    private void AimAndFire()
    {
        int randNum = Random.Range(0, vrPlayerPieces.Count);
        PrintDebugMsg_Turret("Random number: " + randNum + "/" + (vrPlayerPieces.Count - 1) + ")");
        currTarget = vrPlayerPieces[randNum];
        PrintDebugMsg_Turret("Starting to aim at " + currTarget.name + "...");
        AimTowardsAndFire();
    }
    /// <summary>
    /// Prepares to aim at the given target.
    /// </summary>
    /// <param name="target">Target to aim at.</param>
    private void AimAndFire(Transform target)
    {
        currTarget = target;
        PrintDebugMsg_Turret("Starting to aim at " + currTarget.name + "...");
        AimTowardsAndFire();
    }
    /// <summary>
    /// Continues to aim at the currently assigned target.
    /// </summary>
    private void ContinueAiming()
    {
        PrintDebugMsg_Turret("Continuing to aim at " + currTarget.name + "...");
        AimTowardsAndFire();
    }
    /// <summary>
    /// Aims towards the currently assigned target for only one step (Does not continue to aim towards target).
    /// </summary>
    protected virtual void AimTowards()
    {
        if (currTarget != null)
        {
            Vector3 targetDir = currTarget.position - transform.position;
            float step = aimSpeed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0);
            transform.rotation = Quaternion.LookRotation(newDir);
        }
        else PrintErrorDebugMsg_Turret("No currTarget!");
    }
    /// <summary>
    /// Aims towards the currently assigned target for only one step (Does not continue to aim towards target). If it is aiming at target then it will fire.
    /// </summary>
    protected virtual void AimTowardsAndFire()
    {
        AimTowards();

        Vector3 dirFromTarget = (currTarget.position - transform.position).normalized;
        float dotProd = Vector3.Dot(dirFromTarget, transform.forward);
        if(dotProd >= .999)
        {
            transform.LookAt(currTarget);
            PrintDebugMsg_Turret("Finished aiming at " + currTarget.name + "!");
            currTarget = null;
            if (autoConceal) FireLaserAndConceal();
            else Invoke("FireLaser", postFireDelay);
        }
    }
    #endregion

    #region Firing
    /// <summary>
    /// Fires the laser that this turret is assigned.
    /// </summary>
    private void FireLaser()
    {
        if (!stunned)
        {
            PrintDebugMsg_Turret("Firing...");
            GameObject spawnedLaser = ObjectManager.SINGLETON.CheckAddObject(laser);
            if (spawnedLaser == null) spawnedLaser = Instantiate(laser, laserSpawnpoint.position, laserSpawnpoint.rotation);
            else
            {
                spawnedLaser.transform.position = laserSpawnpoint.position;
                spawnedLaser.transform.rotation = laserSpawnpoint.rotation;
            }
            spawnedLaser.gameObject.SetActive(true);
            spawnedLaser.GetComponent<Laser>().Fired(transform, damage);
            currTarget = null;

            if (shotClips.Length > 0)
            {
                int randNum = Random.Range(0, shotClips.Length - 1);
                audioSource.clip = shotClips[randNum];
                audioSource.Play();
            }

            visEffects.createEffect((int)fireingEffect, laserSpawnpoint.position);
        }
        else Invoke("FireLaser", postRevealDelay);
    }
    /// <summary>
    /// Fires the laser that this turret is assigned. Then starts straightening out to conceal itself.
    /// </summary>
    protected void FireLaserAndConceal()
    {
        PrintDebugMsg_Turret("Firing...");
        Invoke("FireLaser", postRevealDelay);
        Invoke("StartConceal", postRevealDelay + postFireDelay);
    }
    #endregion

    /// <summary>
    /// Make to turret no longer stunned.
    /// </summary>
    private void UnStun()
    {
        PrintDebugMsg_Turret("No longer stunned!");
        stunned = false;
        CancelInvoke("StunEffect");
        if(stunEffectLight != null) stunEffectLight.SetActive(false);
    }

    /// <summary>
    /// Make a stun particle effect happen once in a random position and with a new random interval.
    /// </summary>
    private void StunEffect()
    {
        if (stunned || isDebug_Turret)
        {
            if (stunEffects.Length > 0)
            {
                randInt = Random.Range(0, stunEffects.Length);
                visualEffectNames effect = stunEffects[randInt];

                Vector3 pos = new Vector3(Random.Range(minSpawnPos.x, maxSpawnPos.x), Random.Range(minSpawnPos.y, maxSpawnPos.y), Random.Range(minSpawnPos.z, maxSpawnPos.z));
                visEffects.createEffect((int)effect, transform.position + pos);
            }

            if (stunBoomClips.Length > 0)
            {
                audioSource.clip = stunBoomClips[Random.Range(0, stunBoomClips.Length)];
                audioSource.Play();
            }

            randFloat = Random.Range(minMaxSpawnRate.x, minMaxSpawnRate.y);
            Invoke("StunEffect", randFloat);
        }
    }

    /// <summary>
    /// Updates the position of the levitation device underneath the turret.
    /// </summary>
    private void UpdateLevitationDevice()
    {
        spawnedLDevice.position = transform.position + (Vector3.down * levitationDevicePos);
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Turret(string msg)
    {
        if (isDebug_Turret) Debug.Log(isDebugScriptName_Turret + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Turret(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Turret + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Turret(string msg)
    {
        Debug.LogError(isDebugScriptName_Turret + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    
    #endregion
    #endregion

    #region Start & Update Functions
    protected void Awake()
    {
        PrintDebugMsg_Turret("Debugging enabled.");

        SetUp();
    }
    private void Update()
    {
        if (!stunned)
        {
            if (revealing) Revealing();
            else if (concealing) Concealing();
            else if (straightening) Straighten();

            if(closingDoor && spawnpointDoor.IsClosed)
            {
                DisableSelf();
                closingDoor = false;
            }

            if (currTarget != null) ContinueAiming();
        }

        if (isDebug_Turret && !IsInvoking("StunEffect")) StunEffect();

        UpdateLevitationDevice();
    }
    #endregion
}