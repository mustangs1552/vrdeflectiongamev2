﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class Laser : Projectile
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_Laser = false;
    private string isDebugScriptName_Laser = "Laser";
    #endregion

    #region Public
    [SerializeField] private visualEffectNames impactEffect = 0;
    [SerializeField] private float flashIntensity = 5;
    [SerializeField] private float flashDuration = .25f;
    #endregion

    #region Private
    private Transform shooter = null;
    private float damage = 0;

    private Light ptLight = null;
    private float startIntensity = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Fired by an object.
    /// </summary>
    /// <param name="shotBy">The object that shot this laser.</param>
    /// <param name="dmg">Laser's damage.</param>
    public void Fired(Transform shotBy, float dmg)
    {
        shooter = shotBy;
        damage = dmg;

        ptLight.intensity = flashIntensity;
        ResetLifespan();
        Invoke("EndFlash", flashDuration);
        ResetCurrSpeed();
    }

    /// <summary>
    /// Makes the laser look at the shooter and move towards them. If the shooter is not active or exists then simply spin 180.
    /// </summary>
    public void Redirect(Transform sword, float redirectSpeed = 0)
    {
        PrintDebugMsg_Laser("Redirecting to " + ((shooter != null) ? shooter.name : "nothing") + "...");

        if (shooter != null && shooter.gameObject.activeInHierarchy) transform.LookAt(shooter);
        else transform.Rotate(Vector3.left * 180, Space.Self);

        if (sword.parent.name == "RightHandAnchor") TrainingGameModeManager.SINGLETON.VRPlayer.GenerateHapticFeedback(true);
        else if (sword.parent.name == "LeftHandAnchor") TrainingGameModeManager.SINGLETON.VRPlayer.GenerateHapticFeedback(false);
        else PrintErrorDebugMsg_Laser("No hand anchor found!");

        ResetLifespan();
        PrintDebugMsg_Laser("Pre-re-direct speed: " + CurrSpeed);
        CurrSpeed += redirectSpeed;
        PrintDebugMsg_Laser("Post-re-direct speed: " + CurrSpeed);
    }
    #endregion

    #region Private
    /// <summary>
    /// End the higher intensity period.
    /// </summary>
    private void EndFlash()
    {
        ptLight.intensity = startIntensity;
    }
    #endregion

    #region Unity Functions
    private void OnTriggerEnter(Collider other)
    {
        PrintDebugMsg_Laser("Hit an object!");

        if (other.gameObject.tag == "PlasmaSword")
        {
            PrintDebugMsg_Laser("Hit the plasma sword!");
            PlasmaSword pSword = other.GetComponent<PlasmaSword>();
            Redirect(other.transform, pSword.GetSwingForce());
            visEffects.createEffect((int)impactEffect, transform.position);
            return;
        }
        HealthPiece targetHealthPeice = other.gameObject.GetComponent<HealthPiece>();
        if (targetHealthPeice != null)
        {
            PrintDebugMsg_Laser("Hit " + other.gameObject.name + " which has health!");
            if (targetHealthPeice.TakeDamage(damage, gameObject.tag)) PrintDebugMsg_Laser(other.gameObject.name + " is a valid contact! Damage dealt!");
            else PrintDebugMsg_Laser("Not a valid target. Not dealing damage.");
            RemoveSelf();
            return;
        }
        Turret targetTurret = other.gameObject.GetComponent<Turret>();
        if (targetTurret != null)
        {
            PrintDebugMsg_Laser("Hit " + other.gameObject.name + " which is a turret!");
            targetTurret.Stun();
            RemoveSelf();
            visEffects.createEffect((int)impactEffect, transform.position);
            return;
        }
        MonitorPlayer targetMPlayer = other.gameObject.GetComponent<MonitorPlayer>();
        if (targetMPlayer != null)
        {
            PrintDebugMsg_Laser("Hit " + other.gameObject.name + " which is a monitor player!");
            targetMPlayer.Death();
            RemoveSelf();
            return;
        }

        PrintDebugMsg_Laser("Hit nothing important.");
        visEffects.createEffect((int)impactEffect, transform.position);
        RemoveSelf();
    }
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Laser(string msg)
    {
        if (isDebug_Laser) Debug.Log(isDebugScriptName_Laser + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Laser(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Laser + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Laser(string msg)
    {
        Debug.LogError(isDebugScriptName_Laser + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// The object that shot this laser.
    /// </summary>
    public Transform Shooter
    {
        get
        {
            return shooter;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_Laser("Debugging enabled.");

        ptLight = GetComponentInChildren<Light>();
        startIntensity = ptLight.intensity;
    }
    protected override void Update()
    {
        if (shooter != null && !shooter.gameObject.activeInHierarchy)
        {
            PrintDebugMsg_Laser("Shooter removed.");
            shooter = null;
        }

        base.Update();
    }
    #endregion
}