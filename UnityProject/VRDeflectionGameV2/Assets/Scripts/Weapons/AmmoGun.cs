﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class AmmoGun : Gun
{
    #region Global Variables
    [Header("AmmoGun")]
    #region Default Variables
    [SerializeField] private bool isDebug_AmmoGun = false;
    private string isDebugScriptName_AmmoGun = "AmmoGun";
    #endregion

    #region Public
    [SerializeField] private int maxAmmo = 10;
    [SerializeField] private int clipSize = 1;
    [SerializeField] private float reloadTime = 2;
    #endregion

    #region Private
    private int currTotalAmount = 0;
    private int currClipAmount = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Starts reloading the clip.
    /// </summary>
    public void StartReloading()
    {
        PrintDebugMsg_AmmoGun("Now reloading...");
        Invoke("Reload", reloadTime);
    }

    /// <summary>
    /// Re-supply the gun by the amount given.
    /// </summary>
    /// <param name="amount">The amount to re-supply.</param>
    /// <returns>True if successful.</returns>
    public override bool ReSupply(float amount)
    {
        if (currTotalAmount < maxAmmo)
        {
            currTotalAmount += (int)amount;
            if (currTotalAmount > maxAmmo) currTotalAmount = maxAmmo;

            PrintDebugMsg_AmmoGun("Reloaded by " + ((int)amount) + " and is now " + currTotalAmount + ".");
            return true;
        }

        PrintDebugMsg_AmmoGun("Ammo full.");
        return false;
    }
    /// <summary>
    /// ReRe-supply the gun to its max amount.
    /// </summary>
    /// <returns>True if successful.</returns>
    public override bool ReSupplyFull()
    {
        return ReSupply(maxAmmo);
    }

    /// <summary>
    /// If no ammo in the clip then start reloading once selected.
    /// </summary>
    public override void OnWeaponSelected()
    {
        PrintDebugMsg_AmmoGun(gameObject.name + " selected.");
        if (currClipAmount <= 0) StartReloading();
    }
    /// <summary>
    /// When being put away cancel reloading (no reloading while in your pocket).
    /// </summary>
    public override void OnWeaponDeSelected()
    {
        PrintDebugMsg_AmmoGun(gameObject.name + " de-selected.");
        if (IsInvoking("Reload")) CancelInvoke("Reload");
    }
    #endregion

    #region Private
    /// <summary>
    /// Check to see if able to fire.
    /// </summary>
    /// <returns>Returns true if able.</returns>
    protected override bool CheckSupply()
    {
        if(currClipAmount > 0 && !IsInvoking("Reload"))
        {
            PrintDebugMsg_AmmoGun("Has ammo available! Ammo at " + currClipAmount + "/" + currTotalAmount + ".");
            return true;
        }

        return false;
    }
    /// <summary>
    /// Adjusts supply after shot.
    /// </summary>
    protected override void PostShot()
    {
        currClipAmount--;
        if(currClipAmount <= 0)
        {
            currClipAmount = 0;
            StartReloading();
        }

        PrintDebugMsg_AmmoGun("Shot! Ammo now at " + currClipAmount + "/" + currTotalAmount + ".");
    }
    
    /// <summary>
    /// Reloads the clip to its max or as much that is available.
    /// </summary>
    private void Reload()
    {
        int amountToFill = clipSize - currClipAmount;
        if(amountToFill > 0 && currTotalAmount > 0)
        {
            if (currTotalAmount < amountToFill) amountToFill = currTotalAmount;
            currClipAmount += amountToFill;
            currTotalAmount -= amountToFill;
            PrintDebugMsg_AmmoGun("Finished reloading! Ammo now at " + currClipAmount + "/" + currTotalAmount + ".");
        }
    }

    /// <summary>
    /// Sets up variables.
    /// </summary>
    private void Setup()
    {
        currClipAmount = clipSize;
        currTotalAmount = maxAmmo;
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_AmmoGun(string msg)
    {
        if (isDebug_AmmoGun) Debug.Log(isDebugScriptName_AmmoGun + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_AmmoGun(string msg)
    {
        Debug.LogWarning(isDebugScriptName_AmmoGun + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_AmmoGun(string msg)
    {
        Debug.LogError(isDebugScriptName_AmmoGun + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        base.Awake();
        PrintDebugMsg_AmmoGun("Debugging enabled.");

        Setup();
    }
    #endregion
}