﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class DualAxisTurret : Turret
{
    #region Global Variables
    [Header("DualAxisTurret")]

    #region Default Variables
    [SerializeField] private bool isDebug_DualAxisTurret = false;
    private string isDebugScriptName_DualAxisTurret = "DualAxisTurret";
    #endregion

    #region Public
    [SerializeField] private Transform gun = null;
    [SerializeField] private Transform lookAtObj = null;
    #endregion

    #region Private
    private Vector3 gunStartRot = Vector3.zero;
    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private
    private void RotateGun(bool straightening)
    {
        Vector3 targetDir = ((!straightening) ? currTarget.position : gunStartRot) - gun.position;
        float step = aimSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(gun.forward, targetDir, step, 0);
        Quaternion newRot = Quaternion.LookRotation(newDir);
        gun.localRotation = new Quaternion(newRot.x, gun.localRotation.y, gun.localRotation.z, newRot.w);
    }
    private void RotateBase(bool straightening)
    {
        Vector3 targetDir = ((!straightening) ? currTarget.position : gunStartRot) - transform.position;
        float step = aimSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0);
        Quaternion newRot = Quaternion.LookRotation(newDir);
        transform.rotation = new Quaternion(transform.rotation.x, newRot.y, transform.rotation.z, newRot.w);
    }

    protected override void Straighten()
    {
        RotateBase(true);
        RotateGun(true);

        Vector3 dirFromTarget = (gunStartRot - gun.position).normalized;
        float dotProd = Vector3.Dot(dirFromTarget, transform.forward);
        if (dotProd >= .999)
        {
            PrintDebugMsg_DualAxisTurret("Finished straightening...");
            concealing = true;
            straightening = false;
            lastDist = Vector3.Distance(transform.position, startPos);
        }
    }
    protected override void AimTowards()
    {
        if (currTarget != null)
        {
            RotateBase(false);
            RotateGun(false);
        }
        else PrintErrorDebugMsg_DualAxisTurret("No current target!");
    }
    protected override void AimTowardsAndFire()
    {
        AimTowards();

        Vector3 dirFromTarget = (currTarget.position - gun.position).normalized;
        float dotProd = Vector3.Dot(dirFromTarget, gun.forward);
        if (dotProd >= .999)
        {
            lookAtObj.LookAt(currTarget);
            transform.rotation = new Quaternion(transform.rotation.x, lookAtObj.rotation.y, transform.rotation.z, lookAtObj.rotation.w);
            gun.LookAt(currTarget);

            PrintDebugMsg_DualAxisTurret("Finished aiming at " + currTarget.name + "!");
            currTarget = null;
            if (autoConceal) FireLaserAndConceal();
            else Invoke("FireLaser", postFireDelay);
        }
    }
    #endregion

    #region Unity Functions
    private void OnEnable()
    {
        gunStartRot = gun.position + gun.forward * 10;
    }
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_DualAxisTurret(string msg)
    {
        if (isDebug_DualAxisTurret) Debug.Log(isDebugScriptName_DualAxisTurret + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_DualAxisTurret(string msg)
    {
        Debug.LogWarning(isDebugScriptName_DualAxisTurret + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_DualAxisTurret(string msg)
    {
        Debug.LogError(isDebugScriptName_DualAxisTurret + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        base.Awake();
        PrintDebugMsg_DualAxisTurret("Debugging enabled.");
    }
    #endregion
}