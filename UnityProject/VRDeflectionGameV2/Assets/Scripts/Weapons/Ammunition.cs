﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class Ammunition : PickUp
{
    #region Global Variables
    [Header("Ammunition")]
    #region Default Variables
    [SerializeField] private bool isDebug_Ammunition = false;
    private string isDebugScriptName_Ammunition = "Ammunition";
    #endregion
    
    #region Public
    [SerializeField] private bool isCharge = true;
    [SerializeField] private int amount = 10;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// When picked-up, tell the player to attempt to re-supply thier current weapon.
    /// </summary>
    /// <param name="player">The player that picked this up.</param>
    public override void OnPickUp(GameObject player)
    {
        MonitorPlayerWeapons mpWeapons = player.GetComponent<MonitorPlayerWeapons>();
        if (mpWeapons != null)
        {
            PrintDebugMsg_Ammunition("Attempting to re-supply " + player.name + "'s current weapon...");

            if (mpWeapons.ReSupplyCurrentWeapon(isCharge, amount))
            {
                PrintDebugMsg_Ammunition("Successfully re-supplied.");
                RemoveSelf();
            }
            else PrintDebugMsg_Ammunition("Unable to re-supply.");
        }
        else PrintErrorDebugMsg_Ammunition("Only monitor players can pickup ammunition!");
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Ammunition(string msg)
    {
        if (isDebug_Ammunition) Debug.Log(isDebugScriptName_Ammunition + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Ammunition(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Ammunition + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Ammunition(string msg)
    {
        Debug.LogError(isDebugScriptName_Ammunition + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_Ammunition("Debugging enabled.");
    }
    #endregion
}