﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class WeaponPickup : PickUp
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_WeaponPickup = false;
    private string isDebugScriptName_WeaponPickup = "WeaponPickup";
    #endregion

    #region Public
    [Range(0, 3)] [SerializeField] private int weaponNum = 0;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// When picked up, tells the player to attempt to pick this weapon up.
    /// </summary>
    /// <param name="player">The player picking up this weapon.</param>
    public override void OnPickUp(GameObject player)
    {
        MonitorPlayerWeapons mpWeapons = player.GetComponent<MonitorPlayerWeapons>();
        if (mpWeapons != null)
        {
            PrintDebugMsg_WeaponPickup(player.name + " is attempting to pickup a new weapon...");
            if(mpWeapons.PickupWeapon(weaponNum))
            {
                PrintDebugMsg_WeaponPickup("Successfully pickedup.");
                RemoveSelf();
            }
        }
        else PrintDebugMsg_WeaponPickup("Only monitor players can pickup weapons.");
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_WeaponPickup(string msg)
    {
        if (isDebug_WeaponPickup) Debug.Log(isDebugScriptName_WeaponPickup + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_WeaponPickup(string msg)
    {
        Debug.LogWarning(isDebugScriptName_WeaponPickup + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_WeaponPickup(string msg)
    {
        Debug.LogError(isDebugScriptName_WeaponPickup + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_WeaponPickup("Debugging enabled.");
    }
    #endregion
}