﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class PlasmaSword : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_PlasmaSword = false;
    private string isDebugScriptName_PlasmaSword = "PlasmaSword";
    #endregion

    #region Public
    [SerializeField] private GameObject energyEmitter = null;

    [SerializeField] private float redirectMultiplier = 2;
    [SerializeField] private Transform tip = null;
    [SerializeField] private float minUpdateDist = .1f;
    #endregion

    #region Private
    private Vector3 lastPos = Vector3.zero;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Gets the swing force via distance of the tip last update and multiplier.
    /// </summary>
    /// <returns></returns>
    public float GetSwingForce()
    {
        float dist = Vector3.Distance(tip.position, lastPos);
        return redirectMultiplier * dist;
    }
    public Vector3 GetSwingDir()
    {
        return tip.position - lastPos;
    }

    /// <summary>
    /// Hide the energy of saber and disable collider.
    /// </summary>
    public void EnableEnergy()
    {
        energyEmitter.SetActive(true);
        GetComponent<Collider>().enabled = true;
    }
    /// <summary>
    /// Show the energy of saber and enable collider.
    /// </summary>
    public void DisableEnergy()
    {
        energyEmitter.SetActive(false);
        GetComponent<Collider>().enabled = false;
    }
    #endregion

    #region Private
    /// <summary>
    /// Updates the position of the tip if minnimum distance is reached.
    /// </summary>
    private void UpdateLastPos()
    {
        if(Vector3.Distance(tip.position, lastPos) >= minUpdateDist) lastPos = tip.position;
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_PlasmaSword(string msg)
    {
        if (isDebug_PlasmaSword) Debug.Log(isDebugScriptName_PlasmaSword + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_PlasmaSword(string msg)
    {
        Debug.LogWarning(isDebugScriptName_PlasmaSword + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_PlasmaSword(string msg)
    {
        Debug.LogError(isDebugScriptName_PlasmaSword + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_PlasmaSword("Debugging enabled.");
    }
    private void Update()
    {
        UpdateLastPos();
    }
    #endregion
}