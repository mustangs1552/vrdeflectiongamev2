﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class ChargeGun : Gun
{
    #region Global Variables
    [Header("Charge Gun")]

    #region Default Variables
    [SerializeField] private bool isDebug_ChargeGun = false;
    private string isDebugScriptName_ChargeGun = "ChargeGun";
    #endregion

    #region Public
    [SerializeField] [Range(0, 100)] private float chargeDepletionPerShot = 5;
    [SerializeField] [Range(0, 100)] private float passiveRechargeAmount = 1;
    [SerializeField] private float passiveRechargeFrequency = 1;
    #endregion

    #region Private
    private float currCharge = 100;
    private float lastCharge = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Re-supplies this weapon if not already full.
    /// </summary>
    /// <param name="amount">Amount to be re-supplied.</param>
    /// <returns>True if successful.</returns>
    public override bool ReSupply(float amount)
    {
        if(currCharge < 100)
        {
            currCharge += amount;
            if (currCharge > 100) currCharge = 100;

            PrintDebugMsg_ChargeGun("re-charged " + gameObject.name + " by " + amount + " and is now " + currCharge + ".");
            return true;
        }

        PrintDebugMsg_ChargeGun("Charge already full.");
        return false;
    }
    /// <summary>
    /// Re-supplies this weapon to full.
    /// </summary>
    /// <returns>True if successful.</returns>
    public override bool ReSupplyFull()
    {
        return ReSupply(100);
    }

    /// <summary>
    /// Not currently used.
    /// </summary>
    public override void OnWeaponSelected()
    {
        PrintDebugMsg_ChargeGun(gameObject.name + " selected.");
    }
    /// <summary>
    /// Not currently used.
    /// </summary>
    public override void OnWeaponDeSelected()
    {
        PrintDebugMsg_ChargeGun(gameObject.name + " de-selected.");
    }
    #endregion

    #region Private
    /// <summary>
    /// Checks to see if charge is available and returns so that the Gun class can fire.
    /// </summary>
    /// <returns>True if charge is available.</returns>
    protected override bool CheckSupply()
    {
        if(currCharge >= chargeDepletionPerShot)
        {
            PrintDebugMsg_ChargeGun("Has charge! Charge at " + currCharge + ".");
            return true;
        }

        return false;
    }
    /// <summary>
    /// Adjusts the new charge after the Gun class has fired.
    /// </summary>
    protected override void PostShot()
    {
        currCharge -= chargeDepletionPerShot;
        PrintDebugMsg_ChargeGun("Shot! Charge now at " + currCharge + ".");
    }

    /// <summary>
    /// Passivly re-charges by the set amount and the set frquency.
    /// </summary>
    private void PassiveRecharge()
    {
        if (passiveRechargeAmount > 0 && currCharge < 100 && Time.time - lastCharge >= passiveRechargeFrequency)
        {
            currCharge += passiveRechargeAmount;
            if (currCharge > 100) currCharge = 100;
            PrintDebugMsg_ChargeGun("Recharged by " + passiveRechargeAmount + " to " + currCharge + ".");

            lastCharge = Time.time;
        }
    }

    /// <summary>
    /// Sets variables.
    /// </summary>
    private void Setup()
    {
        lastCharge = Time.time;
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_ChargeGun(string msg)
    {
        if (isDebug_ChargeGun) Debug.Log(isDebugScriptName_ChargeGun + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_ChargeGun(string msg)
    {
        Debug.LogWarning(isDebugScriptName_ChargeGun + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_ChargeGun(string msg)
    {
        Debug.LogError(isDebugScriptName_ChargeGun + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        base.Awake();
        PrintDebugMsg_ChargeGun("Debugging enabled.");

        Setup();
    }
    private void Update()
    {
        PassiveRecharge();
    }
    #endregion
}