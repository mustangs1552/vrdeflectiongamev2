﻿/* Created by: Matthew George
 */

using UnityEngine;
using ObjectUtility;

public enum WeaponAction
{
    Start,
    Continue,
    Stop,
}

/// <summary>
/// The gun clss handles a gun's ability to fire only. Checking ammunition supply is handled by the specific gun that inherits this class.
/// </summary>
public abstract class Gun : MonoBehaviour
{
    #region Global Variables
    [Header("Gun")]

    #region Default Variables
    [SerializeField] private bool isDebug_Gun = false;
    private string isDebugScriptName_Gun = "Gun";
    #endregion

    #region Public
    [SerializeField] private GameObject projectile = null;
    [SerializeField] private bool isFullAuto = false;
    [SerializeField] private float fireRate = 2;
    [SerializeField] private float damage = 5;
    #endregion

    #region Private
    private Transform owner = null;
    private Transform projectileSpawnpoint = null;
    private float lastShot = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Fires the weapon according to its weapon type (semi-auto, full-auto, etc.).
    /// </summary>
    /// <param name="action"></param>
    public void Fire(WeaponAction action)
    {
        PrintDebugMsg_Gun("Firing action: " + action.ToString());
        switch (action)
        {
            case WeaponAction.Start:
                Shoot();
                break;
            case WeaponAction.Continue:
                if (isFullAuto) Shoot();
                break;
        }
    }

    /// <summary>
    /// Re-supplying is handled in the derived classes. Re-supplies the gun by a set amount.
    /// </summary>
    /// <param name="amount">The amount to re-supply.</param>
    /// <returns>True if successful.</returns>
    public abstract bool ReSupply(float amount);
    /// <summary>
    /// Re-supplying is handled in the derived classes. Re-supplies the gun fully.
    /// </summary>
    /// <returns>True if successful.</returns>
    public abstract bool ReSupplyFull();

    /// <summary>
    /// What to do when the weapon is selected.
    /// </summary>
    public abstract void OnWeaponSelected();
    /// <summary>
    /// What to do when the weapon is put away.
    /// </summary>
    public abstract void OnWeaponDeSelected();
    #endregion

    #region Private
    /// <summary>
    /// To be overrided by the inheriting class to check if ammunition supply available before firing.
    /// </summary>
    /// <returns>True if supply is available.</returns>
    protected abstract bool CheckSupply();
    /// <summary>
    /// To be ovverided by the inheriting class so that it can adjust its supply, or whatever else it needs to do, after firing.
    /// </summary>
    protected abstract void PostShot();

    /// <summary>
    /// Fires one projectile if supply is available and can fire again.
    /// </summary>
    private void Shoot()
    {
        if (Time.time - lastShot >= fireRate && CheckSupply())
        {
            PrintDebugMsg_Gun("Firing a shot...");
            GameObject spawnedProjectile = ObjectManager.SINGLETON.CheckAddObject(projectile);
            if (spawnedProjectile == null) spawnedProjectile = Instantiate(projectile, projectileSpawnpoint.position, projectileSpawnpoint.rotation, projectileSpawnpoint);
            else
            {
                spawnedProjectile.transform.position = projectileSpawnpoint.position;
                spawnedProjectile.transform.rotation = projectileSpawnpoint.rotation;
            }
            spawnedProjectile.SetActive(true);

            Laser laser = spawnedProjectile.GetComponent<Laser>();
            if (laser != null) laser.Fired(owner, damage);

            lastShot = Time.time;
            PostShot();
        }
    }

    /// <summary>
    /// Sets up variables.
    /// </summary>
    private void Setup()
    {
        projectileSpawnpoint = ChildrenUtility.FindChildByName(this.transform, "ProjectileSpawnpoint");
        if (projectileSpawnpoint == null) PrintErrorDebugMsg_Gun("No projectile spawnpoint found!");

        owner = ParentUtility.GetHighestParent(transform);
        if (owner == null) PrintErrorDebugMsg_Gun("No parent found!");

        lastShot -= fireRate;
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Gun(string msg)
    {
        if (isDebug_Gun) Debug.Log(isDebugScriptName_Gun + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Gun(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Gun + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Gun(string msg)
    {
        Debug.LogError(isDebugScriptName_Gun + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties

    #endregion
    #endregion

    #region Start & Update Functions
    protected void Awake()
    {
        PrintDebugMsg_Gun("Debugging enabled.");
    }
    private void Start()
    {
        Setup();
    }
    #endregion
}