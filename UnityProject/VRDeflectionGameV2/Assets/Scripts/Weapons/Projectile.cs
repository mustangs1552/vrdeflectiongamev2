﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// Simply moves straight forwad.
/// </summary>
public class Projectile : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_Projectile = false;
    private string isDebugScriptName_Projectile = "Projectile";
    #endregion

    #region Public
    [SerializeField] private float speed = 1;
    [SerializeField] private float lifespan = 10;
    #endregion

    #region Private
    private float currSpeed = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Remove this projectile from the scene via ObjectManager.
    /// </summary>
    public void RemoveSelf()
    {
        PrintDebugMsg_Projectile("Removing self...");
        ObjectManager.SINGLETON.DestroyObject(gameObject);
        if (IsInvoking("RemoveSelf")) CancelInvoke("RemoveSelf");
    }

    /// <summary>
    /// Resets the timer that removes this projectile.
    /// </summary>
    public void ResetLifespan()
    {
        CancelInvoke("RemoveSelf");
        Invoke("RemoveSelf", lifespan);
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions
    /// <summary>
    /// Restart lifespan.
    /// </summary>
    private void OnEnable()
    {
        Invoke("RemoveSelf", lifespan);
    }
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Projectile(string msg)
    {
        if (isDebug_Projectile) Debug.Log(isDebugScriptName_Projectile + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Projectile(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Projectile + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Projectile(string msg)
    {
        Debug.LogError(isDebugScriptName_Projectile + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// The current speed this projectile is moving at.
    /// </summary>
    public float CurrSpeed
    {
        get
        {
            return currSpeed;
        }
        set
        {
            currSpeed = value;
        }
    }
    /// <summary>
    /// Reset the current speed to its default value.
    /// </summary>
    public void ResetCurrSpeed()
    {
        currSpeed = speed;
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_Projectile("Debugging enabled.");
    }
    private void Start()
    {
        ObjectManager.SINGLETON.AddObject(gameObject);
        Invoke("RemoveSelf", lifespan);

        currSpeed = speed;
    }
    protected virtual void Update()
    {
        transform.Translate(Vector3.forward * currSpeed * Time.deltaTime, Space.Self);
    }
    #endregion
}