﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class Explosion : TemporaryObj
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_Explosion = false;
    private string isDebugScriptName_Explosion = "Explosion";
    #endregion

    #region Public
    [SerializeField] private float radius = 5;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private
    /// <summary>
    /// Deal damage to all with health within range.
    /// </summary>
    private void Explode()
    {
        PrintDebugMsg_Explosion("Exploding...");

        RaycastHit[] hits;
        hits = Physics.SphereCastAll(new Ray(transform.position, Vector3.forward), radius, radius);

        foreach(RaycastHit hit in hits)
        {
            PrintDebugMsg_Explosion("Checking " + hit.transform.name + " to see if damagable...");

            HealthPiece health = hit.transform.GetComponent<HealthPiece>();
            if (health != null)
            {
                PrintDebugMsg_Explosion("Hit an object with health.");
                float damagedPassed = Mathf.Abs((Vector3.Distance(transform.position, hit.transform.position) / radius) - 1);
                health.TakeDamage(damagedPassed);
            }
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Shows the radius of the set explosion range.
    /// </summary>
    private void ShowRadius()
    {
        if(isDebug_Explosion)
        {
            Debug.DrawRay(transform.position, Vector3.forward, Color.red);
            Debug.DrawRay(transform.position, Vector3.right, Color.red);
            Debug.DrawRay(transform.position, Vector3.back, Color.red);
            Debug.DrawRay(transform.position, Vector3.left, Color.red);
            Debug.DrawRay(transform.position, Vector3.up, Color.red);
            Debug.DrawRay(transform.position, Vector3.down, Color.red);
        }
    }

    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Explosion(string msg)
    {
        if (isDebug_Explosion) Debug.Log(isDebugScriptName_Explosion + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Explosion(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Explosion + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Explosion(string msg)
    {
        Debug.LogError(isDebugScriptName_Explosion + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        base.Awake();
        PrintDebugMsg_Explosion("Debugging enabled.");

        Explode();
    }
    private void Update()
    {
        ShowRadius();
    }
    #endregion
}