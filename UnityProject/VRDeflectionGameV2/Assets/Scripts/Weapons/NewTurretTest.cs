﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class NewTurretTest : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_NewTurretTest = false;
    private string isDebugScriptName_NewTurretTest = "NewTurretTest";
    #endregion

    #region Public
    [SerializeField] private Transform target = null;

    [SerializeField] private Transform gun = null;
    [SerializeField] private Transform lookAtObj = null;
    [SerializeField] private float speed = 1;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private
    private void RotateGun()
    {
        Vector3 targetDir = target.position - gun.position;
        float step = speed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(gun.forward, targetDir, step, 0);
        Quaternion newRot = Quaternion.LookRotation(newDir);
        gun.localRotation = new Quaternion(newRot.x, gun.localRotation.y, gun.localRotation.z, newRot.w);
    }
    private void RotateBase()
    {
        Vector3 targetDir = target.position - transform.position;
        float step = speed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0);
        Quaternion newRot = Quaternion.LookRotation(newDir);
        transform.rotation = new Quaternion(transform.rotation.x, newRot.y, transform.rotation.z, newRot.w);
    }
    private void RotateAll()
    {
        lookAtObj.LookAt(target);
        transform.rotation = new Quaternion(transform.rotation.x, lookAtObj.rotation.y, transform.rotation.z, lookAtObj.rotation.w);
        gun.LookAt(target);
    }
    private void Aim()
    {
        RotateBase();
        RotateGun();
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_NewTurretTest(string msg)
    {
        if (isDebug_NewTurretTest) Debug.Log(isDebugScriptName_NewTurretTest + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_NewTurretTest(string msg)
    {
        Debug.LogWarning(isDebugScriptName_NewTurretTest + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_NewTurretTest(string msg)
    {
        Debug.LogError(isDebugScriptName_NewTurretTest + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_NewTurretTest("Debugging enabled.");
    }
    private void Update()
    {
        if (target != null) Aim();
    }
    #endregion
}