﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class Missile : Projectile
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_Missile = false;
    private string isDebugScriptName_Missile = "Missile";
    #endregion

    #region Public

    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private

    #endregion

    #region Unity Functions
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "PlasmaSword")
        {
            PrintDebugMsg_Missile("Was cut by plasma sword.");
            RemoveSelf();
        }
    }
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Missile(string msg)
    {
        if (isDebug_Missile) Debug.Log(isDebugScriptName_Missile + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Missile(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Missile + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Missile(string msg)
    {
        Debug.LogError(isDebugScriptName_Missile + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_Missile("Debugging enabled.");
    }
    #endregion
}