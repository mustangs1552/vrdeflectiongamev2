﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class LevitationDevice : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_LevitationDevice = false;
    private string isDebugScriptName_LevitationDevice = "LevitationDevice";
    #endregion

    #region Public

    #endregion

    #region Private
    private bool darknessMode = false;
    private float lastAlpha = -1;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Hides the particles via thier alpha.
    /// </summary>
    public void HideParticles()
    {
        ParticleSystem.MainModule mainPartSys = transform.GetChild(0).GetComponent<ParticleSystem>().main;
        mainPartSys.startColor = new Color(mainPartSys.startColor.color.r, mainPartSys.startColor.color.g, mainPartSys.startColor.color.b, 0);
        lastAlpha = mainPartSys.startColor.color.a;
    }
    /// <summary>
    /// Shows the particles via thier alpha.
    /// </summary>
    public void ShowParticles()
    {
        ParticleSystem.MainModule mainPartSys = transform.GetChild(0).GetComponent<ParticleSystem>().main;
        mainPartSys.startColor = new Color(mainPartSys.startColor.color.r, mainPartSys.startColor.color.g, mainPartSys.startColor.color.b, 1);
        lastAlpha = mainPartSys.startColor.color.a;
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_LevitationDevice(string msg)
    {
        if (isDebug_LevitationDevice) Debug.Log(isDebugScriptName_LevitationDevice + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_LevitationDevice(string msg)
    {
        Debug.LogWarning(isDebugScriptName_LevitationDevice + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_LevitationDevice(string msg)
    {
        Debug.LogError(isDebugScriptName_LevitationDevice + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    public bool DarknessMode
    {
        get
        {
            return darknessMode;
        }
        set
        {
            darknessMode = value;
            if (darknessMode) HideParticles();
            else ShowParticles();
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_LevitationDevice("Debugging enabled.");
    }
    private void Update()
    {
        if (TrainingGameModeManager.SINGLETON.DarknessMode)
        {
            if (lastAlpha != 0) HideParticles();
        }
        else if(lastAlpha != 1) ShowParticles();
    }
    #endregion
}