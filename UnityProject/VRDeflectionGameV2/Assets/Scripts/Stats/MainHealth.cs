﻿/* Created by: Matthew George
 */

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This is the main health script for a character/object. This doesn't track collisions. This is the script that holds the cahracter/object's health values and is manipulated by the health pieces linked to this script.The health pieces serve as parts of the character/object that can take damage (such as body and head).
/// </summary>
public class MainHealth : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_MainHealth = false;
    private string isDebugScriptName_MainHealth = "MainHealth";
    #endregion

    #region Public
    [Header("Main Stats")]
    [SerializeField]
    private float maxHealth = 100;
    [Tooltip("Heal by this amount every passiveHealRate. Deal damage when < 0 and do nothing when == 0.")]
    [SerializeField]
    private float passiveHealAmount = 1;
    [SerializeField] private float passiveHealRate = 1;
    [SerializeField] private float globalDmgMultiplier = 1;
    [Header("Tags")]
    [SerializeField]
    private bool allowFreindlyFire = false;
    [SerializeField] private string[] allyTags = null;
    [SerializeField] private string[] enemyTags = null;
    #endregion

    #region Private
    private float currMaxHealth = 0;
    private float currHealth = 0;
    private float currPassiveHealAmount = 0;
    private float currPassiveHealRate = 0;
    private float currGlobalDmgMultiplier = 0;
    private bool passiveHealRateChanged = false;
    private List<HealthPiece> healthPieces = new List<HealthPiece>();
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Adjust health by amount given.
    /// </summary>
    /// <param name="amount">Amount to be adjusted by. Deal damage when less than 0.</param>
    public void AdjustHealth(float amount)
    {
        if (amount != 0)
        {
            currHealth += (amount < 0) ? amount * globalDmgMultiplier : amount;
            CheckValueBoundries();
            PrintDebugMsg_MainHealth(((amount >= 0) ? "Healed" : "Damaged") + " for " + Mathf.Abs((amount < 0) ? amount * globalDmgMultiplier : amount) + ". Current health: " + currHealth);

            VRPlayer vrPlayer = gameObject.GetComponent<VRPlayer>();
            vrPlayer.HealthAdjusted(amount);
        }
    }

    /// <summary>
    /// Checks the given tag and returns a struct with two booleans saying if it did make contact with something contactable and wheter or not it was an ally or an enemy.
    /// </summary>
    /// <param name="tag">The tag to check.</param>
    /// <returns>Contact info about what it made contact with.</returns>
    public ContactInfo CheckTag(string tag)
    {
        ContactInfo info = new ContactInfo();
        info.validContact = false;

        foreach (string t in enemyTags)
        {
            if (t == tag)
            {
                PrintDebugMsg_MainHealth("Made contact with an enemy object with a(n) \"" + tag + "\" tag.");
                info.validContact = true;
                info.enemyContact = true;
                return info;
            }
        }

        if (allowFreindlyFire)
        {
            foreach (string t in allyTags)
            {
                if (t == tag)
                {
                    PrintDebugMsg_MainHealth("Made contact with an ally object with a(n) \"" + tag + "\" tag.");
                    info.validContact = true;
                    info.enemyContact = false;
                    return info;
                }
            }
        }

        return info;
    }

    /// <summary>
    /// Adds the given HealthPiece to the list of pieces using this MainHealth script.
    /// </summary>
    /// <param name="piece">The HealthPiece to add.</param>
    public void AddHealthPiece(HealthPiece piece)
    {
        if (healthPieces.Count > 0)
        {
            foreach (HealthPiece hp in healthPieces)
            {
                if (hp == piece)
                {
                    PrintWarningDebugMsg_MainHealth("HealthPiece already in list.");
                    return;
                }
            }
        }

        healthPieces.Add(piece);
    }

    /// <summary>
    /// Reset all the health values.
    /// </summary>
    public void ResetHealth()
    {
        currMaxHealth = maxHealth;
        currHealth = currMaxHealth;
        currPassiveHealAmount = passiveHealAmount;
        currPassiveHealRate = passiveHealRate;
        currGlobalDmgMultiplier = globalDmgMultiplier;
    }
    #endregion

    #region Private
    /// <summary>
    /// Set the values of the health variables that will actually be used.
    /// </summary>
    private void SetUp()
    {
        PrintDebugMsg_MainHealth("Setting up...");

        currMaxHealth = maxHealth;
        currHealth = currMaxHealth;
        currPassiveHealAmount = passiveHealAmount;
        currPassiveHealRate = passiveHealRate;
        currGlobalDmgMultiplier = globalDmgMultiplier;
    }

    /// <summary>
    /// Passivly heal over time. Can also take damage overtime when currPassiveHealAmount less than 0 and disable when equal to 0.
    /// </summary>
    private void PassiveHeal()
    {
        if ((currHealth < currMaxHealth || currPassiveHealAmount < 0) && currPassiveHealAmount != 0)
        {
            currHealth += currPassiveHealAmount;
            CheckValueBoundries();
            PrintDebugMsg_MainHealth(((currPassiveHealAmount >= 0) ? "Healed" : "Damaged") + " for " + Mathf.Abs(currPassiveHealAmount) + ". Current health: " + currHealth);
        }

        if (passiveHealRateChanged)
        {
            PrintDebugMsg_MainHealth("Detected passive heal rate change.");
            CancelInvoke("PassiveHeal");
            InvokeRepeating("PassiveHeal", currPassiveHealRate, currPassiveHealRate);
            passiveHealRateChanged = false;
        }
    }

    /// <summary>
    /// Check to see if the health values are more or less than their limits.
    /// </summary>
    private void CheckValueBoundries()
    {
        if (currPassiveHealRate < 0) currPassiveHealRate = 0;

        if (currHealth <= 0) Die();
        else if (currHealth > currMaxHealth) currHealth = currMaxHealth;
    }

    /// <summary>
    /// What to do when currHealth reaches 0.
    /// </summary>
    private void Die()
    {
        PrintDebugMsg_MainHealth("Died!");

        VRPlayer vrPlayer = gameObject.GetComponent<VRPlayer>();
        if(vrPlayer != null)
        {
            PrintDebugMsg_MainHealth("VR player died!");
            vrPlayer.Death();
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_MainHealth(string msg)
    {
        if (isDebug_MainHealth) Debug.Log(isDebugScriptName_MainHealth + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_MainHealth(string msg)
    {
        Debug.LogWarning(isDebugScriptName_MainHealth + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_MainHealth(string msg)
    {
        Debug.LogError(isDebugScriptName_MainHealth + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// The current health level.
    /// </summary>
    public float CurrHealth
    {
        get
        {
            return currHealth;
        }
    }
    /// <summary>
    /// The current maximum health level.
    /// </summary>
    public float CurrMaxHealth
    {
        get
        {
            return currMaxHealth;
        }
        set
        {
            currMaxHealth = value;
            CheckValueBoundries();
        }
    }
    /// <summary>
    /// The current passive heal amount.
    /// </summary>
    public float CurrPassiveHealAmount
    {
        get
        {
            return currPassiveHealAmount;
        }
        set
        {
            currPassiveHealAmount = value;
            CheckValueBoundries();
        }
    }
    /// <summary>
    /// The current passive heal rate.
    /// </summary>
    public float CurrPassiveHealRate
    {
        get
        {
            return currPassiveHealRate;
        }
        set
        {
            currPassiveHealRate = value;
            passiveHealRateChanged = true;
        }
    }
    /// <summary>
    /// The multiplier that will be applied to all damage received.
    /// </summary>
    public float CurrGlobalDmgMultiplier
    {
        get
        {
            return currGlobalDmgMultiplier;
        }
        set
        {
            currGlobalDmgMultiplier = value;
        }
    }
    /// <summary>
    /// The HealthPieces currently linked to this MainHealth.
    /// </summary>
    public List<HealthPiece> HealthPieces
    {
        get
        {
            return healthPieces;
        }
    }

    /// <summary>
    /// Reset the current max health.
    /// </summary>
    public void ResetCurrMaxHealth()
    {
        currMaxHealth = maxHealth;
    }
    /// <summary>
    /// Reset the current passive heal amount.
    /// </summary>
    public void ResetCurrPassiveHealAmount()
    {
        currPassiveHealAmount = passiveHealAmount;
    }
    /// <summary>
    /// Reset the current passive heal rate.
    /// </summary>
    public void ResetCurrPassiveHealRate()
    {
        currPassiveHealRate = passiveHealRate;
    }
    /// <summary>
    /// Reset the global damage multiplier.
    /// </summary>
    public void ResetGlobalDmgMultiplier()
    {
        currGlobalDmgMultiplier = globalDmgMultiplier;
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_MainHealth("Debugging enabled.");

        SetUp();

        InvokeRepeating("PassiveHeal", currPassiveHealRate, currPassiveHealRate);
    }
    #endregion
}

/// <summary>
/// Info about what was made contact with.
/// </summary>
public struct ContactInfo
{
    public bool validContact;
    public bool enemyContact;
}