﻿/* Created by: Matthew George
 */

using UnityEngine;
using ObjectUtility;

/// <summary>
/// This script communicates with the main health script where the health values are actually stored. This script is a piece on a character/object that can take damage (such as the body or head).
/// </summary>
public class HealthPiece : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_HealthPiece = false;
    private string isDebugScriptName_HealthPiece = "HealthPiece";
    #endregion

    #region Public
    [SerializeField] private MainHealth mainHealth = null;
    [SerializeField] private float damageMultiplier = 1;
    [Header("Internally Handled Damage")]
    [SerializeField]
    private bool allowInternalDmg = false;
    [SerializeField] private float internalDamage = 5;
    [Tooltip("Use collision or trigger?")]
    [SerializeField]
    private bool useCollision = false;
    #endregion

    #region Private
    private Transform highestParent = null;
    private float currDamageMultiplier = 0;
    private float currInternalDamage = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Apply damage ignoring ally/enemy tags.
    /// </summary>
    /// <param name="damage">Damage to be applied.</param>
    public void TakeDamage(float damage)
    {
        mainHealth.AdjustHealth(-damage * currDamageMultiplier);
        PrintDebugMsg_HealthPiece("Taking " + (-damage * currDamageMultiplier) + " of damage...");
    }
    /// <summary>
    /// Apply damage only if given enemy/ally tag returns a valid contact.
    /// </summary>
    /// <param name="damage">Damage to be applied.</param>
    /// <param name="tag">The tag of the enemy/ally to check.</param>
    /// <returns>Returns true if made a valid contact.</returns>
    public bool TakeDamage(float damage, string tag)
    {
        ContactInfo info = mainHealth.CheckTag(tag);
        if (info.validContact)
        {
            TakeDamage(damage);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Returns a ContactInfo showing if the given tag would be a valid contact for damage.
    /// </summary>
    /// <param name="tag">The tag of the enemy/ally to check.</param>
    public ContactInfo CheckTag(string tag)
    {
        return mainHealth.CheckTag(tag);
    }
    #endregion

    #region Private
    /// <summary>
    /// Set the values of the health variables that will actually be used.
    /// </summary>
    private void SetUp()
    {
        PrintDebugMsg_HealthPiece("Setting up...");

        currDamageMultiplier = damageMultiplier;
        currInternalDamage = internalDamage;

        highestParent = ParentUtility.GetHighestParent(transform);
        mainHealth.AddHealthPiece(this);
    }

    /// <summary>
    /// Apply damage to main health. Called by collision or trigger events on this script and doesn't receive damage from outside scripts.
    /// </summary>
    private void TakeDamageInternal()
    {
        mainHealth.AdjustHealth(-currInternalDamage * currDamageMultiplier);
        PrintDebugMsg_HealthPiece("Taking " + (-currInternalDamage * currDamageMultiplier) + " internal damage...");
    }
    #endregion

    #region Unity Functions
    private void OnCollisionEnter(Collision collision)
    {
        if (useCollision && allowInternalDmg)
        {
            if (highestParent != ParentUtility.GetHighestParent(collision.transform))
            {
                ContactInfo info = mainHealth.CheckTag(collision.gameObject.tag);
                if (info.validContact) TakeDamageInternal();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!useCollision && allowInternalDmg)
        {
            if (highestParent != ParentUtility.GetHighestParent(other.transform))
            {
                ContactInfo info = mainHealth.CheckTag(other.gameObject.tag);
                if (info.validContact) TakeDamageInternal();
            }
        }
    }
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_HealthPiece(string msg)
    {
        if (isDebug_HealthPiece) Debug.Log(isDebugScriptName_HealthPiece + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_HealthPiece(string msg)
    {
        Debug.LogWarning(isDebugScriptName_HealthPiece + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_HealthPiece(string msg)
    {
        Debug.LogError(isDebugScriptName_HealthPiece + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// Current multiplier that will be applied to all damage received.
    /// </summary>
    public float CurrDamageMultiplier
    {
        set
        {
            currDamageMultiplier = value;
        }
        get
        {
            return currDamageMultiplier;
        }
    }
    /// <summary>
    /// Base damage that will be applied when internal damage handling is enabled.
    /// </summary>
    public float CurrInternalDamage
    {
        set
        {
            currInternalDamage = value;
        }
        get
        {
            return currInternalDamage;
        }
    }

    /// <summary>
    /// Resets the damage multiplier.
    /// </summary>
    public void ResetCurrDamageMultiplier()
    {
        currDamageMultiplier = damageMultiplier;
    }
    /// <summary>
    /// Resets the current internal damage.
    /// </summary>
    public void ResetCurrInternalDamage()
    {
        currInternalDamage = internalDamage;
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_HealthPiece("Debugging enabled.");

        SetUp();
    }
    #endregion
}