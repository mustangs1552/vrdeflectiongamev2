﻿/* Created by: Matthew George
 */

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class Keypad : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_Keypad = false;
    private string isDebugScriptName_Keypad = "Keypad";
    #endregion

    #region Public
    public static Keypad SINGLETON = null;

    [SerializeField] private Text text = null;
    [SerializeField] private Button decimalButton = null;
    #endregion

    #region Private
    private GameObject callbackObj = null;
    private bool intOnly = false;
    private bool allowMultipleDecimals = false;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Shows and prepares the keypad for use.
    /// </summary>
    /// <param name="callback">Callback object to be informed when input is done.</param>
    /// <param name="onlyInt">Only integer input.</param>
    /// <param name="multipleDecimalsAllowed">Allow more than one '.'.</param>
    public void Activate(GameObject callback, bool onlyInt, bool multipleDecimalsAllowed)
    {
        callbackObj = callback;
        intOnly = onlyInt;
        if (intOnly) decimalButton.gameObject.SetActive(false);
        else decimalButton.gameObject.SetActive(true);
        allowMultipleDecimals = multipleDecimalsAllowed;
        decimalButton.interactable = true;
        gameObject.SetActive(true);
        text.text = "";
    }

    /// <summary>
    /// Done key was pressed.
    /// </summary>
    public void Done()
    {
        if (text.text.Length > 0 && text.text[text.text.Length - 1] == '.') DeleteKey();

        callbackObj.SendMessage("KeypadInputDone", text.text);
        gameObject.SetActive(false);
    }
    /// <summary>
    /// Cancels input via the keypad.
    /// </summary>
    public void Cancel()
    {
        text.text = "";
        Done();
    }
    /// <summary>
    /// A number key was pressed.
    /// </summary>
    /// <param name="num">The number pressed.</param>
    public void NumberKey(string num)
    {
        if(num == ".")
        {
            text.text += num;
            if(!allowMultipleDecimals) decimalButton.interactable = false;
        }
        else text.text += num;
    }
    /// <summary>
    /// The delete key was pressed.
    /// </summary>
    public void DeleteKey()
    {
        if (text.text.Length > 0)
        {
            string newStr = "";
            for (int i = 0; i < text.text.Length - 1; i++) newStr += text.text[i];

            if (text.text[text.text.Length - 1] == '.') decimalButton.interactable = true;
            text.text = newStr;
        }
    }
    #endregion

    #region Private

    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Keypad(string msg)
    {
        if (isDebug_Keypad) Debug.Log(isDebugScriptName_Keypad + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Keypad(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Keypad + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Keypad(string msg)
    {
        Debug.LogError(isDebugScriptName_Keypad + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_Keypad("Debugging enabled.");

        if (Keypad.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_Keypad("More than one Keypad singletons found!");

        gameObject.SetActive(false);
    }
    #endregion
}