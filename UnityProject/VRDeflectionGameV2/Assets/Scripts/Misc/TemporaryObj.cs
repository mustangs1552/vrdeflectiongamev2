﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class TemporaryObj : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_TemporaryObj = false;
    private string isDebugScriptName_TemporaryObj = "TemporaryObj";
    #endregion

    #region Public
    [SerializeField] private float lifespan = 5;
    #endregion

    #region Private

    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private
    /// <summary>
    /// Removes this object via the object manager.
    /// </summary>
    private void RemoveSelf()
    {
        ObjectManager.SINGLETON.DestroyObject(gameObject);
    }
    #endregion

    #region Unity Functions
    /// <summary>
    /// Restarts the lifespan when re-enabled.
    /// </summary>
    private void OnEnable()
    {
        if (IsInvoking("RemoveSelf")) CancelInvoke("RemoveSelf");
        Invoke("RemoveSelf", lifespan);
    }
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_TemporaryObj(string msg)
    {
        if (isDebug_TemporaryObj) Debug.Log(isDebugScriptName_TemporaryObj + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_TemporaryObj(string msg)
    {
        Debug.LogWarning(isDebugScriptName_TemporaryObj + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_TemporaryObj(string msg)
    {
        Debug.LogError(isDebugScriptName_TemporaryObj + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    protected void Awake()
    {
        PrintDebugMsg_TemporaryObj("Debugging enabled.");

        ObjectManager.SINGLETON.AddObject(gameObject);
        Invoke("RemoveSelf", lifespan);
    }
    #endregion
}