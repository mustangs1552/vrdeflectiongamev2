﻿/* Created by: Matthew George
 */

using UnityEngine;

[RequireComponent(typeof(Light))]

/// <summary>
/// 
/// </summary>
public class LightFlicker : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_LightFlicker = false;
    private string isDebugScriptName_LightFlicker = "LightFlicker";
    #endregion

    #region Public
    [SerializeField] private Vector2 minMaxIntensity = new Vector2(0, 1);
    [SerializeField] private Vector2 minMaxUpdateRate = new Vector2(0, 1);
    #endregion

    #region Private
    private float randNum = 0;
    private Light lightComp = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    
    #endregion

    #region Private
    /// <summary>
    /// Pick a random intensity and re-invoke this function again with a random delay.
    /// </summary>
    private void Flicker()
    {
        randNum = Random.Range(minMaxIntensity.x, minMaxIntensity.y);
        lightComp.intensity = randNum;

        randNum = Random.Range(minMaxUpdateRate.x, minMaxUpdateRate.y);
        Invoke("Flicker", randNum);
    }

    /// <summary>
    /// Set variables and start invoking the filcker effect.
    /// </summary>
    private void SetUp()
    {
        lightComp = GetComponent<Light>();

        randNum = Random.Range(minMaxUpdateRate.x, minMaxUpdateRate.y);
        Invoke("Flicker", randNum);
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_LightFlicker(string msg)
    {
        if (isDebug_LightFlicker) Debug.Log(isDebugScriptName_LightFlicker + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_LightFlicker(string msg)
    {
        Debug.LogWarning(isDebugScriptName_LightFlicker + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_LightFlicker(string msg)
    {
        Debug.LogError(isDebugScriptName_LightFlicker + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties

    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_LightFlicker("Debugging enabled.");

        SetUp();
    }
    #endregion
}