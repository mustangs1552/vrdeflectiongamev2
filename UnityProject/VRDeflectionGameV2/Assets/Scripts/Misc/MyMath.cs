﻿/// <summary>
/// Contains math related functions and classes.
/// </summary>
namespace Assets.Scripts.Misc.MyMath
{
    /// <summary>
    /// Contains math related functions.
    /// </summary>
    public static class MyMath
    {
        /// <summary>
        /// Gets the answer of an exponent.
        /// </summary>
        /// <param name="baseNum">The base number.</param>
        /// <param name="power">The exponent.</param>
        /// <returns>The result.</returns>
        public static float Exponent(float baseNum, float power)
        {
            float result = 1;
            for (int i = 0; i < power; i++) result *= baseNum;
            return result;
        }
    }
}
