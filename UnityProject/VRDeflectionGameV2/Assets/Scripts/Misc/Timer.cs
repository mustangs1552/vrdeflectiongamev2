﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class Timer : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_Timer = false;
    private string isDebugScriptName_Timer = "Timer";
    #endregion

    #region Public
    public static Timer SINGLETON = null;

    [SerializeField] private bool debugShowTimeDotTime = false;
    #endregion

    #region Private
    private float startTime = 0;
    private float timeLength = 0;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Start a countdown from the given time.
    /// </summary>
    /// <param name="time">The time to countdown from.</param>
    public void StartCountdown(float time)
    {
        PrintDebugMsg_Timer("Starting countdown at " + time + "...");
        timeLength = time;
        startTime = Time.time;
        Invoke("TimeUp", timeLength);
    }
    /// <summary>
    /// Start an endless timer counting up.
    /// </summary>
    public void StartTimer()
    {
        PrintDebugMsg_Timer("Starting timer...");
        startTime = Time.time;
    }

    /// <summary>
    /// Cancel the current timer/countdown and reset.
    /// </summary>
    public void CancelTimer()
    {
        PrintDebugMsg_Timer("Canceling timer...");
        CancelInvoke("TimeUp");
        startTime = 0;
        timeLength = 0;
    }
    #endregion

    #region Private
    /// <summary>
    /// Called when the coundown reaches 0.
    /// </summary>
    private void TimeUp()
    {
        PrintDebugMsg_Timer("Time up!");
        startTime = 0;
        timeLength = 0;

        if(TrainingGameModeManager.SINGLETON != null) TrainingGameModeManager.SINGLETON.TimeUp();
        if (AssassinGameModeManager.SINGLETON != null) AssassinGameModeManager.SINGLETON.TimeUp();
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_Timer(string msg)
    {
        if (isDebug_Timer) Debug.Log(isDebugScriptName_Timer + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_Timer(string msg)
    {
        Debug.LogWarning(isDebugScriptName_Timer + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_Timer(string msg)
    {
        Debug.LogError(isDebugScriptName_Timer + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters Setters Properties
    /// <summary>
    /// The current time that the timer/countdown is at in seconds.
    /// </summary>
    public float CurrTimeSec
    {
        get
        {
            if (timeLength > 0) return timeLength - (Time.time - startTime);
            else return Time.time - startTime;
        }
    }
    /// <summary>
    /// The current time that the timer/countdown is at in mins and seconds.
    /// </summary>
    public MinSec CurrTimeMinSec
    {
        get
        {
            float timeRemaining = 0;
            if (timeLength > 0) timeRemaining = timeLength - (Time.time - startTime);
            else timeRemaining = Time.time - startTime;

            int secs = (int)timeRemaining;
            int mins = secs / 60;
            secs = secs % 60;
            PrintDebugMsg_Timer("Current time: " + mins + ":" + secs + ((debugShowTimeDotTime) ? " (" + timeRemaining + ")" : ""));
            return new MinSec(mins, secs);
        }
    }

    /// <summary>
    /// The time that the countdown is counting down from. 0 if not active.
    /// </summary>
    public float TimeLength
    {
        get
        {
            return timeLength;
        }
    }
    /// <summary>
    /// The time that the timer/countdown started. 0 if not active.
    /// </summary>
    public float StartTime
    {
        get
        {
            return startTime;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_Timer("Debugging enabled.");

        if (Timer.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg_Timer("More than one Timer singletons found!");
    }
    #endregion
}

/// <summary>
/// Minutes and seconds.
/// </summary>
public struct MinSec
{
    public int mins;
    public int secs;

    public MinSec(int m, int s)
    {
        mins = m;
        secs = s;
    }
}