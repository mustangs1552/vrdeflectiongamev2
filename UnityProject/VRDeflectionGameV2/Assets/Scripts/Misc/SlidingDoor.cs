﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public class SlidingDoor : MonoBehaviour
{
    #region Global Variables
    #region Default Variables
    [SerializeField] private bool isDebug_SlidingDoor = false;
    private string isDebugScriptName_SlidingDoor = "SlidingDoor";
    #endregion

    #region Public
    [Header("Movement")]
    [SerializeField] private Transform leftDoor = null;
    [SerializeField] private Transform rightDoor = null;
    [SerializeField] private float openSpeed = 1;
    [Tooltip("Door pushes in/out before sliding open/closed.")]
    [SerializeField] private bool useDepth = true;
    [Header("Effects")]
    [SerializeField] private bool loopDoorMovingSound = true;
    [SerializeField] private AudioClip doorClosedSound = null;
    [SerializeField] private AudioClip doorMovingSound = null;
    [SerializeField] private AudioClip doorOpenedSound = null;
    #endregion

    #region Private
    private float leftClosedPos, rightClosedPos = 0;
    private float leftOpenPos, rightOpenPos = 0;
    private float depthOpenPos = 0;
    private float startClosedPos = 0;

    private enum DoorState
    {
        Open,
        Closing,
        Closed,
        Opening
    }
    private DoorState state = DoorState.Closed;

    private AudioSource audioSource = null;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Starts to open the door.
    /// </summary>
    public void OpenDoor()
    {
        if (state == DoorState.Closed || state == DoorState.Closing)
        {
            state = DoorState.Opening;
            if (audioSource != null && doorMovingSound != null)
            {
                audioSource.clip = doorMovingSound;
                audioSource.loop = loopDoorMovingSound;
                audioSource.Play();
            }
        }
    }
    /// <summary>
    /// Starts to close the door.
    /// </summary>
    public void CloseDoor()
    {
        if (state == DoorState.Open || state == DoorState.Opening)
        {
            state = DoorState.Closing;
            if (audioSource != null && doorMovingSound != null)
            {
                audioSource.clip = doorMovingSound;
                audioSource.loop = loopDoorMovingSound;
                audioSource.Play();
            }
        }
    }

    /// <summary>
    /// Sets the doors' position to their open position.
    /// </summary>
    public void ToOpenPosition()
    {
        state = DoorState.Open;

        if (leftDoor != null) leftDoor.localPosition = new Vector3(leftOpenPos, leftDoor.localPosition.y, ((useDepth) ? depthOpenPos : leftDoor.localPosition.z));
        if (rightDoor != null) rightDoor.localPosition = new Vector3(rightOpenPos, rightDoor.localPosition.y, ((useDepth) ? depthOpenPos : rightDoor.localPosition.z));
    }
    /// <summary>
    /// Sets the doors' position to their closed position.
    /// </summary>
    public void ToClosedPosition()
    {
        state = DoorState.Closed;

        if (leftDoor != null) leftDoor.localPosition = new Vector3(leftClosedPos, leftDoor.localPosition.y, startClosedPos);
        if (rightDoor != null) rightDoor.localPosition = new Vector3(rightClosedPos, rightDoor.localPosition.y, startClosedPos);
    }
    #endregion

    #region Private
    /// <summary>
    /// Sets up variables.
    /// </summary>
    private void SetUp()
    {
        if (leftDoor != null) leftClosedPos = leftDoor.localPosition.x;
        if(rightDoor != null) rightClosedPos = rightDoor.localPosition.x;

        if (leftDoor != null) leftOpenPos = leftClosedPos - leftDoor.localScale.x;
        if (rightDoor != null) rightOpenPos = rightClosedPos + rightDoor.localScale.x;

        if (leftDoor != null)
        {
            startClosedPos = leftDoor.localPosition.z;
            depthOpenPos = leftDoor.localPosition.z - leftDoor.localScale.z;
        }
        else if (rightDoor != null)
        {
            startClosedPos = rightDoor.localPosition.z;
            depthOpenPos = rightDoor.localPosition.z - rightDoor.localScale.z;
        }
        else PrintErrorDebugMsg_SlidingDoor("No doors found!");

        audioSource = GetComponent<AudioSource>();
    }

    /// <summary>
    /// Opens the door by pushing it in (if depth is enabled) and opening it horizontally.
    /// </summary>
    private void Opening()
    {
        PrintDebugMsg_SlidingDoor("Opening...");

        // Depth
        if (useDepth && ((leftDoor != null && leftDoor.localPosition.z > depthOpenPos) || (rightDoor != null && rightDoor.localPosition.z > depthOpenPos)))
        {
            PrintDebugMsg_SlidingDoor("    ...depth.");

            if (leftDoor != null) leftDoor.Translate(Vector3.back * openSpeed * Time.deltaTime);
            if (rightDoor != null) rightDoor.Translate(Vector3.back * openSpeed * Time.deltaTime);

            if ((leftDoor != null && leftDoor.localPosition.z <= depthOpenPos) || (rightDoor != null && rightDoor.localPosition.z <= depthOpenPos))
            {
                if (leftDoor != null) leftDoor.localPosition = new Vector3(leftDoor.localPosition.x, leftDoor.localPosition.y, depthOpenPos);
                if (rightDoor != null) rightDoor.localPosition = new Vector3(rightDoor.localPosition.x, rightDoor.localPosition.y, depthOpenPos);
            }
        }
        // Horizontal
        else if ((leftDoor != null && leftDoor.localPosition.x > leftOpenPos) || (rightDoor != null && rightDoor.localPosition.x < rightOpenPos))
        {
            PrintDebugMsg_SlidingDoor("    ...horizontal.");

            if (leftDoor != null) leftDoor.Translate(Vector3.left * openSpeed * Time.deltaTime);
            if (rightDoor != null) rightDoor.Translate(Vector3.right * openSpeed * Time.deltaTime);

            if ((leftDoor != null && leftDoor.localPosition.x <= leftOpenPos) || (rightDoor != null && rightDoor.localPosition.x >= rightOpenPos))
            {
                if (leftDoor != null) leftDoor.localPosition = new Vector3(leftOpenPos, leftDoor.localPosition.y, leftDoor.localPosition.z);
                if (rightDoor != null) rightDoor.localPosition = new Vector3(rightOpenPos, rightDoor.localPosition.y, rightDoor.localPosition.z);
            }
        }
        else
        {
            PrintDebugMsg_SlidingDoor("Open.");

            state = DoorState.Open;
            if (audioSource != null)
            {
                if(loopDoorMovingSound) audioSource.Stop();
                if (doorOpenedSound != null)
                {
                    audioSource.clip = doorOpenedSound;
                    audioSource.loop = false;
                    audioSource.Play();
                }
            }
        }
    }
    /// <summary>
    /// Closes the door by closing horizontally and then pulling it out (if depth is enabled).
    /// </summary>
    private void Closing()
    {
        PrintDebugMsg_SlidingDoor("Closing...");

        // Horizontal
        if ((leftDoor != null && leftDoor.localPosition.x < leftClosedPos) || (rightDoor != null && rightDoor.localPosition.x > rightClosedPos))
        {
            PrintDebugMsg_SlidingDoor("    ...horizontal.");

            if (leftDoor != null) leftDoor.Translate(Vector3.right * openSpeed * Time.deltaTime);
            if (rightDoor != null) rightDoor.Translate(Vector3.left * openSpeed * Time.deltaTime);

            if ((leftDoor != null && leftDoor.localPosition.x >= leftClosedPos) || (rightDoor != null && rightDoor.localPosition.x <= rightClosedPos))
            {
                if (leftDoor != null) leftDoor.localPosition = new Vector3(leftClosedPos, leftDoor.localPosition.y, leftDoor.localPosition.z);
                if (rightDoor != null) rightDoor.localPosition = new Vector3(rightClosedPos, rightDoor.localPosition.y, rightDoor.localPosition.z);
            }
        }
        // Depth
        else if (useDepth && ((leftDoor != null && leftDoor.localPosition.z < startClosedPos) || (rightDoor != null && rightDoor.localPosition.z < startClosedPos)))
        {
            PrintDebugMsg_SlidingDoor("    ...depth.");

            if (leftDoor != null) leftDoor.Translate(Vector3.forward * openSpeed * Time.deltaTime);
            if (rightDoor != null) rightDoor.Translate(Vector3.forward * openSpeed * Time.deltaTime);

            if ((leftDoor != null && leftDoor.localPosition.z >= startClosedPos) || (rightDoor != null && rightDoor.localPosition.z >= startClosedPos))
            {
                if (leftDoor != null) leftDoor.localPosition = new Vector3(leftDoor.localPosition.x, leftDoor.localPosition.y, startClosedPos);
                if (rightDoor != null) rightDoor.localPosition = new Vector3(rightDoor.localPosition.x, rightDoor.localPosition.y, startClosedPos);
            }
        }
        else
        {
            PrintDebugMsg_SlidingDoor("Closed.");

            state = DoorState.Closed;
            if (audioSource != null)
            {
                if (loopDoorMovingSound) audioSource.Stop();
                if (doorClosedSound != null)
                {
                    audioSource.clip = doorClosedSound;
                    audioSource.loop = false;
                    audioSource.Play();
                }
            }
        }
    }
    #endregion

    #region Unity Functions

    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_SlidingDoor(string msg)
    {
        if (isDebug_SlidingDoor) Debug.Log(isDebugScriptName_SlidingDoor + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_SlidingDoor(string msg)
    {
        Debug.LogWarning(isDebugScriptName_SlidingDoor + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_SlidingDoor(string msg)
    {
        Debug.LogError(isDebugScriptName_SlidingDoor + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    /// <summary>
    /// Is the door completly open?
    /// </summary>
    public bool IsOpen
    {
        get
        {
            if (state == DoorState.Open) return true;
            else return false;
        }
    }
    /// <summary>
    /// Is the door completly closed?
    /// </summary>
    public bool IsClosed
    {
        get
        {
            if (state == DoorState.Closed) return true;
            else return false;
        }
    }
    /// <summary>
    /// Is the door opening?
    /// </summary>
    public bool IsOpening
    {
        get
        {
            if (state == DoorState.Opening) return true;
            else return false;
        }
    }
    /// <summary>
    /// Is the door closing?
    /// </summary>
    public bool IsClosing
    {
        get
        {
            if (state == DoorState.Closing) return true;
            else return false;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_SlidingDoor("Debugging enabled.");

        SetUp();
    }
    private void Update()
    {
        if (leftDoor != null || rightDoor != null)
        {
            if (state == DoorState.Opening) Opening();
            else if (state == DoorState.Closing) Closing();

            if (isDebug_SlidingDoor)
            {
                if (state == DoorState.Closed) OpenDoor();
                else if (state == DoorState.Open) CloseDoor();
            }
        }
    }
    #endregion
}