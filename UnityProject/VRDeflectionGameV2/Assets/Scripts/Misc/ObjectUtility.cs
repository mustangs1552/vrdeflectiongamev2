﻿using UnityEngine;

/// <summary>
/// Functions for actions on objects.
/// </summary>
namespace ObjectUtility
{
    /// <summary>
    /// Functions for actions on the children of an object.
    /// </summary>
    public static class ChildrenUtility
    {
        /// <summary>
        /// Iterates through all of an object's children (including beyond the first level of children) and encapsulates thier bounds.
        /// </summary>
        /// <param name="obj">The object to be iterated through.</param>
        /// <returns>The bounds of the children.</returns>
        public static Bounds GetAllChildrenBounds(Transform obj)
        {
            Bounds objBounds = new Bounds();

            for (int i = 0; i < obj.childCount; i++)
            {
                Transform currChild = obj.GetChild(i);
                if (currChild.GetComponent<Renderer>()) objBounds.Encapsulate(currChild.GetComponent<Renderer>().bounds);
                if (currChild.childCount > 0) objBounds.Encapsulate(GetAllChildrenBounds(currChild));
            }

            return objBounds;
        }

        /// <summary>
        /// Iterates through all of an object's children (including beyond the first level of children) and adds a Mesh Collider to any that have renderers.
        /// </summary>
        /// <param name="obj">The object to be iterated through.</param>
        public static void AddChildrenMeshColliders(Transform obj)
        {
            for (int i = 0; i < obj.childCount; i++)
            {
                Transform currChild = obj.GetChild(i);
                if (currChild.GetComponent<Renderer>() != null) currChild.gameObject.AddComponent<MeshCollider>();
                if (currChild.childCount > 0) AddChildrenMeshColliders(currChild);
            }
        }

        /// <summary>
        /// Searches through all the children at the first level of the given parent and returns the first child that has the same name given.
        /// </summary>
        /// <param name="parent">The parent to search under.</param>
        /// <param name="name">The name of the child to search for.</param>
        /// <returns>The child found.</returns>
        public static Transform FindChildByName(Transform parent, string name)
        {
            for(int i = 0; i < parent.childCount; i++)
            {
                Transform currChild = parent.GetChild(i);
                if (currChild.name == name) return currChild;
            }

            return null;
        }
    }

    /// <summary>
    /// Functions for actions on the parents of a hierarchy.
    /// </summary>
    public static class ParentUtility
    {
        /// <summary>
        /// Returns the Transform of the highest parent of the given child.
        /// </summary>
        /// <param name="child">The child to find the highest parent of.</param>
        /// <returns>The highest parent.</returns>
        public static Transform GetHighestParent(Transform child)
        {
            Transform currParent = child;
            Transform highestParent = null;
            while (highestParent == null)
            {
                if (currParent.parent == null) highestParent = currParent;
                else currParent = currParent.parent;
            }

            return highestParent;
        }
    }
}
