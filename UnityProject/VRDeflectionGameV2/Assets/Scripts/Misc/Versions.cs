﻿/* Created by: Matthew George
 */
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class contains the game's version.
/// </summary>
public class Versions : MonoBehaviour
{
    #region Global Vareables
    #region Default Vareables
    [SerializeField] private bool isDebug = false;
    private string debugScriptName = "Versions";
    #endregion

    #region Public
    public static Versions SINGLETON = null;

    [SerializeField] private Text[] versionTexts = null;
    #endregion

    #region Private
    private int releaseVer = 1;
    private int buildVer = 0;
    private int mileNum = 0;
    private int currIteration = 0;

    private string ver = "";
    #endregion
    #endregion

    #region Functions
    #region Public

    #endregion

    #region Private

    #endregion

    #region Unity Functions
    public void OnGUI()
    {
        if (isDebug) GUI.Label(new Rect(0f, 0f, 100f, 50f), "Version: " + ver);
    }
    #endregion

    #region Debug
    /// <summary>
    /// Use Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg(string msg)
    {
        if (isDebug) Debug.Log(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg(string msg)
    {
        Debug.LogWarning(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Use Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg(string msg)
    {
        Debug.LogError(debugScriptName + "(" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    public string Version
    {
        get
        {
            return ver;
        }
    }
    public int[] VersionPieces
    {
        get
        {
            return new int[] { releaseVer, buildVer, mileNum, currIteration };
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        if (Versions.SINGLETON == null) SINGLETON = this;
        else PrintErrorDebugMsg("A \"Versions\" singleton already exists!");

        PrintDebugMsg("Debugging enabled.");
        ver += releaseVer + "." + buildVer + "." + mileNum + "." + currIteration;
        PrintDebugMsg("Game version: " + ver);

        foreach(Text versionText in versionTexts) versionText.text = "Version: " + ver;
    }
    #endregion
}