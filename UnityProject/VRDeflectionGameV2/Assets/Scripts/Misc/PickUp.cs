﻿/* Created by: Matthew George
 */

using UnityEngine;

/// <summary>
/// 
/// </summary>
public abstract class PickUp : MonoBehaviour
{
    #region Global Variables
    [Header("Pickup")]
    #region Default Variables
    [SerializeField] private bool isDebug_PickUp = false;
    private string isDebugScriptName_PickUp = "PickUp";
    #endregion

    #region Public
    [Tooltip("Set to < 0 to not respawn.")]
    [SerializeField] private float respawnRate = 5;
    #endregion

    #region Private
    private Vector3 startPos = Vector3.zero;
    private Quaternion startRot = Quaternion.identity;
    #endregion
    #endregion

    #region Functions
    #region Public
    /// <summary>
    /// Called when picked-up.
    /// </summary>
    /// <param name="player">The player that attempted to pick this up.</param>
    public abstract void OnPickUp(GameObject player);

    /// <summary>
    /// Removes this object from the game via the object manager and tells the pickup spawner that it needs to be respawned if respanw rate is >= 0.
    /// </summary>
    public void RemoveSelf()
    {
        PrintDebugMsg_PickUp("Removing self...");
        ObjectManager.SINGLETON.DestroyObject(gameObject);
        if(respawnRate >= 0) PickupSpawner.SINGLETON.StartCoroutine("RespawnPickup", this);
    }
    /// <summary>
    /// Sets this object's position to where it started.
    /// </summary>
    public void Respawn()
    {
        transform.position = startPos;
        transform.rotation = startRot;
    }
    #endregion

    #region Private
    /// <summary>
    /// Sets variables and adds this object to the list on the object manager.
    /// </summary>
    private void Setup()
    {
        startPos = transform.position;
        startRot = transform.rotation;
        ObjectManager.SINGLETON.AddObject(gameObject);
    }
    #endregion

    #region Unity Functions
    
    #endregion

    #region Debug
    /// <summary>
    /// Uses Debug.Log() to post a message with the name of this script and the object its attached to as the prefix only if the debugging flag is true under GlobalVariables->DefaultVariables.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintDebugMsg_PickUp(string msg)
    {
        if (isDebug_PickUp) Debug.Log(isDebugScriptName_PickUp + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogWarning() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintWarningDebugMsg_PickUp(string msg)
    {
        Debug.LogWarning(isDebugScriptName_PickUp + " (" + this.gameObject.name + "): " + msg);
    }
    /// <summary>
    /// Uses Debug.LogError() to post a message with the name of this script and the object its attached to as the prefix.
    /// </summary>
    /// <param name="msg">The message.</param>
    private void PrintErrorDebugMsg_PickUp(string msg)
    {
        Debug.LogError(isDebugScriptName_PickUp + " (" + this.gameObject.name + "): " + msg);
    }
    #endregion

    #region Getters, Setters, and Properties
    /// <summary>
    /// The time before this object respawns.
    /// </summary>
    public float RespawnRate
    {
        get
        {
            return respawnRate;
        }
    }
    #endregion
    #endregion

    #region Start & Update Functions
    private void Awake()
    {
        PrintDebugMsg_PickUp("Debugging enabled.");
    }
    private void Start()
    {
        Setup();
    }
    #endregion
}